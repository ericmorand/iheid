# IHEID Drupal Backend

## Installation instructions

### 1. Pull the project sources

### 2. Create a database

### 3. Install the dependencies

````shell
composer install
````
### 4. Create your environment variables

IHEID Drupal Backend expects the following environment variables to be set:

* `IHEID_DB_HOST`: The host hosting the database - `localhost` for example
* `IHEID_DB_PORT`: The port to use to communicate with the host - usually `3306`
* `IHEID_DB_NAME`: The name of the database
* `IHEID_DB_USERNAME`: The name of a user with at least all-but-grant privileges on the database
* `IHEID_DB_PASSWORD`: The password of the database user
* `IHEID_DB_DRIVER`: The name of the PDO driver to use - `mysql` for MySQL databases for example 

Set them system-wide or copy `web/.env.dist` to `web/.env` and edit the copy to include your settings.

> `web/.env` is ignored by the VCS.

### 6. Install Drupal 8

Open your browser to the URL of your Drupal virtual host and install Drupal 8 using English and the _minimal_ profile.

> This step should be automated in the future.

### 5. Import the configuration

It's essential to set the site UUID _before_ importing the configuration.

````shell
vendor/bin/drush cset "system.site" uuid "6e4070ed-958f-4069-93ee-da0f624a75cd" -y && vendor/bin/drush cim -y
````

> Should we make this key unique for each environment? In this case, the build process would have to retrieve the key from the environment config using `vendor/bin/drush cget "system.site" uuid` and then update `system.site.yml` accordingly prior to importing the config. Not tested yet.

### 6. Enable Twig templates debug

Optionally, you can enable Twig templates debug by copying `web/sites/default/default.services.yml` to `web/sites/default/services.yml` and setting the key `parameters:twig.config:debug:` to true in the copy.

> `web/sites/default/services.yml` is ignored by the VCS.

### 7. Build the theme

Optionally, you can build and deploy the theme assets and Twig templates by running the npm `build:theme` script:

`npm run build:theme`

Drupal cache will be cleared when the deployment is done.