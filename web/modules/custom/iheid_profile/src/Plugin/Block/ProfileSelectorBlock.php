<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 11.06.2018
 * Time: 12:41
 */

namespace Drupal\iheid_profile\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy\TermStorage;

/**
 * This block renders the selected taxonomies as a block.
 *
 * @Block(
 *   id = "iheid_profile_selector",
 *   admin_label = @Translation("Profile selector")
 * )
 */
class ProfileSelectorBlock extends BlockBase implements BlockPluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state)
    {
        $form = parent::blockForm($form, $form_state);

        $vocabularies = Vocabulary::loadMultiple();

        foreach ($vocabularies as $item) {
            /** @var Vocabulary $item */
            $key = $item->id();
            $value = $item->label();

            $values[$key] = $value;
        }

        $form['vocabulary'] = [
            '#title' => $this->t('Vocabulary'),
            '#type' => 'select',
            '#options' => $values,
            '#default_value' => $this->configuration['vocabulary'],
            '#required' => TRUE,
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state)
    {
        parent::blockSubmit($form, $form_state);

        $this->configuration['vocabulary'] = $form_state->getValue('vocabulary');
    }

    /**
     * Builds and returns the renderable array for this block plugin.
     *
     * If a block should not be rendered because it has no content, then this
     * method must also ensure to return no content: it must then only return an
     * empty array, or an empty array with #cache set (with cacheability metadata
     * indicating the circumstances for it being empty).
     *
     * @return array
     *   A renderable array representing the content of the block.
     *
     * @see \Drupal\block\BlockViewBuilder
     */
    public function build()
    {
        $renderArray = [];

        $vocabulary_id = $this->configuration['vocabulary'];

        $terms = $this->getList($vocabulary_id);

        if ($terms) {
            $renderArray += [
                '#theme' => 'profile_selector',
                '#profiles' => $terms,
            ];
        }

        return $renderArray;
    }

    private function getList($vocabulary_id)
    {
        /** @var TermStorage $storage */
        $storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
        $terms = $storage->loadTree($vocabulary_id);

        $result = array_map(function ($termData) {
            $term = Term::load($termData->tid);

            return [
                'id' => $term->id(),
                'title' => $term->getName(),
                'url' => Url::fromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $term->id()])
            ];
        }, $terms);

        return $result;
    }
}
