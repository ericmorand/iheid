<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 25.05.2018
 * Time: 12:16
 */

namespace Drupal\iheid_research_project\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'Project collaborator' formatter.
 *
 * @FieldFormatter(
 *   id = "project_collaborator",
 *   label = @Translation("Project collaborator"),
 *   field_types = {
 *     "project_collaborator"
 *   }
 * )
 */
class ProjectCollaboratorFormatter extends EntityReferenceEntityFormatter
{
    /**
     * {@inheritdoc}
     */
    public static function defaultSettings()
    {
        return [
                'highlighted_view_mode' => 'default',
                'color_scheme' => 'dark',
            ] + parent::defaultSettings();
    }

    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        // NOTE: We have used the content of the parent function, instead of using the result of the parent function.
        //       Because you cannot alter some of the values after the parent function have done with its purpose,
        //       so simply we copy the content, and add our extra lines. This may change the next versions of Drupal.

        $elements = [];

        foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
            // Due to render caching and delayed calls, the viewElements() method
            // will be called later in the rendering process through a '#pre_render'
            // callback, so we need to generate a counter that takes into account
            // all the relevant information about this field and the referenced
            // entity that is being rendered.
            $recursive_render_id = $items->getFieldDefinition()->getTargetEntityTypeId()
                . $items->getFieldDefinition()->getTargetBundle()
                . $items->getName()
                // We include the referencing entity, so we can render default images
                // without hitting recursive protections.
                . $items->getEntity()->id()
                . $entity->getEntityTypeId()
                . $entity->id();

            if (isset(static::$recursiveRenderDepth[$recursive_render_id])) {
                static::$recursiveRenderDepth[$recursive_render_id]++;
            } else {
                static::$recursiveRenderDepth[$recursive_render_id] = 1;
            }

            // Protect ourselves from recursive rendering.
            if (static::$recursiveRenderDepth[$recursive_render_id] > static::RECURSIVE_RENDER_LIMIT) {
                $this->loggerFactory->get('entity')->error('Recursive rendering detected when rendering entity %entity_type: %entity_id, using the %field_name field on the %bundle_name bundle. Aborting rendering.', [
                    '%entity_type' => $entity->getEntityTypeId(),
                    '%entity_id' => $entity->id(),
                    '%field_name' => $items->getName(),
                    '%bundle_name' => $items->getFieldDefinition()->getTargetBundle(),
                ]);
                return $elements;
            }

            if ($items[$delta]->get('highlighted')->getValue() == TRUE) {
                $view_mode = $this->getSetting('highlighted_view_mode');
            } else {
                $view_mode = $this->getSetting('view_mode');
            }

            $view_builder = $this->entityTypeManager->getViewBuilder($entity->getEntityTypeId());
            $elements[$delta] = $view_builder->view($entity, $view_mode, $entity->language()->getId());

            // Add a resource attribute to set the mapping property's value to the
            // entity's url. Since we don't know what the markup of the entity will
            // be, we shouldn't rely on it for structured data such as RDFa.
            if (!empty($items[$delta]->_attributes) && !$entity->isNew() && $entity->hasLinkTemplate('canonical')) {
                $items[$delta]->_attributes += ['resource' => $entity->toUrl()->toString()];
            }
        }

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function view(FieldItemListInterface $items, $langcode = NULL)
    {
        $elements = parent::view($items, $langcode);

        $elements['#color_scheme'] = $this->getSetting('color_scheme');

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        $elements = parent::settingsForm($form, $form_state);

        $elements['highlighted_view_mode'] = [
            '#title' => t('Highlighted View Mode'),
            '#type' => 'select',
            '#options' => $this->entityDisplayRepository->getViewModeOptions($this->getFieldSetting('target_type')),
            '#default_value' => $this->getSetting('highlighted_view_mode'),
            '#weight' => 1
        ];

        $elements['color_scheme'] = [
            '#title' => t('Color scheme'),
            '#type' => 'select',
            '#default_value' => $this->getSetting('color_scheme'),
            '#options' => $this->getColorSchemeOptions(),
            '#weight' => 3
        ];

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsSummary()
    {
        $summary = [];
        $settings = $this->getSettings();

        $view_modes = $this->entityDisplayRepository->getViewModeOptions($this->getFieldSetting('target_type'));

        $view_mode = $this->getSetting('view_mode');
        $highlighted_view_mode = $this->getSetting('highlighted_view_mode');

        if ($highlighted_view_mode === $view_mode) {
            $summary[] = t('Rendered as @mode', ['@mode' => isset($view_modes[$view_mode]) ? $view_modes[$view_mode] : $view_mode]);
        }
        else {
            $summary[] = t('Rendered as @mode/@highlighted_mode', [
                '@mode' => isset($view_modes[$view_mode]) ? $view_modes[$view_mode] : $view_mode,
                '@highlighted_mode' => isset($view_modes[$highlighted_view_mode]) ? $view_modes[$highlighted_view_mode] : $highlighted_view_mode,
            ]);
        }

        $summary[] = t('@color_scheme color scheme', ['@color_scheme' => $this->getColorSchemeOptions()[$settings['color_scheme']]]);

        return $summary;
    }

    public function getColorSchemeOptions()
    {
        return [
            'light' => $this->t('Light'),
            'dark' => $this->t('Dark')
        ];
    }
}
