<?php

/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 30.05.2018
 * Time: 17:20
 */

namespace Drupal\iheid_research_project\Plugin\Field\FieldItemList;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\Core\TypedData\Exception\MissingDataException;

/**
 * Item list for a computed field that displays highlighted project collaborator list.
 */
class HighlightedProjectCollaboratorItemList extends EntityReferenceFieldItemList
{
    use ComputedItemListTrait;

    /**
     * Computes the values for an item list.
     */
    protected function computeValue()
    {
        /** @var \Drupal\node\Entity\Node $entity */
        $entity = $this->getEntity();

        $currentLanguageCode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();

        if ($entity->hasTranslation($currentLanguageCode)) {
            $entity = $entity->getTranslation($currentLanguageCode);
        }

        /** @var FieldItemListInterface $field */
        $field = null;

        if ($entity->hasField('field_collaborators')) {
            $field = $entity->get('field_collaborators');
        } else {
            $field = $entity->get('field_project_collaborators');
        }

        foreach ($field->getValue() as $delta => $value) {
            if (!isset($value['position'])) {
                $value += [
                    'position' => null,
                    'highlighted' => TRUE
                ];
            }

            if ($value['highlighted'] == TRUE) {
                $this->list[] = $this->createItem($delta, $value);
            }
        }

        $this->valueComputed = TRUE;
    }
}
