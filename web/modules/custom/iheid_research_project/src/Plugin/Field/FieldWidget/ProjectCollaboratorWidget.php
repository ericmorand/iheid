<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 25.05.2018
 * Time: 15:21
 */

namespace Drupal\iheid_research_project\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Widget implementation of the 'project_collaborator' field type.
 *
 * @FieldWidget(
 *   id = "project_collaborator_default",
 *   label = @Translation("Project collaborator"),
 *   description = @Translation("An autocomplete project collaborator widget."),
 *   field_types = {
 *     "project_collaborator",
 *   }
 * )
 */
class ProjectCollaboratorWidget extends EntityReferenceAutocompleteWidget
{
    /**
     * {@inheritdoc}
     */
    public static function defaultSettings()
    {
        return [
                'position' => '',
                'highlighted' => FALSE
            ] + parent::defaultSettings();
    }

    public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state)
    {
        $elements = parent::formElement($items, $delta, $element, $form, $form_state);

        $elements['position'] = array(
            '#type' => 'textfield',
            '#title' => t('Position'),
            '#description' => t('The position of the collaborator in the project'),
            '#default_value' => isset($items[$delta]->position) ? $items[$delta]->position : NULL,
            '#weight' => 2,
        );

        $elements['highlighted'] = array(
            '#type' => 'checkbox',
            '#title' => t('Highlighted'),
            '#description' => t('Whether the collaborator is highlighted in the project'),
            '#default_value' => isset($items[$delta]->highlighted) ? $items[$delta]->highlighted : NULL,
            '#weight' => 3,
        );

        $elements['#element_validate'][] = [$this, 'validate'];

        return $elements;
    }

    /**
     * Validate the form fields.
     */
    public static function validate($elements, FormStateInterface $form_state)
    {
        if ($elements['target_id']['#value'] && strlen($elements['position']['#value']) == 0) {
            $form_state->setError($elements['position'], t('You must enter a position value'));
        }
    }
}