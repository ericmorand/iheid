<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 13.06.2018
 * Time: 12:35
 */

namespace Drupal\iheid_settings;

use Drupal\file\Entity\File;

class CustomSettingsManager
{
    private static $customConfigName = 'system.iheid_settings';

    /**
     * Gets the large logo's url from settings
     *
     * @return string
     */
    public static function getLargeLogoUrl()
    {
        $config = \Drupal::config(self::getCustomConfigName());
        $file = File::load($config->get('large_logo'));

        return file_create_url($file->getFileUri());
    }

    /**
     * Gets the small logo's url from settings
     *
     * @return string
     */
    public static function getSmallLogoUrl()
    {
        $config = \Drupal::config(self::getCustomConfigName());
        $file = File::load($config->get('small_logo'));

        return file_create_url($file->getFileUri());
    }

    /**
     * @return string
     */
    public static function getCustomConfigName()
    {
        return self::$customConfigName;
    }
}