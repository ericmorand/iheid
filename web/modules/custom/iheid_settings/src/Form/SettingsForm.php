<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 12.06.2018
 * Time: 14:14
 */

namespace Drupal\iheid_settings\Form;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\file\FileUsage\DatabaseFileUsageBackend;
use Drupal\iheid_settings\CustomSettingsManager;
use Drupal\system\Form\SiteInformationForm;

/**
 * Defines a form that configures forms module settings.
 */
class SettingsForm extends SiteInformationForm
{
    public function buildForm(array $form, FormStateInterface $form_state)
    {

        $form['logos'] = [
            '#type' => 'details',
            '#title' => t('Logos'),
            '#open' => TRUE
        ];

        $largeLogo = null;

        if ($largeLogoId = theme_get_setting('logo_large')) {
            $largeLogo = \Drupal\media\Entity\Media::load($largeLogoId);
        }

        $form['logos']['logo_large'] = array(
            '#type' => 'entity_autocomplete',
            '#target_type' => 'media',
            '#title' => t('Large'),
            '#default_value' => $largeLogo,
        );

        $smallLogo = null;

        if ($smallLogoId = theme_get_setting('logo_small')) {
            $smallLogo = \Drupal\media\Entity\Media::load($smallLogoId);
        }

        $form['logos']['logo_small'] = array(
            '#type' => 'entity_autocomplete',
            '#target_type' => 'media',
            '#title' => t('Small'),
            '#default_value' => $smallLogo,
        );

        $secondaryLogo = null;

        if ($secondaryLogoId = theme_get_setting('logo_secondary')) {
            $secondaryLogo = \Drupal\media\Entity\Media::load($secondaryLogoId);
        }

        $form['logos']['logo_secondary'] = array(
            '#type' => 'entity_autocomplete',
            '#target_type' => 'media',
            '#title' => t('Secondary'),
            '#default_value' => $secondaryLogo,
        );
        }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {

    }

    protected function getEditableConfigNames()
    {
        return [CustomSettingsManager::getCustomConfigName(), 'system.site'];
    }
}
