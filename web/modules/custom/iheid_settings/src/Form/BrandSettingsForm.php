<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 12.06.2018
 * Time: 14:14
 */

namespace Drupal\iheid_settings\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class BrandSettingsForm extends ConfigFormBase
{
    /**
     * Returns a unique string identifying the form.
     *
     * @return string
     *   The unique string identifying the form.
     */
    public function getFormId()
    {
        return 'iheid_settings_brand_settings';
    }


    protected function getEditableConfigNames()
    {
        return ['iheid_settings.brand_settings'];
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config('iheid_settings.brand_settings');

        $form['logos'] = [
            '#type' => 'details',
            '#title' => t('Logos'),
            '#open' => TRUE
        ];

        $largeLogo = null;

        if ($largeLogoId = $config->get('logo_large')) {
            $largeLogo = \Drupal\media\Entity\Media::load($largeLogoId);
        }

        $form['logos']['logo_large'] = array(
            '#type' => 'entity_autocomplete',
            '#target_type' => 'media',
            '#title' => t('Large'),
            '#default_value' => $largeLogo,
        );

        $smallLogo = null;

        if ($smallLogoId = $config->get('logo_small')) {
            $smallLogo = \Drupal\media\Entity\Media::load($smallLogoId);
        }

        $form['logos']['logo_small'] = array(
            '#type' => 'entity_autocomplete',
            '#target_type' => 'media',
            '#title' => t('Small'),
            '#default_value' => $smallLogo,
        );

        $secondaryLogo = null;

        if ($secondaryLogoId = $config->get('logo_secondary')) {
            $secondaryLogo = \Drupal\media\Entity\Media::load($secondaryLogoId);
        }

        $form['logos']['logo_secondary'] = array(
            '#type' => 'entity_autocomplete',
            '#target_type' => 'media',
            '#title' => t('Secondary'),
            '#default_value' => $secondaryLogo,
        );

        return parent::buildForm($form, $form_state);
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        parent::submitForm($form, $form_state);
        $this->config('iheid_settings.brand_settings')
            ->set('logo_large', $form_state->getValue('logo_large'))
            ->set('logo_small', $form_state->getValue('logo_small'))
            ->set('logo_secondary', $form_state->getValue('logo_secondary'))
            ->save();
    }
}
