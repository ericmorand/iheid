<?php

namespace Drupal\iheid_social\Form;

use Drupal\Core\Cache\CacheTagsInvalidator;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SettingsForm extends ConfigFormBase
{
    /**
     * @var CacheTagsInvalidator $cacheTagsInvalidator
     */
    protected $cacheTagsInvalidator;

    /**
     * SettingsForm constructor.
     * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
     * @param \Drupal\Core\Cache\CacheTagsInvalidator $cacheTagsInvalidator
     */
    public function __construct(ConfigFactoryInterface $config_factory, CacheTagsInvalidator $cacheTagsInvalidator)
    {
        parent::__construct($config_factory);

        $this->cacheTagsInvalidator = $cacheTagsInvalidator;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('config.factory'),
            $container->get('cache_tags.invalidator')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'iheid_social_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return ['iheid_social.settings'];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config('iheid_social.settings');

        $form['facebook'] = [
            '#type' => 'fieldset',
            '#title' => t('Facebook')
        ];

        $form['facebook']['facebook_app_id'] = [
            '#type' => 'textfield',
            '#title' => t('App ID'),
            '#description' => t('Your Facebook App ID'),
            '#default_value' => $config->get('facebook.app_id'),
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        parent::submitForm($form, $form_state);

        $this->config('iheid_social.settings')
            ->set('facebook.app_id', $form_state->getValue('facebook_app_id'))
            ->save();

        $this->cacheTagsInvalidator->invalidateTags([
            'config.iheid_social'
        ]);
    }
}
