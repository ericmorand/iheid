<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 20.06.2018
 * Time: 15:12
 */

namespace Drupal\iheid_social\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'social_profile' formatter.
 *
 * @FieldFormatter (
 *   id = "social_profile",
 *   label = @Translation("Social profile"),
 *   field_types = {
 *     "social_profile"
 *   }
 * )
 */
class SocialProfileFormatter extends FormatterBase
{

    /**
     * Builds a renderable array for a field value.
     *
     * @param \Drupal\Core\Field\FieldItemListInterface $items
     *   The field values to be rendered.
     * @param string $langcode
     *   The language that should be used to render the field.
     *
     * @return array
     *   A renderable array for $items, as an array of child elements keyed by
     *   consecutive numeric indexes starting from 0.
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = [];

        foreach ($items as $delta => $item) {
            $url = $item->url;

            if ($item->network === 'email') {
                $url = 'mailto:' . $url;
            }
            else {
                if ((strpos($url, 'http://') === false) || (strpos($url, 'https://') === false)) {
                    $url = 'https://' . $url;
                }
            }

            $elements[$delta] = array(
                '#theme' => 'social_profile_formatter',
                '#network' => $item->network,
                '#title' => $item->network === 'email' ? $item->url : $item->network,
                '#url' => $url
            );
        }

        return $elements;
    }
}
