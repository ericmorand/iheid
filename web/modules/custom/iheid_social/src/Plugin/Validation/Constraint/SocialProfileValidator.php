<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 20.06.2018
 * Time: 12:12
 */

namespace Drupal\iheid_social\Plugin\Validation\Constraint;

use Drupal\iheid_social\Plugin\Field\FieldType\SocialProfileItem;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class SocialProfileValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     */
    public function validate($items, Constraint $constraint)
    {
        $values = $items->getValue();

        $network = $values['network'];
        $url = $values['url'];

        if ($network && empty($url)) {
            $this->context->addViolation($constraint->networkWithNoUrl, ['%value' => $network]);
        } elseif ($url && !$network) {
            $this->context->addViolation($constraint->urlWithNoUrl, ['%value' => $url]);
        }
    }
}
