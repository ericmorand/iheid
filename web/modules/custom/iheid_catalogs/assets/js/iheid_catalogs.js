(function ($, Drupal, drupalSettings) {

    'use strict';

    let $view, viewIds = [
        'collaborator', 'course', 'event', 'news', 'page', 'publication', 'research-project',
    ];

    Drupal.behaviors.collapseFilters = {
        attach: function (context, settings) {
            viewIds.forEach(function (element) {
                let $viewTmp, idTmp,
                    idTemplate = "views-exposed-form-catalog-0-page-1";
                idTmp = idTemplate.replace("0", element);
                $viewTmp = $("#" + idTmp);
                // check if view_id exits in page
                if ($viewTmp.length) {
                    // set main ID
                    $view = $viewTmp;
                }
            });


            // TODO: once FE is done replace tmp selectors with real structured
            // elements
            let catalogs = drupalSettings.catalogs;

            if (catalogs) {
                let count = 0;

                $view.find(".js-webform-type-checkboxes").once(".webform-type-checkboxes").each(function (el) {
                    let $fieldset = $(this),
                        check = $fieldset.attr('data-drupal-selector');

                    catalogs.every(function (element, index) {
                        let id = 'edit-field-' + element.vid,
                            $elem = $("#" + element.vid),
                            template = element.template;

                        $fieldset.find(".js-form-type-checkbox").once(".iheid--form--element").each(function (e) {
                            let $checkboxWrapper = $(this),
                                $checkbox = $checkboxWrapper.find("input"),
                                $inner = $fieldset.find(".inner-wrapper");

                            if ($checkbox.is(':checked')) {
                                if (check == id) {
                                    let label = $checkboxWrapper.find("label").text();
                                    $inner.append("<span>" + label + "</span>");
                                }
                                count++;
                            }
                        });

                        if (check != id) {
                            return false;
                        }

                        if (!$elem.length) {
                            $("body").append(template);
                        }

                        $('<div class="inner-wrapper"></div>').insertAfter($fieldset.find("legend"));
                        $fieldset.addClass("hide-wrapper").find("legend").addClass("modal-trigger ico-plus").attr('data-id', element.vid);

                        if ($(".iheid--ui--modal.opened").length) {
                            let $html = $fieldset.find(".fieldgroup").clone();
                            $(".iheid--ui--modal.opened").find(".filter-wrapper").html($html.html());
                        }
                    });
                });

                if (count > 0) {
                    $("#filter-count").text("(" + count + " applied)");
                }
            }
        }
    };

    /**
     * Open the filters modal
     */
    $(document).on('click', '.modal-trigger', function () {
        let $self = $(this),
            id = $self.attr('data-id'),
            $fieldset = $self.parent(),
            $html = $fieldset.find(".fieldgroup").clone();
        $('<div class="overlay" style="z-index: 0;"></div>').insertBefore(".iheid--ui--modal");

        $("#" + id).addClass("opened").css(
            {
                'z-index': 0,
                'opacity': 1,
                'position': 'absolute',
                'display': 'block'
            }
        ).find(".content").find(".filter-wrapper").html($html.html());
        $('html, body').animate({scrollTop: (0)}, 500);
    });

    /**
     * Close the filters modal
     */
    $(document).on('click', '.iheid--ui--modal.opened .close', function () {
        $("body > .overlay").remove();
        $(".iheid--ui--modal.opened").removeClass("opened").attr('style', 'display:none');
    });

    /**
     * Autosearch for filters
     */
    $(document).on("keyup", ".filter-search", function (e) {
        let $input = $(this),
            filter = $input.val().toUpperCase(),
            $checkboxWrapper = $(".iheid--ui--modal.opened").find(".filter-wrapper"),
            $checkboxes = $checkboxWrapper.find(".js-form-type-checkbox"), a, i;

        for (i = 0; i < $checkboxes.length; i++) {
            a = $checkboxes[i].getElementsByTagName("label")[0]
            if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                $checkboxes[i].classList.remove("hidden");
            }
            else {
                $checkboxes[i].classList.add("hidden");
            }
        }
    });

    $(document).on("click", ".form-filters-wrapper .ico-plus", function () {
        let $self = $(this).parent();

        if ($self.hasClass("closed")) {
            $self.removeClass("closed").addClass("opened");
        }
        else {
            $self.removeClass("opened").addClass("closed");
        }
    });

    /**
     * Pseudo clear all button
     */
    $(document).on("click", ".clear-all", function () {
        $view.find('input[data-drupal-selector="edit-reset"]').click();
    });
    // Eof
})(jQuery, Drupal, drupalSettings);