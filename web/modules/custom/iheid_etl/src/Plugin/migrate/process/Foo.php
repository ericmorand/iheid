<?php
/**
 * Created by PhpStorm.
 * User: ericmorand
 * Date: 27/03/18
 * Time: 20:40
 */

namespace Drupal\iheid_etl\Plugin\migrate\process;

use Drupal\migrate\Annotation\MigrateProcessPlugin;
use Drupal\migrate\ProcessPluginBase;

/**
 * Class Foo
 * @package Drupal\iheid_etl\Plugin\migrate\process
 *
 * @MigrateProcessPlugin(
 *   id="iheid_etl_foo"
 * )
 */
class Foo extends ProcessPluginBase
{

}