<?php

namespace Drupal\iheid_navigation\Breadcrumb;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Link;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\TermStorage;

class BreadCrumbBuilder implements BreadcrumbBuilderInterface
{
    /**
     * {@inheritdoc}
     */
    public function applies(RouteMatchInterface $attributes)
    {
        $parameters = $attributes->getParameters()->all();

        if (isset($parameters['node']) && !empty($parameters['node'])) {
            return TRUE;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function build(RouteMatchInterface $route_match)
    {
        $breadcrumb = new Breadcrumb();

        if ($entity = iheid_core_get_current_entity()) {
            $breadcrumb->addLink(Link::createFromRoute(t('Home'), '<front>'));

            if ($entity->hasField('field_main_category')) {
                $categoryId = $entity->get('field_main_category')->target_id;

                if ($category = Term::load($categoryId)) {
                    // find direct parent
                    /** @var TermStorage $termStorage */
                    $termStorage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
                    $parents = $termStorage->loadParents($categoryId);

                    if ($parent = current($parents)) {
                        $breadcrumb->addLink(Link::createFromRoute($parent->label(), 'entity.taxonomy_term.canonical', ['taxonomy_term' => $parent->id()]));
                    }
                }
            }

            $breadcrumb->addLink(Link::createFromRoute($entity->label(), '<none>'));
        }

        $breadcrumb->addCacheContexts(['route']);

        return $breadcrumb;
    }

}
