<?php

namespace Drupal\iheid_navigation\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Render\Renderer;

/**
 * Provides a menu render element.
 *
 * @RenderElement("menu")
 */
class Menu extends RenderElement
{
    /**
     * {@inheritdoc}
     */
    public function getInfo()
    {
        $class = get_class($this);

        return [
            '#pre_render' => [
                [$class, 'preRenderMenu'],
            ]
        ];
    }

    public static function preRenderMenu($element)
    {
        $menuTree = \Drupal::menuTree();
        $menuName = $element['#target_id'];

        // build the typical default set of menu tree parameters.
        $parameters = $menuTree->getCurrentRouteMenuTreeParameters($menuName);

        // load the tree based on this set of parameters.
        $tree = $menuTree->load($menuName, $parameters);

        // transform the tree using the manipulators you want.
        $manipulators = array(
            // only show links that are accessible for the current user.
            array('callable' => 'menu.default_tree_manipulators:checkAccess',),
            // use the default sorting of menu links.
            array('callable' => 'menu.default_tree_manipulators:generateIndexAndSort',),
        );

        $tree = $menuTree->transform($tree, $manipulators);

        // build a renderable array from the transformed tree.
        $menu = $menuTree->build($tree);

        // render the menu to markup
        /** @var Renderer $renderer */
        $renderer = \Drupal::service('renderer');
        $element['#markup'] = $renderer->render($menu);

        return $element;
    }
}
