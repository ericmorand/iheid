<?php

namespace Drupal\iheid_navigation\Element;

use Drupal\Core\Menu\MenuLinkTree;
use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Render\Renderer;
use Drupal\iheid_navigation\Menu\SiteMapMenuLinkTree;

/**
 * Provides a site-map menu render element.
 *
 * @RenderElement("site-map-menu")
 */
class SiteMapMenu extends RenderElement
{
    /**
     * {@inheritdoc}
     */
    public function getInfo()
    {
        $class = get_class($this);

        return [
            '#target_id' => '',
            '#langcode' => '',
            '#pre_render' => [
                [$class, 'preRenderMenu'],
            ],
        ];
    }

    public static function preRenderMenu($element)
    {
        /** @var SiteMapMenuLinkTree $menuTree */
        $menuTree = \Drupal::service('iheid_navigation.menu.site_map_link_tree');
        $menuName = 'site-map';

        // build the typical default set of menu tree parameters.
        $parameters = $menuTree->getCurrentRouteMenuTreeParameters($menuName);

        // load the tree based on this set of parameters.
        $tree = $menuTree->load($menuName, $parameters);

        // transform the tree using the manipulators you want.
        $manipulators = array(
            // only show links that are accessible for the current user.
            array('callable' => 'menu.default_tree_manipulators:checkAccess',),
            // use the default sorting of menu links.
            array('callable' => 'menu.default_tree_manipulators:generateIndexAndSort',),
        );

        $tree = $menuTree->transform($tree, $manipulators);

        dump($tree); exit;


        // build a renderable array from the transformed tree.
        $menu = $menuTree->build($tree);

        // render the menu to markup
        /** @var Renderer $renderer */
        $renderer = \Drupal::service('renderer');
        $element['#markup'] = $renderer->render($menu);

        return $element;
    }
}
