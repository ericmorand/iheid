<?php
namespace Drupal\iheid_navigation\Plugin\Block;

use Drupal\Core\Annotation\ContextDefinition;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Entity\EntityDisplayRepository;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\language\Plugin\Block\LanguageBlock;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This block renders the current node.
 *
 * @Block(
 *   id = "burger_menu",
 *   admin_label = @Translation("Burger menu"),
 * )
 */
class BurgerMenuBlock extends LanguageBlock
{
    /**
     * Builds and returns the renderable array for this block plugin.
     *
     * If a block should not be rendered because it has no content, then this
     * method must also ensure to return no content: it must then only return an
     * empty array, or an empty array with #cache set (with cacheability metadata
     * indicating the circumstances for it being empty).
     *
     * @return array
     *   A renderable array representing the content of the block.
     *
     * @see \Drupal\block\BlockViewBuilder
     */
    public function build()
    {
        $routeName = $this->pathMatcher->isFrontPage() ? '<front>' : '<current>';
        $type = $this->getDerivativeId();
        /** @var \stdClass $links */
        $links = $this->languageManager->getLanguageSwitchLinks($type, Url::fromRoute($routeName));

        $currentLanguageCode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();

        $renderArray = [
            '#theme' => 'burger_menu',
            '#primary' => [
                '#type' => 'menu',
                '#target_id' => 'header'
            ],
            '#secondary' => [
                '#type' => 'menu',
                '#target_id' => 'site-map--' . $currentLanguageCode
            ],
            '#language_switcher' => [
                '#theme' => 'links__language_block',
                '#links' => $links->links,
            ],
            '#show_next_title' => t('View full site map')
        ];

        return $renderArray;
    }
}
