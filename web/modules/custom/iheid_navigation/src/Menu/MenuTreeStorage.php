<?php

namespace Drupal\iheid_navigation\Menu;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Menu\MenuLinkDefault;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\TermStorage;

/**
 * Provides a menu tree storage using the database.
 */
class MenuTreeStorage extends \Drupal\Core\Menu\MenuTreeStorage
{
    /**
     * Loads a menu link tree from the storage.
     *
     * This function may be used build the data for a menu tree only, for example
     * to further massage the data manually before further processing happens.
     * MenuLinkTree::checkAccess() needs to be invoked afterwards.
     *
     * The tree order is maintained using an optimized algorithm, for example by
     * storing each parent in an individual field, see
     * https://www.drupal.org/node/141866 for more details. However, any details
     * of the storage should not be relied upon since it may be swapped with a
     * different implementation.
     *
     * @param string $menu_name
     *   The name of the menu.
     * @param \Drupal\Core\Menu\MenuTreeParameters $parameters
     *   The parameters to determine which menu links to be loaded into a tree.
     *
     * @return array
     *   An array with 2 elements:
     *   - tree: A fully built menu tree containing an array.
     * @see static::treeDataRecursive()
     *   - route_names: An array of all route names used in the tree.
     */
//    public function loadTreeData($menu_name, MenuTreeParameters $parameters)
//    {
//        if ($menu_name === 'site-map') {
//            dump('loadTreeData', $menu_name);
//            exit;
//        }
//        else {
//            return parent::loadTreeData($menu_name, $parameters);
//        }
//    }

    /**
     * Loads links in the given menu, according to the given tree parameters.
     *
     * @param string $menu_name
     *   A menu name.
     * @param \Drupal\Core\Menu\MenuTreeParameters $parameters
     *   The parameters to determine which menu links to be loaded into a tree.
     *   This method will set the absolute minimum depth, which is used in
     *   MenuTreeStorage::doBuildTreeData().
     *
     * @return array
     *   A flat array of menu links that are part of the menu. Each array element
     *   is an associative array of information about the menu link, containing
     *   the fields from the {menu_tree} table. This array must be ordered
     *   depth-first.
     */
    protected function loadLinks($menu_name, MenuTreeParameters $parameters)
    {
        if (strpos($menu_name,  'site-map--') === 0) {
            $links = [];
            $languageCode = substr($menu_name, 10);

            // fetch taxonomy terms
            /** @var TermStorage $termStorage */
            $termStorage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
            $terms = $termStorage->loadTree('category');

            foreach ($terms as $delta => $termObj) {
                $termId = $termObj->tid;
                $term = Term::load($termId);

                if ($term->hasTranslation($languageCode)) {
                    $term = $term->getTranslation($languageCode);
                }

                $linkId = $menu_name . '.' . $termId;
                $linkParent = $menu_name . ($termObj->parents[0] ? '.' . $termObj->parents[0] : '');

                $links[$linkId] = [
                    'menu_name' => $menu_name,
                    'id' => $linkId,
                    'parent' => $linkParent,
                    'title' => serialize($term->label()),
                    'class' => MenuLinkDefault::class,
                    'enabled' => 1,
                    'route_name' => 'entity.taxonomy_term.canonical',
                    'route_parameters' => serialize([
                        'taxonomy_term' => $termId
                    ]),
                    'depth' => $termObj->depth,
                    'options' => serialize([]),
                    'description' => serialize($term->label()),
                    'has_children' => count($termStorage->loadChildren($termId)) > 0
                ];
            }

            return $links;
        } else {
            return parent::loadLinks($menu_name, $parameters);
        }
    }
}
