<?php

namespace Drupal\iheid_navigation\Menu;

use Drupal\Core\Controller\ControllerResolverInterface;
use Drupal\Core\Menu\MenuActiveTrailInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\Core\Menu\MenuLinkTree;
use Drupal\Core\Menu\MenuLinkTreeElement;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Menu\MenuTreeStorageInterface;
use Drupal\Core\Routing\RouteProviderInterface;

class SiteMapMenuLinkTree extends MenuLinkTree
{
    /**
     * Returns a tree containing of MenuLinkTreeElement based upon tree data.
     *
     * This method converts the tree representation as array coming from the tree
     * storage to a tree containing a list of MenuLinkTreeElement[].
     *
     * @param array $data_tree
     *   The tree data coming from the menu tree storage.
     *
     * @return \Drupal\Core\Menu\MenuLinkTreeElement[]
     *   An array containing the elements of a menu tree.
     */
    protected function createInstances(array $data_tree) {
        $tree = [];

        dump($data_tree); exit;

        foreach ($data_tree as $key => $element) {
            $subtree = $this->createInstances($element['subtree']);
            // Build a MenuLinkTreeElement out of the menu tree link definition:
            // transform the tree link definition into a link definition and store
            // tree metadata.
            $tree[$key] = new MenuLinkTreeElement(
                $this->menuLinkManager->createInstance($element['definition']['id']),
                (bool) $element['has_children'],
                (int) $element['depth'],
                (bool) $element['in_active_trail'],
                $subtree
            );
        }
        return $tree;
    }

}
