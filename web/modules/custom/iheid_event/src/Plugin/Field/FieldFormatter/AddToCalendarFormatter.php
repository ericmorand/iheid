<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 7.06.2018
 * Time: 12:40
 */

namespace Drupal\iheid_event\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;

/**
 * Plugin implementation of the 'Add to Calendar' formatter.
 *
 * @FieldFormatter(
 *   id = "add_to_calendar",
 *   label = @Translation("Add to Calendar"),
 *   field_types = {
 *     "add_to_calendar"
 *   }
 * )
 */
class AddToCalendarFormatter extends LinkFormatter
{
    /**
     * Builds a renderable array for a field value.
     *
     * @param \Drupal\Core\Field\FieldItemListInterface $items
     *   The field values to be rendered.
     * @param string $langcode
     *   The language that should be used to render the field.
     *
     * @return array
     *   A renderable array for $items, as an array of child elements keyed by
     *   consecutive numeric indexes starting from 0.
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = parent::viewElements($items, $langcode);

        foreach ($elements as $delta => &$element) {
            $element['#theme'] = ['add_to_calendar_formatter'];
        }

        return $elements;
    }
}
