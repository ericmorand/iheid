<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 7.06.2018
 * Time: 12:13
 */

namespace Drupal\iheid_event\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\link\Plugin\Field\FieldType\LinkItem;

/**
 * Plugin implementation of the 'add_to_calendar' field type.
 *
 * @FieldType(
 *   id = "add_to_calendar",
 *   label = @Translation("Add to Calendar"),
 *   description = @Translation("This field displays a content."),
 *   category = @Translation("Reference"),
 *   default_formatter = "add_to_calendar",
 *   no_ui = TRUE
 * )
 */
class AddToCalendarItem extends LinkItem
{

}
