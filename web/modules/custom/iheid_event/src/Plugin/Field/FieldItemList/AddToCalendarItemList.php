<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 7.06.2018
 * Time: 13:32
 */

namespace Drupal\iheid_event\Plugin\Field\FieldItemList;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\Core\Url;

/**
 * Item list for a computed field that displays highlighted project collaborator list.
 */
class AddToCalendarItemList extends FieldItemList
{
    use ComputedItemListTrait;

    /**
     * Computes the values for an item list.
     */
    protected function computeValue()
    {
        if ($entity = iheid_core_get_current_entity()) {
            // sanity check for the type
            if ($entity->getType() == 'event') {
                $url = Url::fromRoute('iheid_event.icalendar', [
                    'node_id' => $entity->get('nid')->getString()
                ]);

                $this->list[] = $this->createItem(0, [
                    'title' => t('Add to calendar'),
                    'uri' => $url->toUriString()
                ]);
            }
        }

        $this->valueComputed = TRUE;
    }
}
