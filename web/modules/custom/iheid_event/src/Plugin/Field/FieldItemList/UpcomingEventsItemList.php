<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 7.06.2018
 * Time: 13:32
 */

namespace Drupal\iheid_event\Plugin\Field\FieldItemList;

use Drupal\Core\Url;
use Drupal\iheid_search\Plugin\Field\FieldItemList\ThreeCardsItemList;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Item list for a computed field that displays highlighted project collaborator list.
 */
class UpcomingEventsItemList extends ThreeCardsItemList
{
    /**
     * @return Url
     */
    protected function createWsUrl()
    {
        /** @var Paragraph $paragraph */
        if ($paragraph = $this->getEntity()) {
            $termIds = [];

            if ($paragraph->hasField('field_terms')) {
                $termValues = $paragraph->get('field_terms')->getValue();

                foreach ($termValues as $delta => $termValue) {
                    $termIds[$delta] = $termValue['target_id'];
                }
            }

            return Url::fromRoute('iheid_event.upcoming_events', [
                'terms' => $termIds
            ]);
        }
    }
}
