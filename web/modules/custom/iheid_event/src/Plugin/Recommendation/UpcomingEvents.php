<?php

namespace Drupal\iheid_event\Plugin\Recommendation;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\iheid_search\Plugin\Annotation\RecommendationPlugin;
use Drupal\iheid_search\Plugin\RecommendationPluginBase;
use Drupal\iheid_search\RecommendedItem;

/**
 * Class UpcomingEvents
 *
 * @RecommendationPlugin(
 *     id = "upcoming_events",
 *     type = "upcoming_events",
 *     bundle = "event"
 * )
 */
class UpcomingEvents extends RecommendationPluginBase
{
    /**
     * @param int $range_start
     * @param int $total_items
     * @return array|RecommendedItem[]
     */
    public function get($range_start = 0, $total_items = 3)
    {
        $results = [];

        $date = new DrupalDateTime();
        $date->setTimezone(new \DateTimezone(DATETIME_STORAGE_TIMEZONE));
        $formatted = $date->format(\Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

        $query = \Drupal::entityQuery('node')
            ->condition('type', 'event')
            ->condition('status', 1)
            ->condition('field_start_end_date.value', $formatted, '>')
            ->sort('field_start_end_date.value', 'ASC')
            ->range($range_start, $total_items);

        foreach ($query->execute() as $id => $queryResult) {
            $item = new RecommendedItem();
            $item->setId($queryResult);
            $item->setWeight(0);

            $results[] = $item;
        }

        return $results;
    }
}
