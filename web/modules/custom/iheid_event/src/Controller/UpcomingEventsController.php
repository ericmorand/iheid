<?php
/**
 * Created by PhpStorm.
 * User: ericmorand
 * Date: 17/07/18
 * Time: 15:41
 */

namespace Drupal\iheid_event\Controller;


use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\iheid_search\RecommendationService;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UpcomingEventsController extends ControllerBase
{
    /**
     * @var RecommendationService
     */
    protected $recommendationEngine;

    /**
     * @var EntityTypeManager
     */
    protected $entityTypeManager;

    public function __construct($recommendationEngine, EntityTypeManager $entityTypeManager) {
        $this->recommendationEngine = $recommendationEngine;
        $this->entityTypeManager = $entityTypeManager;
    }

    public static function create(ContainerInterface $container)
    {
        $recommendationEngine = $container->get('iheid_search.engine');
        $entityTypeManager = $container->get('entity_type.manager');

        return new static($recommendationEngine, $entityTypeManager);
    }

    public function get(Request $request) {
        $results = [];

        $nodeIds = $this->recommendationEngine->getUpcomingEvents();
        $builder = $this->entityTypeManager->getViewBuilder('node');

        /** @var Node[] $nodes */
        $nodes = Node::loadMultiple($nodeIds);

        foreach ($nodes as $node) {
            $results[] = [
                'id' => $node->id(),
                'markup' => render($builder->view($node, $request->get('view_mode', null)))
            ];
        }

        return new JsonResponse($results);
    }
}
