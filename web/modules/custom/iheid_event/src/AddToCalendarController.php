<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 7.06.2018
 * Time: 17:18
 */

namespace Drupal\iheid_event;

use Drupal\node\Entity\Node;
use Eluceo\iCal\Component\Calendar;
use Eluceo\iCal\Component\Event;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AddToCalendarController
{
    public function iCalendar(Request $request, $node_id)
    {
        $node = Node::load($node_id);

        // Sanity check
        if (isset($node) && $node->getType() == 'event') {
            // Declaring the values
            $daterange = $node->get('field_start_end_date')->getValue()[0];
            $author = $node->get('field_speakers')->getValue();
            $body = $node->get('body')->getValue();
            $summary = '';
            $speakers = [];

            if (isset($body)) {
                $summary = $body[0]['summary'] ?: '';
            }

            foreach ($author as $delta => $value) {
                $speakers[] = $value['value'];
            }

            // Creating calendar api object
            $calendar = new Calendar('iheid');
            $calendar->setTimezone('Europe/Zurich');

            $event = new Event();
            $event->setSummary($node->getTitle())
                ->setDescription(t("@header by @speakers.\n\n@summary", [
                    '@header' => $node->getTitle(),
                    '@speakers' => implode(', ', $speakers),
                    '@summary' => $summary,
                ])->render())
                ->setLocation($node->get('field_location')->getString())
                ->setDtStart(new \DateTime($daterange['value']))
                ->setDtEnd(new \DateTime($daterange['end_value']));

            $calendar->addComponent($event);

            // Forcing content headers to download the ics file
            return new Response($calendar->render(), 200, [
                'Content-Type' => 'text/calendar; charset=utf-8',
                'Content-Disposition' => 'attachment; filename="event.ics"',
            ]);
        } else {
            return new Response(t('Event not found')->render(), 404);
        }
    }
}
