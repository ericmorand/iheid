<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 14.06.2018
 * Time: 16:37
 */

namespace Drupal\iheid_contact\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Entity\EntityDisplayRepository;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * This block renders the contacts information modal as a block.
 *
 * @Block(
 *   id = "iheid_contact_information_modal",
 *   admin_label = @Translation("Contact Information Modal Block"),
 * )
 */
class ContactsModalBlock extends BlockBase implements BlockPluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state)
    {
        $form = parent::blockForm($form, $form_state);

        /** @var EntityDisplayRepository $entityDisplayRepository */
        $entityDisplayRepository = \Drupal::service('entity_display.repository');
        $viewModes = $entityDisplayRepository->getViewModes('taxonomy_term');

        $options = [];

        foreach ($viewModes as $key => $value) {
            $options[$key] = $value['label'];
        }

        $form['view_mode'] = [
            '#title' => $this->t('View mode'),
            '#description' => $this->t('Select the view mode used to render the pillar.'),
            '#type' => 'select',
            '#options' => $options,
            '#required' => TRUE,
            '#default_value' => $this->configuration['view_mode'] ?: 'full',
        ];

        $form['body'] = [
            '#title' => $this->t('Body'),
            '#type' => 'text_format',
            '#default_value' => $this->configuration['body']['value'] ?: '',
            '#format' => $this->configuration['body']['format'] ?: 'html'
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state)
    {
        parent::blockSubmit($form, $form_state);

        $this->configuration['view_mode'] = $form_state->getValue('view_mode');
        $this->configuration['body'] = $form_state->getValue('body');
    }

    /**
     * Builds and returns the renderable array for this block plugin.
     *
     * If a block should not be rendered because it has no content, then this
     * method must also ensure to return no content: it must then only return an
     * empty array, or an empty array with #cache set (with cacheability metadata
     * indicating the circumstances for it being empty).
     *
     * @return array
     *   A renderable array representing the content of the block.
     *
     * @see \Drupal\block\BlockViewBuilder
     */
    public function build()
    {
        $url = Url::fromRoute('iheid_contact.pillars');

        $url->setRouteParameter('view_mode', $this->configuration['view_mode']);

        return [
            '#theme' => 'contact_list',
            '#service_url' => $url->toString(),
            '#body' => $this->configuration['body']['value']
        ];
    }
}
