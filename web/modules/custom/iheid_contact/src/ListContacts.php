<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 14.06.2018
 * Time: 13:24
 */

namespace Drupal\iheid_contact;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Language\LanguageInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\TermStorage;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ListContacts extends ControllerBase
{
    public function getContacts(Request $request)
    {
        $result = null;

        if ($vocabulary = Vocabulary::load('pillars')) {
            /** @var Term $pillar */
            $pillar = null;

            if (!$pillarId = $request->query->get('pillar_id')) {
                /** @var TermStorage $termStorage */
                $termStorage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');

                $pillars = $termStorage->loadTree($vocabulary->id(), 0, 1);

                $pillarId = current($pillars)->tid;
            }

            $pillar = Term::load($pillarId);

            // Sanity check, the field could be missing
            if ($pillar) {
                $currentLanguageCode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();

                if ($pillar->hasTranslation($currentLanguageCode)) {
                    $pillar = $pillar->getTranslation($currentLanguageCode);
                }

                $viewMode = $request->query->get('view_mode', 'full');

                $viewBuilder = \Drupal::entityTypeManager()->getViewBuilder('taxonomy_term');
                $element = $viewBuilder->view($pillar, $viewMode);

                $result = render($element);
            }
        }

        return new JsonResponse([
            'markup' => $result
        ]);
    }
}
