<?php

namespace Drupal\iheid_news\Plugin\Recommendation;

use Drupal\iheid_search\Plugin\Annotation\RecommendationPlugin;
use Drupal\iheid_search\Plugin\RecommendationPluginBase;
use Drupal\iheid_search\RecommendedItem;

/**
 * Class MostRecent
 *
 * @RecommendationPlugin(
 *     id = "most_recent_news",
 *     type = "most_recent",
 *     bundle = "news"
 * )
 */
class MostRecent extends RecommendationPluginBase
{
    /**
     * @param int $range_start
     * @param int $total_items
     * @return array|RecommendedItem[]
     */
    public function get($range_start = 0, $total_items = 3)
    {
        $results = [];

        $query = \Drupal::entityQuery('node')
            ->condition('type', 'news')
            ->condition('status', 1)
            ->sort('created', 'DESC')
            ->range($range_start, $total_items);

        foreach ($query->execute() as $id => $queryResult) {
            $item = new RecommendedItem();
            $item->setId($queryResult);
            $item->setWeight(0);

            $results[] = $item;
        }

        return $results;
    }
}
