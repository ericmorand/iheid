<?php
/**
 * Created by PhpStorm.
 * User: ericmorand
 * Date: 17/07/18
 * Time: 15:41
 */

namespace Drupal\iheid_search\Controller;

use Drupal\iheid_search\RecommendationController;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class RecommendedContentController extends RecommendationController
{
    public function get(Request $request) {
        $results = [];

        $nodeIds = $this->recommendationEngine->getMostRecentContent(
            $request->get('bundles', null)
        );
        $builder = $this->entityTypeManager->getViewBuilder('node');

        /** @var Node[] $nodes */
        $nodes = Node::loadMultiple($nodeIds);

        foreach ($nodes as $node) {
            $results[] = [
                'id' => $node->id(),
                'markup' => render($builder->view($node, $request->get('view_mode', null)))
            ];
        }

        return new JsonResponse($results);
    }
}
