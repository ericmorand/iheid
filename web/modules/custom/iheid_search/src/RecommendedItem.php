<?php
/**
 * Created by PhpStorm.
 * User: ericmorand
 * Date: 18/07/18
 * Time: 09:59
 */

namespace Drupal\iheid_search;


class RecommendedItem
{
    /**
     * @var int
     */
    protected $id = null;

    /**
     * @param $data int
     * @return $this
     */
    public function setId($data)
    {
        $this->id = $data;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var int
     */
    protected $weight = null;

    /**
     * @param $data int
     * @return $this
     */
    public function setWeight($data)
    {
        $this->weight = $data;
        return $this;
    }

    /**
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }
}
