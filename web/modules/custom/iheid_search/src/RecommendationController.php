<?php
/**
 * Created by PhpStorm.
 * User: ericmorand
 * Date: 17/07/18
 * Time: 15:41
 */

namespace Drupal\iheid_search;


use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class RecommendationController extends ControllerBase
{
    /**
     * @var RecommendationService
     */
    protected $recommendationEngine;

    /**
     * @var EntityTypeManager
     */
    protected $entityTypeManager;

    public function __construct($recommendationEngine, EntityTypeManager $entityTypeManager) {
        $this->recommendationEngine = $recommendationEngine;
        $this->entityTypeManager = $entityTypeManager;
    }

    public static function create(ContainerInterface $container)
    {
        $recommendationEngine = $container->get('iheid_search.engine');
        $entityTypeManager = $container->get('entity_type.manager');

        return new static($recommendationEngine, $entityTypeManager);
    }

    public function getUpcomingEvents() {
        $results = [];

        $nodeIds = $this->recommendationEngine->getUpcomingEvents();
        $builder = $this->entityTypeManager->getViewBuilder('node');

        $nodes = Node::loadMultiple($nodeIds);

        foreach ($nodes as $node) {
            $results[$node->id()] = render($builder->view($node, 'small_card'));
        }
        return new JsonResponse($results);
    }

    public function getMostRecentContent() {
        $results = [];

        $nodeIds = $this->recommendationEngine->getMostRecentContent();
        $builder = $this->entityTypeManager->getViewBuilder('node');

        $nodes = Node::loadMultiple($nodeIds);

        foreach ($nodes as $node) {
            $results[$node->id()] = render($builder->view($node, 'small_card'));
        }
        return new JsonResponse($results);
    }
}
