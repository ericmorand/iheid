<?php
/**
 * Created by PhpStorm.
 * User: ericmorand
 * Date: 22/06/18
 * Time: 20:36
 */

namespace Drupal\iheid_search\Plugin\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Class RecommendationPlugin
 *
 * @package Drupal\iheid_search\Plugin\Annotation
 *
 * @Annotation
 */
class RecommendationPlugin extends Plugin
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $bundle;
}
