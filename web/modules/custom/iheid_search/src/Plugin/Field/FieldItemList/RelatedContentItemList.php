<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 7.06.2018
 * Time: 13:32
 */

namespace Drupal\iheid_search\Plugin\Field\FieldItemList;

use Drupal\Core\Entity\ContentEntityStorageBase;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\TermStorage;

class RelatedContentItemList extends EntityReferenceFieldItemList
{
    use ComputedItemListTrait;

    /**
     * Category sibling 1
     * Category sibling 2
     * Current category
     *   Content sibling 1
     *   Current content
     *   Content sibling 2
     *   Content sibling 3
     * Category sibling 3
     * Category sibling 4
     */
    protected function computeValue()
    {
        $delta = 0;

        // retrieve "Current content"
        $currentEntity = $this->getEntity();

        // retrieve "Current category"
        /** @var EntityReferenceFieldItemList $mainCategoryFieldItemList */
        $mainCategoryFieldItemList = $currentEntity->get('field_main_category');

        /** @var Term $mainCategory */
        if ($mainCategory = current($mainCategoryFieldItemList->referencedEntities())) {
            $mainCategoryId = $mainCategory->id();

            // retrieve category siblings
            /** @var TermStorage $termStorage */
            $termStorage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
            $parentTerms = $termStorage->loadParents($mainCategoryId);

            /** @var Term $parentTerm */
            if (!$parentTerm = current($parentTerms)) {
                $parentTerm = $mainCategory;
            }

            $categorySiblings = $termStorage->loadChildren($parentTerm->id());

            // loop through the siblings
            /** @var ContentEntityStorageBase $entityStorage */
            $entityStorage = \Drupal::service('entity_type.manager')->getStorage($currentEntity->getEntityTypeId());

            foreach ($categorySiblings as $categorySiblingId => $categorySibling) {
                if ($landingPage = iheid_taxonomy_get_landing_page($categorySibling)) {
                    $this->list[$delta++] = $this->createItem($landingPage->id(), [
                        'type' => 'category',
                        'active' => ($landingPage === $currentEntity),
                        'entity' => $landingPage
                    ]);

                    if (($landingPage === $currentEntity) || ($mainCategory === $categorySibling)) {
                        $contentSiblings = $entityStorage->loadByProperties([
                            'field_main_category' => $categorySiblingId,
                        ]);

                        // loop thought content siblings
                        foreach ($contentSiblings as $contentId => $contentSibling) {
                            // todo: we should make the whitelisted bundles an optional field of the entity
                            if ($contentSibling->bundle() === 'catalog' || $contentSibling->bundle() === 'page') {
                                $this->list[$delta++] = $this->createItem($contentId, [
                                    'type' => 'content',
                                    'active' => ($contentSibling === $currentEntity),
                                    'entity' => $contentSibling
                                ]);
                            }
                        }
                    }
                }
            }
        }

        $this->valueComputed = TRUE;
    }
}
