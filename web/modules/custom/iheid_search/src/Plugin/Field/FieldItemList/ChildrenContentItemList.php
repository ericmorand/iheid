<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 7.06.2018
 * Time: 13:32
 */

namespace Drupal\iheid_search\Plugin\Field\FieldItemList;

use Drupal\Core\Entity\ContentEntityStorageBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\iheid_search\RecommendationService;
use Drupal\node\Entity\NodeType;
use Drupal\taxonomy\Entity\Term;

class ChildrenContentItemList extends EntityReferenceFieldItemList
{
    use ComputedItemListTrait;

    /**
     * @var int
     */
    static $childrenContentOffset = 0;

    /**
     * Computes the values for an item list.
     */
    protected function computeValue()
    {
        $currentEntity = iheid_core_get_current_entity();

        if ($currentEntity) {
            if ($currentEntity->hasField('field_main_category')) {
                $mainCategory = null;

                /** @var EntityReferenceFieldItemList $mainCategoryFieldItemList */
                $mainCategoryFieldItemList = $currentEntity->get('field_main_category');

                /** @var Term $mainCategory */
                if ($mainCategory = current($mainCategoryFieldItemList->referencedEntities())) {
                    /** @var RecommendationService $recommendationService */
                    $recommendationService = \Drupal::service('iheid_search.engine');

                    $members = $recommendationService->getChildren($mainCategory, ChildrenContentItemList::$childrenContentOffset, 3);

                    $delta = 0;

                    foreach ($members as $memberId => $member) {
                        $this->list[$delta++] = $this->createItem($memberId, [
                            'entity' => $member
                        ]);
                    }

                    ChildrenContentItemList::$childrenContentOffset += count($members);
                }
            }
        }

        $this->valueComputed = TRUE;
    }
}
