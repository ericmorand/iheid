<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 7.06.2018
 * Time: 13:32
 */

namespace Drupal\iheid_search\Plugin\Field\FieldItemList;

use Drupal\Core\Url;
use Drupal\paragraphs\Entity\Paragraph;

class MostRecentContentItemList extends ThreeCardsItemList
{
    /**
     * @return Url
     */
    protected function createWsUrl()
    {
        /** @var Paragraph $paragraph */
        if ($paragraph = $this->getEntity()) {
            $termIds = [];

            if ($paragraph->hasField('field_terms')) {
                $termValues = $paragraph->get('field_terms')->getValue();

                foreach ($termValues as $delta => $termValue) {
                    $termIds[$delta] = $termValue['target_id'];
                }
            }

            $bundleIds = [];

            if ($paragraph->hasField('field_content_types')) {
                $values = $paragraph->get('field_content_types')->getValue();

                foreach ($values as $delta => $contentTypeValue) {
                    $bundleIds[$delta] = $contentTypeValue['target_id'];
                }
            }

            return Url::fromRoute('iheid_search.most_recent_content', [
                'bundles' => $bundleIds,
                'terms' => $termIds
            ]);
        }
    }
}
