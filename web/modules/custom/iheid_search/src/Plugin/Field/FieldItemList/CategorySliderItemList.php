<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 7.06.2018
 * Time: 13:32
 */

namespace Drupal\iheid_search\Plugin\Field\FieldItemList;

use Drupal\Core\Entity\ContentEntityStorageBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\iheid_search\RecommendationService;
use Drupal\link\Plugin\Field\FieldType\LinkItem;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\TermStorage;

class CategorySliderItemList extends EntityReferenceFieldItemList
{
    use ComputedItemListTrait;

    public $showAllBody = null;
    public $showAllLink = null;

    /**
     * Computes the values for an item list.
     */
    protected function computeValue()
    {
        $entity = $this->getEntity();

        if ($entity) {
            if ($entity->hasField('field_term')) {
                $members = [];

                /** @var RecommendationService $recommendationService */
                $recommendationService = \Drupal::service('iheid_search.engine');

                /** @var string[] $whitelistedBundles */
                $whitelistedBundles = null;

                if ($entity->hasField('field_content_types')) {
                    /** @var EntityReferenceFieldItemList $contentTypesFieldItemList */
                    $contentTypesFieldItemList = $entity->get('field_content_types');

                    /**
                     * @var number $delta
                     * @var NodeType $nodeType
                     */
                    foreach ($contentTypesFieldItemList->referencedEntities() as $delta => $nodeType) {
                        $whitelistedBundles[] = $nodeType->id();
                    }
                }

                /** @var EntityReferenceFieldItemList $termFieldItemList */
                $termFieldItemList = $entity->get('field_term');

                /** @var Term $term */
                if ($term = current($termFieldItemList->referencedEntities())) {
                    $members += $recommendationService->getCategorySliderContent($term, $whitelistedBundles);
                }

                foreach ($members as $memberId => $member) {
                    $this->list[] = $this->createItem($memberId, [
                        'entity' => $member
                    ]);
                }

                $total = count($members);

                if ($entity->hasField('field_link')) {
                    /** @var FieldItemList $showAllFieldItemList */
                    $showAllFieldItemList = $entity->get('field_link');

                    $this->showAllBody = new \Drupal\Core\StringTranslation\PluralTranslatableMarkup(
                        $total,
                        'There is<br><b>@total article</b><br>in this category',
                        'There are<br><b>@total articles</b><br>in this category',
                        ['@total' => $total]
                    );

                    $this->showAllLink = $showAllFieldItemList->view([
                        'label' => 'hidden'
                    ]);
                }
            }
        }

        $this->valueComputed = TRUE;
    }
}
