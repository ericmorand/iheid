<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 7.06.2018
 * Time: 13:32
 */

namespace Drupal\iheid_search\Plugin\Field\FieldItemList;

use Drupal\Core\Url;
use Drupal\paragraphs\Entity\Paragraph;

class RecommendedContentItemList extends ThreeCardsItemList
{
    /**
     * @return Url
     */
    protected function createWsUrl()
    {
        if ($entity = $this->getEntity()) {
            $termIds = [];

            $termValues = $entity->get('field_terms')->getValue();

            foreach ($termValues as $delta => $termValue) {
                $termIds[$delta] = $termValue['target_id'];
            }

            $bundleIds = [];

            $values = $entity->get('field_content_types')->getValue();

            foreach ($values as $delta => $contentTypeValue) {
                $bundleIds[$delta] = $contentTypeValue['target_id'];
            }

            return Url::fromRoute('iheid_search.recommended_content', [
                'bundles' => $bundleIds,
                'terms' => $termIds
            ]);
        }
    }
}
