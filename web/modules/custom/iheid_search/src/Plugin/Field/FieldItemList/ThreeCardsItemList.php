<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 7.06.2018
 * Time: 13:32
 */

namespace Drupal\iheid_search\Plugin\Field\FieldItemList;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\Core\Url;
use Drupal\paragraphs\Entity\Paragraph;

abstract class ThreeCardsItemList extends FieldItemList
{
    use ComputedItemListTrait;

    /**
     * Computes the values for an item list.
     */
    protected function computeValue()
    {
        $this->list[] = $this->createItem(0, [
            'ws_url' => $this->createWsUrl()
        ]);

        $this->valueComputed = TRUE;
    }

    /**
     * @return Url
     */
    abstract protected function createWsUrl();
}
