<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 7.06.2018
 * Time: 12:13
 */

namespace Drupal\iheid_search\Plugin\Field\FieldFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\iheid_event\Plugin\Field\FieldType\UpcomingEventsItem;
use Drupal\iheid_search\Plugin\Field\FieldType\ThreeCardsItem;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'three_cards' field formatter.
 *
 * @FieldFormatter(
 *   id = "three_cards",
 *   label = @Translation("Three cards"),
 *   field_types = {
 *     "three_cards"
 *   }
 * )
 */
class ThreeCardsFormatter extends FormatterBase implements ContainerFactoryPluginInterface
{
    /**
     * The entity type manager.
     *
     * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
     */
    protected $entityDisplayRepository;

    /**
     * Constructs a EntityReferenceEntityFormatter instance.
     *
     * @param string $plugin_id
     *   The plugin_id for the formatter.
     * @param mixed $plugin_definition
     *   The plugin implementation definition.
     * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
     *   The definition of the field to which the formatter is associated.
     * @param array $settings
     *   The formatter settings.
     * @param string $label
     *   The formatter label display setting.
     * @param string $view_mode
     *   The view mode.
     * @param array $third_party_settings
     *   Any third party settings settings.
     * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
     *   The entity display repository.
     */
    public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityDisplayRepositoryInterface $entity_display_repository) {
        parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

        $this->entityDisplayRepository = $entity_display_repository;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
        return new static(
            $plugin_id,
            $plugin_definition,
            $configuration['field_definition'],
            $configuration['settings'],
            $configuration['label'],
            $configuration['view_mode'],
            $configuration['third_party_settings'],
            $container->get('entity_display.repository')
        );
    }

    /**
     * {@inheritdoc}
     */
    public static function defaultSettings() {
        return [
                'view_mode' => 'default',
            ] + parent::defaultSettings();
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state) {
        $elements['view_mode'] = [
            '#type' => 'select',
            '#options' => $this->entityDisplayRepository->getViewModeOptions('node'),
            '#title' => t('View mode'),
            '#default_value' => $this->getSetting('view_mode'),
            '#required' => TRUE,
        ];

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode) {
        $elements = [];

        foreach ($items as $delta => $item) {
            $elements[$delta] = [
                '#theme' => 'three_cards_formatter'
            ];
        }

        return $elements;
    }

    public function view(FieldItemListInterface $items, $langcode = NULL)
    {
        $elements = parent::view($items, $langcode);

        /** @var Url $wsUrl */
        $wsUrl = null;

        /**
         * @var int $delta
         * @var ThreeCardsItem $item
         */
        foreach ($items as $delta => $item) {
            $wsUrl = $item->getValue()['ws_url'];
        }

        $wsUrl->setRouteParameter('view_mode', $this->getSetting('view_mode'));

        $elements['#ws_url'] = $wsUrl->toString();

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsSummary() {
        $summary = [];

        $view_modes = $this->entityDisplayRepository->getViewModeOptions('node');
        $view_mode = $this->getSetting('view_mode');
        $summary[] = t('Rendered as @mode', ['@mode' => isset($view_modes[$view_mode]) ? $view_modes[$view_mode] : $view_mode]);

        return $summary;
    }
}
