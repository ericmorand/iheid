<?php

namespace Drupal\iheid_search\Plugin;

use Drupal\iheid_search\RecommendedItem;

interface RecommendationPluginInterface {
    /**
     * @param int $range_start
     * @param int $total_items
     * @return RecommendedItem[]
     */
    public function get($range_start = 0, $total_items = 0);
}
