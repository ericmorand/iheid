<?php
/**
 * Created by PhpStorm.
 * User: ericmorand
 * Date: 22/06/18
 * Time: 20:42
 */

namespace Drupal\iheid_search\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

class PluginManager extends \Drupal\Core\Plugin\DefaultPluginManager {
    public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
        parent::__construct(
            'Plugin/Recommendation',
            $namespaces,
            $module_handler,
            'Drupal\iheid_search\Plugin\RecommendationPluginInterface',
            'Drupal\iheid_search\Plugin\Annotation\RecommendationPlugin'
        );
    }
}
