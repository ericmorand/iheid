<?php
/**
 * Created by PhpStorm.
 * User: ericmorand
 * Date: 23/06/18
 * Time: 16:31
 */

namespace Drupal\iheid_search;

use Drupal\Core\Database\Query\Query;
use Drupal\Core\Entity\ContentEntityStorageBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\iheid_search\Plugin\Annotation\RecommendationPlugin;
use Drupal\iheid_search\Plugin\PluginManager;
use Drupal\iheid_search\Plugin\RecommendationPluginBase;
use Drupal\iheid_search\Plugin\RecommendationPluginInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy\TermStorage;

class RecommendationService
{
    /**
     * @var RecommendationPluginBase[]
     */
    protected $plugins = [];

    protected $recommendationPluginManager;

    function __construct(PluginManager $recommendationPluginManager)
    {
        $this->recommendationPluginManager = $recommendationPluginManager;
    }

    protected function getPlugins($type, $bundles = null)
    {
        $plugins = [];
        $pluginDefinitions = $this->recommendationPluginManager->getDefinitions();

        /** @var RecommendationPlugin $pluginDefinition */
        foreach ($pluginDefinitions as $pluginDefinition) {
            $pluginType = $pluginDefinition['type'];
            $pluginBundle = $pluginDefinition['bundle'];

            if (($pluginType === $type) && (!$bundles || in_array($pluginBundle, $bundles))) {
                $plugins[] = $this->recommendationPluginManager->createInstance(
                    $pluginDefinition['id']
                );
            }
        }

        return $plugins;
    }

    /**
     * @return number[]
     */
    function getUpcomingEvents()
    {
        $results = [];

        /** @var Query $query */
        $query = null;

        foreach ($this->getPlugins('upcoming_events') as $plugin) {
            foreach ($plugin->get() as $item) {
                $results[] = $item->getId();
            }
        }

        return $results;
    }

    /**
     * @param string[] $bundles
     * @return number[]
     */
    function getMostRecentContent($bundles)
    {
        $results = [];

        /** @var Query $query */
        $query = null;

        foreach ($this->getPlugins('most_recent', $bundles) as $plugin) {
            foreach ($plugin->get() as $item) {
                $results[] = $item->getId();
            }
        }

        return $results;
    }

    /**
     * @param Term $category
     * @param string[] $bundles
     * @return EntityInterface[]
     */
    function getCategorySliderContent(Term $category, $bundles = [])
    {
        $results = [];

        /** @var TermStorage $termStorage */
        $termStorage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
        $children = $termStorage->loadChildren($category->id());

        foreach ($children as $childId => $child) {
            if ($landingPage = iheid_taxonomy_get_landing_page($child)) {
                $results[] = $landingPage;
            }
        }

        return $results;
    }

    /**
     * Returns the terms that are direct children if the term passed as parameter.
     *
     * @param Term $category
     * @param int $offset
     * @return EntityInterface[]
     */
    function getChildren(Term $category, $offset = 0, $length = null)
    {
        $results = [];

        /** @var TermStorage $termStorage */
        $termStorage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
        $children = $termStorage->loadChildren($category->id());

        /**
         * @var int $id
         * @var Term $child
         */
        foreach ($children as $id => $child) {
            if ($child->hasField('field_landing_page')) {
                /** @var EntityReferenceFieldItemList $landingPageFieldItemList */
                $landingPageFieldItemList = $child->get('field_landing_page');

                if ($landingPage = current($landingPageFieldItemList->referencedEntities())) {
                    $results[] = $landingPage;
                }
            }
        }

        $results = array_slice($results, $offset, $length);

        return $results;
    }
}
