<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 6.07.2018
 * Time: 18:27
 */

namespace Drupal\iheid_paragraph\Plugin\Field\FieldType;

use Drupal\text\Plugin\Field\FieldType\TextItem;

/**
 * Defines the 'copyright_text' entity field item.
 *
 * @FieldType(
 *   id = "copyright_text",
 *   label = @Translation("Copyright Text"),
 *   description = @Translation("A copyright text field."),
 *   category = @Translation("Reference"),
 *   default_widget = "copyright_text_default",
 *   default_formatter = "copyright_text",
 * )
 */
class CopyrightTextItem extends TextItem
{
}