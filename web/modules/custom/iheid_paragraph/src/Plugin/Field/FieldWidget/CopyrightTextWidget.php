<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 6.07.2018
 * Time: 18:29
 */

namespace Drupal\iheid_paragraph\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;

/**
 * Plugin implementation of the 'copyright_text' widget.
 *
 * @FieldWidget(
 *   id = "copyright_text_default",
 *   label = @Translation("Copyright Text"),
 *   field_types = {
 *     "copyright_text"
 *   }
 * )
 */
class CopyrightTextWidget extends StringTextfieldWidget
{
}
