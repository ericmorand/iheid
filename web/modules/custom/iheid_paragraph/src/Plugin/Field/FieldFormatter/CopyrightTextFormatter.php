<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 6.07.2018
 * Time: 18:31
 */

namespace Drupal\iheid_paragraph\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\text\Plugin\Field\FieldFormatter\TextDefaultFormatter;

/**
 * Plugin implementation of the 'Copyright Text' formatter.
 *
 * @FieldFormatter(
 *   id = "copyright_text",
 *   label = @Translation("Copyright Text"),
 *   field_types = {
 *     "copyright_text"
 *   }
 * )
 */
class CopyrightTextFormatter extends TextDefaultFormatter
{
    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = parent::viewElements($items, $langcode);

        foreach ($elements as &$element) {
            $element['#theme'] = 'copyright_text_formatter';
        }

        return $elements;
    }
}