<?php
/**
 * Created by PhpStorm.
 * User: ericmorand
 * Date: 14/08/18
 * Time: 19:29
 */

namespace Drupal\iheid_taxonomy\RouteProcessor;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\RouteProcessor\OutboundRouteProcessorInterface;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\Routing\Route;

class RouteProcessorLandingPage implements OutboundRouteProcessorInterface
{
    public function processOutbound($route_name, Route $route, array &$parameters, BubbleableMetadata $bubbleable_metadata = NULL)
    {
        if ($route_name === 'entity.taxonomy_term.canonical') {
            $termId = $parameters['taxonomy_term'];

            if ($term = Term::load($termId)) {
                if ($term->hasField('field_landing_page')) {
                    /** @var EntityReferenceFieldItemList $landingPageItems */
                    $landingPageItems = $term->get('field_landing_page');
                    $landingPageIds = $landingPageItems->getValue();

                    if ($landingPageIds) {
                        if (isset($landingPageIds[0]['target_id'])) {
                            $landingPageId = $landingPageIds[0]['target_id'];
                            $parameters['node'] = $landingPageId;

                            unset($parameters['taxonomy_term']);

                            $route->setPath('/node/{node}');
                        }
                    }
                }
            }
        }
    }
}
