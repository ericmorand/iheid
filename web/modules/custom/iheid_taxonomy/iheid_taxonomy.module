<?php

use Drupal\Core\Entity\EntityFieldManager;
use Drupal\field\Entity\FieldConfig;

/**
 * @param int $tid
 * @return \Drupal\taxonomy\Entity\Term|null
 */
function iheid_taxonomy_get_closest_term_with_logo($tid)
{
    /** @var \Drupal\taxonomy\Entity\Term[] $ancestors */
    $ancestors = \Drupal::service('entity_type.manager')->getStorage("taxonomy_term")->loadAllParents($tid);

    /** @var EntityFieldManager $entityFieldManager */
    $entityFieldManager = \Drupal::service('entity_field.manager');

    foreach ($ancestors as $ancestor) {
        /** @var FieldConfig $logoFieldConfig */
        $logoFieldConfig = $entityFieldManager->getFieldDefinitions($ancestor->getEntityTypeId(), $ancestor->bundle())['field_logo'];

        if ($logoFieldConfig) {
            return $ancestor;
        }
    }

    return null;
}

/**
 * @param \Drupal\taxonomy\Entity\Term $term
 * @return \Drupal\media\Entity\Media|null
 */
function iheid_taxonomy_get_term_logo($term)
{
    $logo = null;
    $logoId = $term->get('field_logo')->target_id;

    if ($logoId) {
        $logo = \Drupal\media\Entity\Media::load($logoId);
    }

    return $logo;
}

/**
 * @param \Drupal\taxonomy\Entity\Term $term
 * @return \Drupal\node\NodeInterface
 */
function iheid_taxonomy_get_landing_page($term)
{
    $landingPage = null;

    if ($term->hasField('field_landing_page')) {
        /** @var \Drupal\Core\Field\EntityReferenceFieldItemList $landingPageFieldItemList */
        $landingPageFieldItemList = $term->get('field_landing_page');

        $landingPage = current($landingPageFieldItemList->referencedEntities());
    }

    return $landingPage;
}
