<?php
/**
 * Prepares variables for "image as decorated image" formatter templates.
 *
 * Default template: image-formatter-decorated-image.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - item: An ImageItem object.
 *   - item_attributes: An optional associative array of html attributes to be
 *     placed in the img tag.
 *   - image_style: An optional image style.
 *   - decorations: An array containing the name of the decorations to apply to the image.
 */
function template_preprocess_image_formatter_decorated_image(&$variables)
{
    // todo: we should rely on core image module "template_preprocess_image_style" function but for some reason it is not called between cache clear calls
    if ($variables['image_style']) {
        $variables['image'] = [
            '#theme' => 'image_style',
            '#style_name' => $variables['image_style'],
        ];
    } else {
        $variables['image'] = [
            '#theme' => 'image',
        ];
    }

    $variables['image']['#attributes'] = $variables['item_attributes'];

    $item = $variables['item'];

    // Do not output an empty 'title' attribute.
    if (\Drupal\Component\Utility\Unicode::strlen($item->title) != 0) {
        $variables['image']['#title'] = $item->title;
    }

    if (($entity = $item->entity) && empty($item->uri)) {
        $variables['image']['#uri'] = $entity->getFileUri();
    } else {
        $variables['image']['#uri'] = $item->uri;
    }

    foreach (['width', 'height', 'alt'] as $key) {
        $variables['image']["#$key"] = $item->$key;
    }
}

/**
 * Prepares variables for "link as button" formatter templates.
 *
 * Default template: link-formatter-button.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - title: An ImageItem object.
 *   - url_title: An optional associative array of html attributes to be
 *     placed in the img tag.
 *   - url: An optional image style.
 *   - variant: A string containing the name of the button variant to use.
 */
function template_preprocess_link_formatter_button(&$variables)
{
}

/**
 * Prepares variables for social share field templates.
 **
 * Default template: social-share-formatter.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - title: (optional) A descriptive or alternate title for the entity link.
 *   - url_title: The anchor text for the link.
 *   - url: A \Drupal\Core\Url object.
 *   - network: The name of the social network.
 */
function template_preprocess_social_share_formatter(&$variables)
{
}

/**
 * Prepares variables for file link templates.
 *
 * Default template: file-link.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - file: A file object to which the link will be created.
 *   - icon_directory: (optional) A path to a directory of icons to be used for
 *     files. Defaults to the value of the "icon.directory" variable.
 *   - description: A description to be displayed instead of the filename.
 *   - attributes: An associative array of attributes to be placed in the a tag.
 */
function template_preprocess_file_formatter_link_as_button(&$variables)
{
    $file = $variables['file'];

    $file_entity = ($file instanceof \Drupal\file\Entity\File) ? $file : \Drupal\file\Entity\File::load($file->fid);
    // @todo Wrap in file_url_transform_relative(). This is currently
    // impossible. As a work-around, we currently add the 'url.site' cache context
    // to ensure different file URLs are generated for different sites in a
    // multisite setup, including HTTP and HTTPS versions of the same site.
    // Fix in https://www.drupal.org/node/2646744.
    $url = file_create_url($file_entity->getFileUri());

    $variables['#cache']['contexts'][] = 'url.site';
    $variables['attributes']['type'] = $file->getMimeType() . '; length=' . $file->getSize();
    $variables['attributes']['href'] = \Drupal\Core\Url::fromUri($url)->getUri();

    // Use the description as the link text if available.
    if (empty($variables['description'])) {
        $variables['description'] = $file_entity->getFilename();
    } else {
        $variables['attributes']['title'] = $file_entity->getFilename();
    }
}

/**
 * Prepares variables for "Content Type" formatter templates.
 *
 * @param array $variables
 *   An associative array containing:
 *   - text: The overriden title of the entity.
 */
function iheid_field_preprocess_content_type_formatter(&$variables)
{
}

/**
 * Prepares variables for fitted image formatter templates.
 *
 * Default template: fitted-image-formatter.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - item: An ImageItem object.
 *   - item_attributes: An optional associative array of html attributes to be
 *     placed in the img tag.
 *   - image_style: An optional image style.
 *   - url: An optional \Drupal\Core\Url object.
 */
function template_preprocess_fitted_image_formatter(&$variables) {
    unset($variables['image']['#theme']);

    $variables['image']['#type'] = 'fitted_image';
    $variables['image']['#fit'] = $variables['fit'];
}
