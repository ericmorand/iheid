<?php

/**
 * Prepares variables for fieldgroup linear layout templates.
 *
 * Default template: field-group-linear-layout.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties and children of
 *     the group.
 */
function template_preprocess_field_group_linear_layout(&$variables)
{
    template_preprocess_field_group_view_group($variables);
}

/**
 * Prepares variables for fieldgroup view group templates.
 *
 * Default template: field-group-linear-layout.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties and children of
 *     the group.
 */
function template_preprocess_field_group_view_group(&$variables)
{
    $element = $variables['element'];

    $variables['label'] = $element['#label'];
    $variables['attributes'] = $element['#attributes'];
    $variables['children'] = (!empty($element['#children'])) ? $element['#children'] : '';
}

/**
 * Prepares variables for fieldgroup link templates.
 *
 * Default template: field-group-link.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties and children of
 *     the group.
 */
function template_preprocess_field_group_link(&$variables)
{
    $element = $variables['element'];

    if (isset($element['#label'])) {
        $variables['label'] = $element['#label'];
    }

    $variables['attributes'] = $element['#attributes'];

    /** @var \Drupal\Core\Url $url */
    $url = $element['#url'];

    if ($url) {
        $variables['attributes'] += $element['#options']['attributes'];
        $variables['attributes']['href'] = $url->toString();
    }

    $variables['children'] = (!empty($element['#children'])) ? $element['#children'] : '';
}
