<?php

namespace Drupal\iheid_field;

use Drupal\Core\Datetime\DateFormatterInterface;

/**
 * Created by PhpStorm.
 * User: ericmorand
 * Date: 01/07/18
 * Time: 21:18
 */
trait TearOffCalendarSheetTrait
{
    /**
     * @param \DateTime $dateTime
     * @param string $timezone
     * @param string $langcode
     * @return array
     */
    protected function explodeDate(\DateTime $dateTime, $timezone, $langcode)
    {
        $timestamp = $dateTime->getTimestamp();

        /** @var DateFormatterInterface $dateFormatter */
        $dateFormatter = \Drupal::service('date.formatter');

        $dayOfWeek = $dateFormatter->format($timestamp, 'custom', 'l', $timezone, $langcode);
        $dayOfMonth = $dateFormatter->format($timestamp, 'custom', 'd', $timezone, $langcode);
        $month = $dateFormatter->format($timestamp, 'custom', 'F', $timezone, $langcode);

        return [
            'day_of_week' => $dayOfWeek,
            'day_of_month' => $dayOfMonth,
            'month' => $month
        ];
    }
}
