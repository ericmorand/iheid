<?php

namespace Drupal\iheid_field\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Annotation\FormElement;
use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides a render element for a fitted image.
 *
 * @FormElement("fitted_image")
 */
class FittedImage extends RenderElement
{
    /**
     * {@inheritdoc}
     */
    public function getInfo()
    {
        return [
            '#theme' => 'fitted_image',
        ];
    }
}
