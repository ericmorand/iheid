<?php

namespace Drupal\iheid_field\Element;

use Drupal\Core\Render\Annotation\FormElement;
use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides a render element for a link group.
 *
 * @FormElement("field_group_link")
 */
class Link extends RenderElement
{
    /**
     * {@inheritdoc}
     */
    public function getInfo()
    {
        return [
            '#theme_wrappers' => array('field_group_link'),
        ];
    }
}
