<?php

namespace Drupal\iheid_field\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Annotation\FormElement;
use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides a render element for a linear layout.
 *
 * @FormElement("field_group_linear_layout")
 */
class LinearLayout extends RenderElement
{
    /**
     * {@inheritdoc}
     */
    public function getInfo()
    {
        return [
            '#theme_wrappers' => array('field_group_linear_layout'),
        ];
    }
}
