<?php

namespace Drupal\iheid_field\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides a raw text render element.
 *
 * @RenderElement("raw_text")
 */
class RawText extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);

    return [
      '#text' => '',
      '#pre_render' => [
        [$class, 'preRenderText'],
      ],
    ];
  }

  public static function preRenderText($element) {
    $text = $element['#text'];

    $element['#children'] = $text;

    return $element;
  }
}
