<?php

namespace Drupal\iheid_field\Plugin\field_group\FieldGroupFormatter;

/**
 * Plugin that disabled the native field_ground 'tab' formatter for "view" context.
 *
 * @FieldGroupFormatter(
 *   id = "tab",
 *   label = @Translation("Tab"),
 *   description = @Translation("This fieldgroup renders the content as a tab."),
 *   format_types = {
 *     "open",
 *     "closed",
 *   },
 *   supported_contexts = {
 *     "form"
 *   }
 * )
 */
class Tab extends \Drupal\field_group\Plugin\field_group\FieldGroupFormatter\Tab
{

}
