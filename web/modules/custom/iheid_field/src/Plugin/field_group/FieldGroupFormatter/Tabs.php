<?php

namespace Drupal\iheid_field\Plugin\field_group\FieldGroupFormatter;

/**
 * Plugin that disabled the native field_ground 'tabs' formatter for "view" context.
 *
 * @FieldGroupFormatter(
 *   id = "tabs",
 *   label = @Translation("Tabs"),
 *   description = @Translation("This fieldgroup renders child groups in its own tabs wrapper."),
 *   supported_contexts = {
 *     "form"
 *   }
 * )
 */
class Tabs extends \Drupal\field_group\Plugin\field_group\FieldGroupFormatter\Tabs
{

}
