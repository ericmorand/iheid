<?php

namespace Drupal\iheid_field\Plugin\field_group\FieldGroupFormatter;

/**
 * Plugin that disabled the native field_ground 'fieldset' formatter for "view" context.
 *
 * @FieldGroupFormatter(
 *   id = "fieldset",
 *   label = @Translation("Fieldset"),
 *   description = @Translation("This fieldgroup renders the inner content in a fieldset with the title as legend."),
 *   supported_contexts = {
 *     "form"
 *   }
 * )
 */
class Fieldset extends \Drupal\field_group\Plugin\field_group\FieldGroupFormatter\Fieldset
{
}
