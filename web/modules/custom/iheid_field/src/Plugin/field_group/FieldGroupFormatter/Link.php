<?php

namespace Drupal\iheid_field\Plugin\field_group\FieldGroupFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\field_group\Annotation\FieldGroupFormatter;

/**
 * Plugin implementation of the 'link group' formatter.
 *
 * @FieldGroupFormatter(
 *   id = "link",
 *   label = @Translation("Link"),
 *   description = @Translation("Renders a field group as a link."),
 *   supported_contexts = {
 *     "view",
 *   },
 *   supported_link_field_types = {
 *    "link"
 *   }
 * )
 */
class Link extends \Drupal\field_group_link\Plugin\field_group\FieldGroupFormatter\Link
{
    /**
     * {@inheritdoc}
     */
    public function preRender(&$element, $rendering_object)
    {
        $element += [
            '#type' => 'field_group_link',
            '#attributes' => []
        ];

        $decorators = $this->getDecorators();

        if (!empty($decorators)) {
            $element['#attributes']['class'] = $decorators;
        }

        parent::preRender($element, $rendering_object);
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm()
    {
        $form = parent::settingsForm();

        unset($form['id']);
        unset($form['classes']);

        $form['decorators'] = array(
            '#title' => t('Decorators'),
            '#type' => 'textarea',
            '#default_value' => $this->getSetting('decorators'),
            '#maxlength' => NULL
        );

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsSummary()
    {
        $summary = array();

        $target = $this->getSetting('target');

        if ($target) {
            $options = $this->getOptions();

            $summary[] = \Drupal::translation()->translate('Target: @target', array('@target' => $options[$target]));
        }

        $decorators = $this->getDecorators();

        if ($decorators) {
            $summary[] = \Drupal::translation()->translate('Decorators: @decorators', array('@decorators' => join(',', $decorators)));
        }

        return $summary;
    }

    /**
     * Get the decorators to add to the group.
     */
    protected function getDecorators()
    {
        return iheid_field_get_decorators($this->getSetting('decorators'));
    }

    protected function getOptions()
    {
        $options = array(
            'entity' => $this->t('Full @entity_type page', array('@entity_type' => $this->group->entity_type)),
            'custom_uri' => $this->t('Custom URL'),
        );

        // TODO: Use static getter once it becomes available.
        $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions($this->group->entity_type, $this->group->bundle);

        /** @var BaseFieldDefinition $field */
        foreach ($fields as $field) {
            if (in_array($field->getType(), $this->pluginDefinition['supported_link_field_types']) && $field->getFieldStorageDefinition()->isBaseField() == FALSE) {
                $options[$field->getName()] = $field->getLabel();
            }
        }

        return $options;
    }
}
