<?php

namespace Drupal\iheid_field\Plugin\field_group\FieldGroupFormatter;

/**
 * Plugin that disabled the native field_ground 'details' formatter for "view" context.
 *
 * @FieldGroupFormatter(
 *   id = "details",
 *   label = @Translation("Details"),
 *   description = @Translation("Add a details element"),
 *   supported_contexts = {
 *     "form"
 *   }
 * )
 */
class Details extends \Drupal\field_group\Plugin\field_group\FieldGroupFormatter\Details
{

}
