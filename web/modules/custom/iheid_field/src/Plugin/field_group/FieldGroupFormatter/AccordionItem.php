<?php

namespace Drupal\iheid_field\Plugin\field_group\FieldGroupFormatter;

/**
 * Plugin that disabled the native field_ground 'accordion_item' formatter for "view" context.
 *
 * @FieldGroupFormatter(
 *   id = "accordion_item",
 *   label = @Translation("Accordion Item"),
 *   description = @Translation("This fieldgroup renders the content in a div, part of accordion group."),
 *   supported_contexts = {
 *     "form"
 *   }
 * )
 */
class AccordionItem extends \Drupal\field_group\Plugin\field_group\FieldGroupFormatter\AccordionItem
{

}
