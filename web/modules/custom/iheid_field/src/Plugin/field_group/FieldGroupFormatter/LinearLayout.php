<?php

namespace Drupal\iheid_field\Plugin\field_group\FieldGroupFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Render\Element;
use Drupal\field_group\Annotation\FieldGroupFormatter;

/**
 * Plugin implementation of the 'linear layout' formatter.
 *
 * @FieldGroupFormatter(
 *   id = "iheid_field_linear_layout",
 *   label = @Translation("Linear layout"),
 *   description = @Translation("This fieldgroup renders child groups using a linear layout."),
 *   supported_contexts = {
 *    "view"
 *   }
 * )
 */
class LinearLayout extends ViewGroup
{
    /**
     * {@inheritdoc}
     */
    public function preRender(&$element, $rendering_object)
    {
        $element += [
            '#type' => 'field_group_linear_layout'
        ];

        parent::preRender($element, $rendering_object);
    }
}
