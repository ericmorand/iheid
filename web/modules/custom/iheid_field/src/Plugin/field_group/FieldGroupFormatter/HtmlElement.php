<?php

namespace Drupal\iheid_field\Plugin\field_group\FieldGroupFormatter;

/**
 * Plugin that disabled the native field_ground 'html_element' formatter for "view" context.
 *
 * @FieldGroupFormatter(
 *   id = "html_element",
 *   label = @Translation("HTML element"),
 *   description = @Translation("This fieldgroup renders the inner content in a HTML element with classes and attributes."),
 *   supported_contexts = {
 *     "form"
 *   }
 * )
 */
class HtmlElement extends \Drupal\field_group\Plugin\field_group\FieldGroupFormatter\HtmlElement
{

}
