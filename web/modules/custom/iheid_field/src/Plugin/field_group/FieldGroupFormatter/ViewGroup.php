<?php

namespace Drupal\iheid_field\Plugin\field_group\FieldGroupFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Element;
use Drupal\field_group\FieldGroupFormatterBase;

/**
 * Plugin implementation of the 'view group' formatter.
 *
 * @FieldGroupFormatter(
 *   id = "iheid_field_view_group",
 *   label = @Translation("View group"),
 *   description = @Translation("This fieldgroup renders its inner content as-is."),
 *   supported_contexts = {
 *    "view"
 *   }
 * )
 */
class ViewGroup extends FieldGroupFormatterBase
{
    /**
     * {@inheritdoc}
     */
    public function preRender(&$element, $rendering_object)
    {
        parent::preRender($element, $rendering_object);

        $element += [
            '#type' => 'field_group_view_group',
            '#attributes' => []
        ];

        $decorators = $this->getDecorators();

        if (!empty($decorators)) {
            $element['#attributes']['class'] = $decorators;
        }

        $element['#label'] = Html::escape($this->t($this->getLabel()));
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm()
    {
        $form = parent::settingsForm();

        unset($form['id']);
        unset($form['classes']);

        $form['decorators'] = array(
            '#title' => t('Decorators'),
            '#type' => 'textarea',
            '#default_value' => $this->getSetting('decorators'),
            '#maxlength' => NULL
        );

        return $form;
    }

    /**
     * Get the decorators to add to the group.
     */
    protected function getDecorators() {
        return iheid_field_get_decorators($this->getSetting('decorators'));
    }

    /**
     * {@inheritdoc}
     */
    public function settingsSummary() {

        $summary = array();

        $decorators = $this->getDecorators();

        if ($decorators) {
            $summary[] = \Drupal::translation()->translate('Decorators: @decorators', array('@decorators' => join(',', $decorators)));
        }

        return $summary;
    }
}
