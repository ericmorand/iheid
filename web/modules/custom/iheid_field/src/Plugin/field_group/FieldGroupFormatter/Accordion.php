<?php

namespace Drupal\iheid_field\Plugin\field_group\FieldGroupFormatter;

/**
 * Plugin that disabled the native field_ground 'accordion' formatter for "view" context.
 *
 * @FieldGroupFormatter(
 *   id = "accordion",
 *   label = @Translation("Accordion"),
 *   description = @Translation("This fieldgroup renders child groups as jQuery accordion."),
 *   supported_contexts = {
 *     "form"
 *   }
 * )
 */
class Accordion extends \Drupal\field_group\Plugin\field_group\FieldGroupFormatter\Accordion
{

}
