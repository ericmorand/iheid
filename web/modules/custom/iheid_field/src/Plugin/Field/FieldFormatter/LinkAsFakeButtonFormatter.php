<?php

namespace Drupal\iheid_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'Link as fake_button' formatter.
 *
 * @FieldFormatter(
 *   id = "iheid_field_link_as_fake_button",
 *   label = @Translation("Fake button"),
 *   field_types = {
 *     "link"
 *   },
 * )
 */
class LinkAsFakeButtonFormatter extends LinkAsButtonFormatter
{
    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = parent::viewElements($items, $langcode);

        foreach ($elements as &$element) {
            $element['#theme'] = 'link_formatter_fake_button';
        }

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        $elements = parent::settingsForm($form, $form_state);

        unset($elements['rel']);
        unset($elements['target']);

        return $elements;
    }
}
