<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 22.06.2018
 * Time: 12:30
 */

namespace Drupal\iheid_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldFormatter\DateTimeDefaultFormatter;
use Drupal\datetime_range\Plugin\Field\FieldFormatter\DateRangeDefaultFormatter;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;

/**
 * Plugin implementation of the 'Discrete parts' datetime formatter.
 *
 * @FieldFormatter(
 *   id = "datetime_discrete_parts",
 *   label = @Translation("Discrete parts"),
 *   field_types = {
 *     "datetime"
 *   }
 * )
 */
class DiscretePartsDateTimeFormatter extends DateTimeDefaultFormatter
{
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $element = [];

        /**
         * @var int $delta
         * @var DateRangeItem $item
         */
        foreach ($items as $delta => $item) {
            if ($item->date) {
                /** @var \Drupal\Core\Datetime\DrupalDateTime $date */
                $date = $item->date;
                $separator = $this->getSetting('parts_separator');

                $element[$delta] = [
                    '#theme' => 'datetime_formatter_discrete_parts',
                    '#date' => $this->buildDateWithIsoAttribute($date),
                    '#time' => $this->buildTimeWithIsoAttribute($date),
                    '#parts_separator' => ['#plain_text' => $separator],
                ];
            }
        }

        return $element;
    }

    /**
     * {@inheritdoc}
     */
    /**
     * {@inheritdoc}
     */
    public static function defaultSettings()
    {
        return [
                'time_format_type' => 'medium',
                'parts_separator' => NULL
            ] + parent::defaultSettings();
    }

    /**
     * {@inheritdoc}
     */
    protected function formatTime($date)
    {
        $format_type = $this->getSetting('time_format_type');
        $timezone = $this->getSetting('timezone_override') ?: $date->getTimezone()->getName();
        return $this->dateFormatter->format($date->getTimestamp(), $format_type, '', $timezone != '' ? $timezone : NULL);
    }

    /**
     * Creates a render array from a date object.
     *
     * @param \Drupal\Core\Datetime\DrupalDateTime $date
     *   A date object.
     *
     * @return array
     *   A render array.
     */
    protected function buildTime(DrupalDateTime $date) {
        $this->setTimeZone($date);

        $build = [
            '#markup' => $this->formatTime($date),
            '#cache' => [
                'contexts' => [
                    'timezone',
                ],
            ],
        ];

        return $build;
    }

    /**
     * Creates a render array from a date object with ISO date attribute.
     *
     * @param \Drupal\Core\Datetime\DrupalDateTime $date
     *   A date object.
     *
     * @return array
     *   A render array.
     */
    protected function buildTimeWithIsoAttribute(DrupalDateTime $date)
    {
        // Create the ISO date in Universal Time.
        $iso_date = $date->format("Y-m-d\TH:i:s") . 'Z';

        $this->setTimeZone($date);

        $build = [
            '#theme' => 'time',
            '#text' => $this->formatTime($date),
            '#html' => FALSE,
            '#attributes' => [
                'datetime' => $iso_date,
            ],
            '#cache' => [
                'contexts' => [
                    'timezone',
                ],
            ],
        ];

        return $build;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        $form = parent::settingsForm($form, $form_state);

        unset($form['format_type']);

        $time = new DrupalDateTime();
        $format_types = $this->dateFormatStorage->loadMultiple();
        $options = [];

        foreach ($format_types as $type => $type_info) {
            $format = $this->dateFormatter->format($time->getTimestamp(), $type);
            $options[$type] = $type_info->label() . ' (' . $format . ')';
        }

        $weight = 1;

        $form['format_type'] = [
            '#type' => 'select',
            '#title' => t('Date format'),
            '#description' => t("Choose a format for displaying the date."),
            '#options' => $options,
            '#default_value' => $this->getSetting('format_type'),
            '#weight' => $weight++
        ];

        $form['time_format_type'] = [
            '#type' => 'select',
            '#title' => t('Time format'),
            '#description' => t("Choose a format for displaying the time."),
            '#options' => $options,
            '#default_value' => $this->getSetting('time_format_type'),
            '#weight' => $weight
        ];

        $form['parts_separator'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Parts separator'),
            '#description' => $this->t('The string to separate the parts of the date'),
            '#default_value' => $this->getSetting('parts_separator')
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsSummary()
    {
        $summary = [];

        if ($override = $this->getSetting('timezone_override')) {
            $summary[] = $this->t('Time zone: @timezone', ['@timezone' => $override]);
        }

        $date = new DrupalDateTime();

        $summary[] = t('Date format: @display', ['@display' => $this->formatDate($date)]);
        $summary[] = t('Time format: @display', ['@display' => $this->formatTime($date)]);

        if ($separator = $this->getSetting('parts_separator')) {
            $summary[] = $this->t('Parts separator: %separator', ['%separator' => $separator]);
        }

        return $summary;
    }
}
