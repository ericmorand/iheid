<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 6.07.2018
 * Time: 11:50
 */

namespace Drupal\iheid_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\Plugin\Field\FieldType\UriItem;
use Waynestate\Youtube\ParseId;

/**
 * Plugin implementation of the 'YouTube' formatter.
 *
 * @FieldFormatter(
 *   id = "youtube",
 *   label = @Translation("YouTube"),
 *   field_types = {
 *     "youtube"
 *   }
 * )
 */
class YouTubeFormatter extends FormatterBase
{
    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = [];

        /**
         * @var $delta
         * @var UriItem $item
         */
        foreach ($items as $delta => $item) {
            $elements[$delta] = [
                '#theme' => 'youtube_formatter',
                '#video_id' => ParseId::fromUrl($item->value),
            ];
        }

        return $elements;
    }
}
