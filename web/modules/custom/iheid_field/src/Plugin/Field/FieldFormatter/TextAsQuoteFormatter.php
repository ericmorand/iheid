<?php

namespace Drupal\iheid_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\text\Plugin\Field\FieldFormatter\TextDefaultFormatter;

/**
 * Plugin implementation of the 'Text as quote' formatter.
 *
 * @FieldFormatter(
 *   id = "iheid_field_text_as_quote",
 *   label = @Translation("Quote"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *     "string",
 *     "string_long"
 *   }
 * )
 */
class TextAsQuoteFormatter extends TextDefaultFormatter
{
    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = parent::viewElements($items, $langcode);

        foreach ($elements as &$element) {
            $element['#theme'] = 'text_formatter_quote';
        }

        return $elements;
    }
}
