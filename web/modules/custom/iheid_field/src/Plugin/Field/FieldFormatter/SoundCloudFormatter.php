<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 3.07.2018
 * Time: 18:37
 */

namespace Drupal\iheid_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\Plugin\Field\FieldType\UriItem;

/**
 * Plugin implementation of the 'SoundCloud' formatter.
 *
 * @FieldFormatter(
 *   id = "soundcloud",
 *   label = @Translation("SoundCloud"),
 *   field_types = {
 *     "soundcloud"
 *   }
 * )
 */
class SoundCloudFormatter extends FormatterBase
{
    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = [];

        /**
         * @var $delta
         * @var UriItem $item
         */
        foreach ($items as $delta => $item) {
            $elements[$delta] = [
                '#theme' => 'soundcloud_formatter',
                '#url' => $item->value,
            ];
        }

        return $elements;
    }
}
