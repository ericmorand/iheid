<?php

namespace Drupal\iheid_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Plugin\Field\FieldFormatter\GenericFileFormatter;

/**
 * Plugin implementation of the 'File link as button formatter.
 *
 * @FieldFormatter(
 *   id = "iheid_field_file_formatter_link_as_button",
 *   label = @Translation("File link as button"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class FileLinkAsButtonFormatter extends GenericFileFormatter
{
    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = parent::viewElements($items, $langcode);

        foreach ($elements as $delta => &$element) {
            $elements[$delta]['#theme'] = 'file_formatter_link_as_button';
            $elements[$delta]['#variant'] = $this->getSetting('variant');
        }

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public static function defaultSettings()
    {
        return [
                'variant' => NULL,
            ] + parent::defaultSettings();
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        $elements = parent::settingsForm($form, $form_state);

        $elements['variant'] = [
            '#title' => t('Variant'),
            '#type' => 'select',
            '#default_value' => $this->getSetting('variant'),
            '#options' => [
                'primary' => $this->t('Primary'),
                'secondary' => $this->t('Secondary')
            ],
            '#weight' => -1
        ];

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsSummary()
    {
        $summary = parent::settingsSummary();
        $settings = $this->getSettings();

        $summary[] = t('File link displayed as @variant button', ['@variant' => $settings['variant']]);

        return $summary;
    }
}
