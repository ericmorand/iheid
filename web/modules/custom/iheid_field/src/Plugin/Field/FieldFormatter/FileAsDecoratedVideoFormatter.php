<?php

namespace Drupal\iheid_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileVideoFormatter;

/**
 * Plugin implementation of the 'File as decorated video' formatter.
 *
 * @FieldFormatter(
 *   id = "iheid_field_file_as_decorated_video",
 *   label = @Translation("Decorated video"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class FileAsDecoratedVideoFormatter extends FileVideoFormatter
{
    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = parent::viewElements($items, $langcode);

        foreach ($elements as $delta => &$element) {
            $element['#theme'] = 'file_formatter_decorated_video';
            $element['#decorations'] = $this->getSetting('decorations');
        }

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public static function defaultSettings()
    {
        return [
                'decorations' => NULL,
            ] + parent::defaultSettings();
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        $elements = parent::settingsForm($form, $form_state);

        $options = [
            'polka-dots' => $this->t('Polka Dots'),
            'texture' => $this->t('Texture'),
            'diagonal-lines' => $this->t('Diagonal lines'),
            'diagonal-stripes' => $this->t('Diagonal stripes'),
            'darkener' => $this->t('Darkener'),
            'bamboo' => $this->t('Bamboo'),
            'checkers' => $this->t('Checkers')
        ];

        asort($options);

        $elements['decorations'] = [
            '#title' => t('Decorations'),
            '#type' => 'select',
            '#multiple' => true,
            '#default_value' => $this->getSetting('decorations'),
            '#options' => $options,
            '#weight' => -1
        ];

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsSummary()
    {
        $summary = parent::settingsSummary();

        $settings = $this->getSettings();

        $decorations = $settings['decorations'];

        foreach ($decorations as $decoration) {
            $decorationsLabels[] = $this->t($decoration);
        }

        if (count($decorationsLabels) > 0) {
            $summary[] = $this->t('Decorated with @decorations', ['@decorations' => join(', ', $decorationsLabels)]);
        }

        return $summary;
    }
}
