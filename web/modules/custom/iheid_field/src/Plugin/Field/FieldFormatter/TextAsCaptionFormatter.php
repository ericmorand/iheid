<?php

namespace Drupal\iheid_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\text\Plugin\Field\FieldFormatter\TextDefaultFormatter;

/**
 * Plugin implementation of the 'Text as caption' formatter.
 *
 * @FieldFormatter(
 *   id = "iheid_field_text_as_caption",
 *   label = @Translation("Caption"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *     "string",
 *     "string_long"
 *   }
 * )
 */
class TextAsCaptionFormatter extends TextDefaultFormatter
{
    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = parent::viewElements($items, $langcode);

        foreach ($elements as &$element) {
            $element['#theme'] = 'text_formatter_caption';
        }

        return $elements;
    }
}
