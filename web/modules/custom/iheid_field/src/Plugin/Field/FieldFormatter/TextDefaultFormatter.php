<?php

namespace Drupal\iheid_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;

class TextDefaultFormatter extends \Drupal\text\Plugin\Field\FieldFormatter\TextDefaultFormatter
{
    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = [];
        $parentElements = parent::viewElements($items, $langcode);

        foreach ($parentElements as $delta => &$parentElement) {
            $elements[$delta] = [
                '#theme' => 'text_default_formatter',
                '#value' => $parentElement
            ];
        }

        return $elements;
    }
}
