<?php

namespace Drupal\iheid_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\text\Plugin\Field\FieldFormatter\TextDefaultFormatter;

/**
 * Plugin implementation of the 'Text as heading' formatter.
 *
 * @FieldFormatter(
 *   id = "iheid_field_text_as_heading",
 *   label = @Translation("Heading"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *     "string",
 *     "string_long"
 *   }
 * )
 */
class TextAsHeadingFormatter extends TextDefaultFormatter
{
    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = parent::viewElements($items, $langcode);

        foreach ($elements as &$element) {
            $element['#theme'] = 'text_formatter_heading';
            $element['#level'] = $this->getSetting('level');
            $element['#variant'] = $this->getSetting('variant');
        }

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public static function defaultSettings()
    {
        return [
                'level' => 1,
                'variant' => null
            ] + parent::defaultSettings();
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        $elements = parent::settingsForm($form, $form_state);

        $options = [];

        for ($i = 1; $i <= 6; $i++) {
            $options[$i] = $this->t('Heading @level', ['@level' => $i]);
        }

        $elements['level'] = [
            '#title' => t('level'),
            '#type' => 'select',
            '#default_value' => $this->getSetting('level'),
            '#options' => $options,
            '#weight' => -1
        ];

        $variantOptions = [
            null => $this->t('None'),
            'bland' => $this->t('Bland'),
            'uniform' => $this->t('Uniform'),
        ];

        $elements['variant'] = [
            '#title' => t('Variant'),
            '#type' => 'select',
            '#default_value' => $this->getSetting('variant'),
            '#options' => $variantOptions,
            '#weight' => -1
        ];

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsSummary()
    {
        $summary = parent::settingsSummary();

        $settings = $this->getSettings();

        $summary[] = t('Heading @level', ['@level' => $settings['level']]);

        if ($variant = $settings['variant']) {
            $summary[] = t('Displayed as @variant', ['@variant' => $variant]);
        }

        return $summary;
    }

    /**
     * {@inheritdoc}
     */
    public static function isApplicable(FieldDefinitionInterface $field_definition)
    {
        // not used but for fuck sake it's awesome this thing exists
        return true;
    }
}
