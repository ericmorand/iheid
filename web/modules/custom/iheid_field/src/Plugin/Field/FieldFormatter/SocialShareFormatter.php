<?php

namespace Drupal\iheid_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Config\Config;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Template\Attribute;

/**
 * Plugin implementation of the 'Social share' formatter.
 *
 * @FieldFormatter(
 *   id = "iheid_social_share",
 *   label = @Translation("Social share"),
 *   field_types = {
 *     "social_share"
 *   }
 * )
 */
class SocialShareFormatter extends FormatterBase
{
    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = [];

        foreach ($items as $delta => $item) {
            $network = $item->getValue()['network'];

            $elements[$delta] = [
                '#url' => $item->getUrl()->setAbsolute(),
                '#theme' => 'social_share_formatter',
                '#network' => $network,
                '#title' => $this->t($network),
                '#attributes' => new Attribute(),
                '#social_settings' => \Drupal::config('iheid_social.settings')->get(),
                '#cache' => [
                    'tags' => [
                        'config.iheid_social'
                    ]
                ],
            ];
        }

        return $elements;
    }
}
