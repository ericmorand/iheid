<?php

namespace Drupal\iheid_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'link' formatter.
 *
 * @FieldFormatter(
 *   id = "link",
 *   label = @Translation("Link"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class TelephoneLinkFormatter extends \Drupal\telephone\Plugin\Field\FieldFormatter\TelephoneLinkFormatter {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    foreach ($elements as $delta => &$element) {
        $element['#theme'] = 'telephone_link_formatter';
        $element['#attributes'] = isset($element['#options']['attributes']) ? $element['#options']['attributes'] : [];
    }

    return $elements;
  }
}
