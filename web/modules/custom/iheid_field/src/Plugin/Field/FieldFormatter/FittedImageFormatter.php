<?php

namespace Drupal\iheid_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;

/**
 * Plugin implementation of the 'Fitted image' formatter.
 *
 * @FieldFormatter(
 *   id = "fitted_image",
 *   label = @Translation("Fitted image"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class FittedImageFormatter extends ImageFormatter
{
    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = parent::viewElements($items, $langcode);

        foreach ($elements as $delta => &$element) {
            $element['#theme'] = 'fitted_image_formatter';
            $element['#fit'] = $this->getSetting('fit');
        }

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public static function defaultSettings()
    {
        return [
                'fit' => NULL,
            ] + parent::defaultSettings();
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        $elements = parent::settingsForm($form, $form_state);

        $options = $this->getFitOptions();

        asort($options);

        $elements['fit'] = [
            '#title' => t('Fit mode'),
            '#type' => 'select',
            '#default_value' => $this->getSetting('fit'),
            '#options' => $options,
            '#weight' => -1
        ];

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsSummary()
    {
        $summary = parent::settingsSummary();

        $settings = $this->getSettings();

        $fit = $settings['fit'];

        if ($fit) {
            $summary[] = $this->t('Fit mode: @fit', ['@fit' => $this->getFitOptions()[$fit]]);
        }

        return $summary;
    }

    protected function getFitOptions() {
        return [
            'fill' => $this->t('Fill'),
            'cover' => $this->t('Cover'),
            'contain' => $this->t('Contain'),
            'scale-down' => $this->t('Scale down')
        ];
    }
}
