<?php

namespace Drupal\iheid_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;

/**
 * Plugin implementation of the 'Link as button' formatter.
 *
 * @FieldFormatter(
 *   id = "iheid_field_link_as_button",
 *   label = @Translation("Button"),
 *   field_types = {
 *     "link"
 *   },
 * )
 */
class LinkAsButtonFormatter extends LinkFormatter
{
    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = parent::viewElements($items, $langcode);

        foreach ($elements as &$element) {
            $element['#theme'] = 'link_formatter_button';
            $element['#variant'] = $this->getSetting('variant');
            $element['#attributes'] = $element['#options']['attributes'] ?: [];
        }

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public static function defaultSettings()
    {
        return [
                'variant' => 'primary'
            ] + parent::defaultSettings();
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        $elements = parent::settingsForm($form, $form_state);

        $elements['variant'] = [
            '#title' => t('Variant'),
            '#type' => 'select',
            '#default_value' => $this->getSetting('variant'),
            '#options' => [
                'primary' => $this->t('Primary'),
                'secondary' => $this->t('Secondary'),
                'tertiary' => $this->t('Tertiary'),
                'download' => $this->t('Download')
            ],
            '#weight' => -2
        ];

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsSummary()
    {
        $summary = parent::settingsSummary();

        $settings = $this->getSettings();

        $summary[] = t('Displayed as @variant button', ['@variant' => $settings['variant']]);

        return $summary;
    }
}
