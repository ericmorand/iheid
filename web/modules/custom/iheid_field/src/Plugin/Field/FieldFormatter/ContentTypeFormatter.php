<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 28.06.2018
 * Time: 17:39
 */

namespace Drupal\iheid_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\StringFormatter;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'Content Type' formatter.
 *
 * @FieldFormatter(
 *   id = "content_type",
 *   label = @Translation("Content Type"),
 *   field_types = {
 *     "content_type"
 *   }
 * )
 */
class ContentTypeFormatter extends StringFormatter
{
    /**
     * Generate the output appropriate for one field item.
     *
     * @param \Drupal\Core\Field\FieldItemInterface $item
     *   One field item.
     *
     * @return array
     *   The textual output generated as a render array.
     */
    protected function viewValue(FieldItemInterface $item) {
        return [
            '#theme' => 'content_type_formatter',
            '#variant' => $this->getSetting('variant'),
            '#text' => $this->getSetting('overridden_title')
                ? $this->t($this->getSetting('overridden_title')) : $item->value,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function defaultSettings()
    {
        return [
                'overridden_title' => NULL,
                'variant' => NULL
            ] + parent::defaultSettings();
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        $elements = [];

        $elements['overridden_title'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Override'),
            '#description' => $this->t('The text to override the name of the content type with'),
            '#default_value' => $this->getSetting('overridden_title'),
        ];

        $elements['variant'] = [
            '#type' => 'select',
            '#title' => $this->t('Variant'),
            '#options' => $this->getVariantOptions(),
            '#default_value' => $this->getSetting('variant'),
        ];

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsSummary()
    {
        $summary = [];

        if ($overridden_title = $this->getSetting('overridden_title')) {
            $summary[] = $this->t('Title overridden to "%overridden_title"', [
                '%overridden_title' => $overridden_title
            ]);
        }

        if ($variant = $this->getSetting('variant')) {
            $summary[] = $this->t('Displayed as @variant', [
                '@variant' => $this->getVariantOptions()[$variant]
            ]);
        }

        return $summary;
    }

    protected function getVariantOptions()
    {
        return [
            null => $this->t('None'),
            'badge' => $this->t('Badge')
        ];
    }
}
