<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 28.06.2018
 * Time: 17:39
 */

namespace Drupal\iheid_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\iheid_field\TearOffCalendarSheetTrait;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;

/**
 * Plugin implementation of the 'Tear-of calendar sheet' formatter.
 *
 * @FieldFormatter(
 *   id = "tear_off_calendar_sheet_formatter",
 *   label = @Translation("Tear-off calendar sheet"),
 *   field_types = {
 *     "visual_date"
 *   }
 * )
 */
class TearOffCalendarSheetDateFormatter extends ImageFormatter
{
    use TearOffCalendarSheetTrait;

    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = parent::viewElements($items, $langcode);

        foreach ($elements as $delta => &$element) {
            $element['#theme'] = 'tear_off_calendar_sheet_formatter';
            $element['#date_parts'] = $this->explodeDate($items[$delta]->date, NULL, $langcode);
        }

        return $elements;
    }
}
