<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 22.06.2018
 * Time: 12:30
 */

namespace Drupal\iheid_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldFormatter\DateTimeDefaultFormatter;
use Drupal\datetime_range\Plugin\Field\FieldFormatter\DateRangeDefaultFormatter;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;

/**
 * Plugin implementation of the 'Discrete parts' daterange formatter.
 *
 * @FieldFormatter(
 *   id = "daterange_discrete_parts",
 *   label = @Translation("Discrete parts"),
 *   field_types = {
 *     "daterange"
 *   }
 * )
 */
class DiscretePartsDateRangeFormatter extends DiscretePartsDateTimeFormatter
{
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $element = [];

        /**
         * @var int $delta
         * @var DateRangeItem $item
         */
        foreach ($items as $delta => $item) {
            if (!empty($item->start_date) && !empty($item->end_date)) {
                /** @var \Drupal\Core\Datetime\DrupalDateTime $startDate */
                $startDate = $item->start_date;
                /** @var \Drupal\Core\Datetime\DrupalDateTime $endDate */
                $endDate = $item->end_date;
                $separator = $this->getSetting('separator');
                $partsSeparator = $this->getSetting('parts_separator');

                $element[$delta] = [
                    '#theme' => 'daterange_formatter_discrete_parts',
                    '#start_date' => $this->buildDateWithIsoAttribute($startDate),
                    '#start_time' => $this->buildTimeWithIsoAttribute($startDate),
                    '#end_date' =>  $this->buildDateWithIsoAttribute($endDate),
                    '#end_time' => $this->buildTimeWithIsoAttribute($endDate),
                    '#separator' => ['#plain_text' => $separator],
                    '#parts_separator' => ['#plain_text' => $partsSeparator],
                    '#interval' => $startDate->diff($endDate)->days
                ];
            }
        }

        return $element;
    }

    /**
     * {@inheritdoc}
     */
    /**
     * {@inheritdoc}
     */
    public static function defaultSettings()
    {
        return [
                'separator' => NULL,
            ] + parent::defaultSettings();
    }

    /**
     * {@inheritdoc}
     */
    protected function formatTime($date)
    {
        $format_type = $this->getSetting('time_format_type');
        $timezone = $this->getSetting('timezone_override') ?: $date->getTimezone()->getName();
        return $this->dateFormatter->format($date->getTimestamp(), $format_type, '', $timezone != '' ? $timezone : NULL);
    }

    /**
     * Creates a render array from a date object.
     *
     * @param \Drupal\Core\Datetime\DrupalDateTime $date
     *   A date object.
     *
     * @return array
     *   A render array.
     */
    protected function buildTime(DrupalDateTime $date) {
        $this->setTimeZone($date);

        $build = [
            '#markup' => $this->formatTime($date),
            '#cache' => [
                'contexts' => [
                    'timezone',
                ],
            ],
        ];

        return $build;
    }

    /**
     * Creates a render array from a date object with ISO date attribute.
     *
     * @param \Drupal\Core\Datetime\DrupalDateTime $date
     *   A date object.
     *
     * @return array
     *   A render array.
     */
    protected function buildTimeWithIsoAttribute(DrupalDateTime $date)
    {
        // Create the ISO date in Universal Time.
        $iso_date = $date->format("Y-m-d\TH:i:s") . 'Z';

        $this->setTimeZone($date);

        $build = [
            '#theme' => 'time',
            '#text' => $this->formatTime($date),
            '#html' => FALSE,
            '#attributes' => [
                'datetime' => $iso_date,
            ],
            '#cache' => [
                'contexts' => [
                    'timezone',
                ],
            ],
        ];

        return $build;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        $form = parent::settingsForm($form, $form_state);

        $form['separator'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Date separator'),
            '#description' => $this->t('The string to separate the start and end dates'),
            '#default_value' => $this->getSetting('separator')
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsSummary()
    {
        $summary = parent::settingsSummary();

        if ($separator = $this->getSetting('separator')) {
            $summary[] = $this->t('Separator: %separator', ['%separator' => $separator]);
        }

        return $summary;
    }
}
