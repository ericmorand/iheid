<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 28.06.2018
 * Time: 17:39
 */

namespace Drupal\iheid_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\iheid_field\TearOffCalendarSheetTrait;
use Drupal\responsive_image\Plugin\Field\FieldFormatter\ResponsiveImageFormatter;

/**
 * Plugin implementation of the 'Responsive tear-of calendar sheet' formatter.
 *
 * @FieldFormatter(
 *   id = "responsive_tear_off_calendar_sheet_formatter",
 *   label = @Translation("Responsive tear-off calendar sheet"),
 *   field_types = {
 *     "visual_date"
 *   }
 * )
 */
class ResponsiveTearOffCalendarSheetDateFormatter extends ResponsiveImageFormatter
{
    use TearOffCalendarSheetTrait;

    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = parent::viewElements($items, $langcode);

        foreach ($elements as $delta => &$element) {
            $element['#theme'] = 'responsive_tear_off_calendar_sheet_formatter';
            $element['#date_parts'] = $this->explodeDate($items[$delta]->date, NULL, $langcode);
        }

        return $elements;
    }
}
