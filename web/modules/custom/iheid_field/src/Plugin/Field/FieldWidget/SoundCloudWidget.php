<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 4.07.2018
 * Time: 19:36
 */

namespace Drupal\iheid_field\Plugin\Field\FieldWidget;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldWidget;
use Drupal\Core\Field\Plugin\Field\FieldWidget\UriWidget;

/**
 * Plugin implementation of the 'soundcloud' widget.
 *
 * @FieldWidget(
 *   id = "soundcloud_default",
 *   label = @Translation("SoundCloud"),
 *   field_types = {
 *     "soundcloud"
 *   }
 * )
 */
class SoundcloudWidget extends UriWidget
{
}
