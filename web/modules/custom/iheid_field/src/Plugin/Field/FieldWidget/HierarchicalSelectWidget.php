<?php

namespace Drupal\iheid_field\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\Annotation\FieldWidget;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy\TermStorage;

/**
 * Plugin implementation of the 'hierarchical_select' widget.
 *
 * @FieldWidget(
 *   id = "hierarchical_select",
 *   label = @Translation("Hierarchical select"),
 *   field_types = {
 *     "entity_reference"
 *   },
 * )
 */
class HierarchicalSelectWidget extends OptionsWidgetBase
{
    /**
     * {@inheritdoc}
     */
    public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state)
    {
        $element = parent::formElement($items, $delta, $element, $form, $form_state);

        $targetId = null;
        $targetTerm = null;
        $parentTerm = null;

        $parentTermId = null;
        $childTermId = null;

        $fieldDefinition = $this->fieldDefinition;
        $fieldName = $fieldDefinition->getName();
        $parentOptions = $this->getParentOptions($items);

        /** @var boolean $inCallback */
        $inCallback = ($triggeringElement = $form_state->getTriggeringElement()) && ($triggeringElement['#parents'][0] === $fieldName) && ($triggeringElement['#parents'][2] === 'parent');

        if ($inCallback) {
            // if we are in a callback, retrieve the parent term id from the triggering - i.e. the "parent" - element
            $parentTermId = $triggeringElement['#value'];
        }
        else {
            // if we are not in a callback and the item value is set, retrieve the target term id from the item value and set the parent term id
            /** @var EntityReferenceItem $item */
            if ($item = $items[$delta]) {
                if (isset($item->getValue()['target_id'])) {
                    $targetId = $item->getValue()['target_id'];

                    if ($targetId) {
                        if ($parentTerm = $this->getParentTerm($targetId)) {
                            $parentTermId = $parentTerm->id();
                            $childTermId = $targetId;
                        } else {
                            $parentTermId = $targetId;
                        }
                    }
                }
            }

            // if the item value is not set, use the first available parent as parent term
            if (!$parentTermId) {
                $parentTermId = key($parentOptions);
            }
        }

        if (!$parentTerm) {
            $parentTerm = Term::load($parentTermId);
        }

        $childOptions = $this->getChildOptions($items->getEntity(), $parentTerm);

        $ajaxWrapperId = Html::getId($fieldName . '-wrapper-' . $delta);

        $element['parent'] = [
            '#type' => 'select',
            '#options' => $parentOptions,
            '#default_value' => $parentTermId,
            '#ajax' => [
                'callback' => [$this, 'ajaxCallback'],
                'event' => 'change',
                'wrapper' => $ajaxWrapperId,
                'progress' => [
                    'type' => 'throbber'
                ],
            ],
            '#delta' => $delta,
            '#prefix' => '<div id="' . $ajaxWrapperId . '">',
            '#weight' => 0
        ];

        $element['child'] = [
            '#type' => 'select',
            '#options' => $childOptions,
            '#default_value' => $childTermId,
            '#weight' => 2,
            '#suffix' => '</div>',
        ];

        $element += [
            '#attached' => [
                'library' => ['iheid_field/hierarchical_select']
            ]
        ];

        if ($fieldDefinition->getFieldStorageDefinition()->getCardinality() == 1) {
            $element += [
                '#type' => 'fieldset',
            ];
        }

        return $element;
    }

    public function ajaxCallback(array $form, FormStateInterface $form_state)
    {
        $triggeringElement = $form_state->getTriggeringElement();
        $delta = $triggeringElement['#delta'];

        $element = $form[$this->fieldDefinition->getName()];
        $elements = $element['widget'][$delta];

        return [
            $elements['parent'],
            $elements['child']
        ];
    }

    /**
     * @param int $termId
     * @return Term
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     */
    protected function getParentTerm($termId) {
        /** @var Term $result */
        $result = null;

        /** @var TermStorage $termStorage */
        $termStorage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');

        $parents = $termStorage->loadParents($termId);
        $result = current($parents);

        return $result;
    }

    protected function getParentOptions(FieldItemListInterface $items)
    {
        $results = [];

        $terms = $this->getOptions($items->getEntity());

        /** @var TermStorage $termStorage */
        $termStorage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');

        foreach ($terms as $id => $label) {
            $parents = $termStorage->loadParents($id);

            /** @var TermInterface $parent */
            if ($parent = current($parents)) {
                $results[$parent->id()] = $parent->getName();
            }
        }

        return $results;
    }

    /**
     * @param FieldableEntityInterface $entity
     * @param Term $parentTerm
     * @return array
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     */
    protected function getChildOptions(FieldableEntityInterface $entity, $parentTerm)
    {
        $parentTermId = $parentTerm->id();

        $results = [
            $parentTermId => '- ' . $parentTerm->getName() . ' -'
        ];

        $options = parent::getOptions($entity);

        /** @var TermStorage $termStorage */
        $termStorage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');

        foreach ($options as $id => $label) {
            $parents = $termStorage->loadParents($id);

            if (array_key_exists($parentTermId, $parents)) {
                $results[$id] = Term::load($id)->getName();
            }
        }

        return $results;
    }

    /**
     * Form validation handler for widget elements.
     *
     * @param array $element
     *   The form element.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The form state.
     */
    public static function validateElement(array $element, FormStateInterface $form_state)
    {
        $targetId = $element['child']['#value'];

        $form_state->setValueForElement($element, [
            $element['#key_column'] => $targetId
        ]);
    }

    public static function isApplicable(FieldDefinitionInterface $field_definition)
    {
        if (strpos($field_definition->getSetting('handler'), 'taxonomy_term') === FALSE) {
            return FALSE;
        }

        return parent::isApplicable($field_definition); // TODO: Change the autogenerated stub
    }
}
