<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 6.07.2018
 * Time: 12:21
 */

namespace Drupal\iheid_field\Plugin\Field\FieldWidget;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldWidget;
use Drupal\Core\Field\Plugin\Field\FieldWidget\UriWidget;

/**
 * Widget implementation of the 'youtube' field type.
 *
 * @FieldWidget(
 *   id = "youtube_default",
 *   label = @Translation("YouTube"),
 *   description = @Translation("A YouTube video player widget."),
 *   field_types = {
 *     "youtube",
 *   }
 * )
 */
class YoutubeWidget extends UriWidget
{

}
