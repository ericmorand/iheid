<?php

namespace Drupal\iheid_field\Plugin\Field\FieldItemList;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Item list for a computed field that displays the entity title.
 *
 * @see \Drupal\iheid_field\Plugin\Field\FieldType\TitleItem
 */
class TitleItemList extends FieldItemList
{
    use ComputedItemListTrait;

    /**
     * {@inheritdoc}
     */
    public function setValue($values, $notify = TRUE) {
        parent::setValue($values, $notify);

        // Make sure that subsequent getter calls do not try to compute the values
        // again.
        $this->valueComputed = TRUE;
    }

    /**
     * Computes the values for an item list.
     */
    protected function computeValue()
    {
        /** @var \Drupal\node\Entity\Node $entity */
        $entity = $this->getEntity();
        $currentLanguageCode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();

        if ($entity->hasTranslation($currentLanguageCode)) {
            $entity = $entity->getTranslation($currentLanguageCode);
        }

        $this->list[0] = $this->createItem(0, $entity->label());

        $this->valueComputed = TRUE;
    }
}
