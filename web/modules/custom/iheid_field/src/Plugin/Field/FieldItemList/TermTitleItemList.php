<?php

namespace Drupal\iheid_field\Plugin\Field\FieldItemList;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\taxonomy\Entity\Term;

/**
 * Item list for a computed field that displays the entity title.
 *
 * @see \Drupal\iheid_field\Plugin\Field\FieldType\TitleItem
 */
class TermTitleItemList extends FieldItemList
{
    use ComputedItemListTrait;

    /**
     * Computes the values for an item list.
     */
    protected function computeValue()
    {
        /** @var Term $entity */
        $entity = $this->getEntity();
        $currentLanguageCode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();

        if ($entity->hasTranslation($currentLanguageCode)) {
            $entity = $entity->getTranslation($currentLanguageCode);
        }

        $this->list[0] = $this->createItem(0, $entity->label());

        $this->valueComputed = TRUE;
    }
}
