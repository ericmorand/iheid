<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 28.06.2018
 * Time: 17:45
 */

namespace Drupal\iheid_field\Plugin\Field\FieldItemList;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Item list for a computed field that displays highlighted project collaborator list.
 */
class ContentTypeItemList extends FieldItemList
{
    use ComputedItemListTrait;

    /**
     * Computes the values for an item list.
     */
    protected function computeValue()
    {
        /** @var \Drupal\node\Entity\Node $entity */
        $entity = $this->getEntity();

        $currentLanguageCode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();

        if ($entity->hasTranslation($currentLanguageCode)) {
            $entity = $entity->getTranslation($currentLanguageCode);
        }

        $this->list[0] = $this->createItem(0, $entity->getType());

        $this->valueComputed = TRUE;
    }
}
