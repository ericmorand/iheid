<?php

namespace Drupal\iheid_field\Plugin\Field\FieldItemList;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Item list for a computed field that displays social share links.
 */
class SocialShareItemList extends FieldItemList
{
    use ComputedItemListTrait;

    /**
     * Computes the values for an item list.
     */
    protected function computeValue()
    {
        /** @var \Drupal\node\Entity\Node $entity */
        $entity = $this->getEntity();

        $currentLanguageCode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();

        if ($entity->hasTranslation($currentLanguageCode)) {
            $entity = $entity->getTranslation($currentLanguageCode);
        }

        // at this point, the entity may not have been saved yet
        // since we need to get its URI and an id is needed for this,
        // we have to check if the entity has an id before going further
        if ($entity->id()) {
            $uri = $entity->toUrl()->toUriString();

            // todo: make this configurable
            $supportedNetworks = [
                'facebook',
                'twitter',
                'linkedin'
            ];

            foreach ($supportedNetworks as $supportedNetwork) {
                $this->list[] = $this->createItem(0, [
                    'uri' => $uri,
                    'network' => $supportedNetwork
                ]);
            }

            $this->valueComputed = TRUE;
        }
    }
}