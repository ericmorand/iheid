<?php

namespace Drupal\iheid_field\Plugin\Field\FieldItemList;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\file\FileStorage;

/**
 * Item list for a computed field that displays the entity title.
 *
 * @see \Drupal\iheid_field\Plugin\Field\FieldType\TitleItem
 */
class VisualDateItemList extends EntityReferenceFieldItemList
{
    use ComputedItemListTrait;

    /**
     * Computes the values for an item list.
     */
    protected function computeValue()
    {
        $file = null;

        /** @var FileStorage $fileStorage */
        $fileStorage = \Drupal::entityTypeManager()->getStorage('file');

        /** @var FileSystem $fileSystemService */
        $fileSystemService = \Drupal::service('file_system');

        $uri = 'public://' . $fileSystemService->basename('iheid_field_visual_date_frame.png');

        if (!file_exists($uri)) {
            $img = imagecreatetruecolor(1, 1);
            imagesavealpha($img, true);
            $color = imagecolorallocatealpha($img, 0, 0, 0, 127);
            imagefill($img, 0, 0, $color);

            ob_start();
            imagepng($img);
            $fileData = ob_get_clean();

            $data = array(
                'status' => FILE_STATUS_PERMANENT,
                'uri' => file_unmanaged_save_data($fileData, $uri)
            );

            $file = $fileStorage->create($data);

            $file->save();
        }
        else {
            $file = current($fileStorage->loadByProperties(['uri' => $uri]));
        }

        // grab event start date
        /** @var \Drupal\node\Entity\Node $entity */
        $entity = $this->getEntity();
        $currentLanguageCode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();

        if ($entity->hasTranslation($currentLanguageCode)) {
            $entity = $entity->getTranslation($currentLanguageCode);
        }

        $this->list[0] = $this->createItem(0, [
            'entity' => $file,
            'date' => new \DateTime($entity->get('field_start_end_date')->value)
        ]);

        $this->valueComputed = TRUE;
    }
}
