<?php

namespace Drupal\iheid_field\Plugin\Field\FieldItemList;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;

/**
 * Item list for a computed field that displays the node logos.
 */
class LogosItemList extends EntityReferenceFieldItemList
{
    use ComputedItemListTrait;

    protected function getSecondaryLogoTerm()
    {
        /** @var Node $node */
        $node = $this->getEntity();
        $term = null;

        if ($node) {
            /** @var Media $logo */
            $categoryId = null;

            if ($node->hasField('field_main_category')) {
                $categoryId = $node->get('field_main_category')->target_id;

                if ($categoryId) {
                    $term = iheid_taxonomy_get_closest_term_with_logo($categoryId);
                }
            }
        }

        return $term;
    }

    /**
     * Computes the values for an item list.
     */
    protected function computeValue()
    {
        $offset = 0;
        $config = \Drupal::config('iheid_settings.brand_settings');

        // main logos
        $smallLogo = null;
        $largeLogo = null;

        if ($smallLogoId = $config->get('logo_small')) {
            $smallLogo = Media::load($smallLogoId);

            $this->list[] = $this->createItem($offset++, $smallLogo);
        }

        if ($largeLogoId = $config->get('logo_large')) {
            $largeLogo = Media::load($largeLogoId);

            $this->list[] = $this->createItem($offset++, $largeLogo);
        }

        // secondary logo
        $secondaryLogo = null;
        $secondaryLogoTerm = $this->getSecondaryLogoTerm();

        if ($secondaryLogoTerm) {
            $secondaryLogo = iheid_taxonomy_get_term_logo($secondaryLogoTerm);
        }

        if (!$secondaryLogo) {
            if ($secondaryLogoId = $config->get('logo_secondary')) {
                $secondaryLogo = Media::load($secondaryLogoId);
            }
        }

        if ($secondaryLogo) {
            $this->list[] = $this->createItem($offset++, $secondaryLogo);
        }

        $this->valueComputed = TRUE;
    }
}
