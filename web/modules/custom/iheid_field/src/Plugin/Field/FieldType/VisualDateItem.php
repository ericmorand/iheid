<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 28.06.2018
 * Time: 17:20
 */

namespace Drupal\iheid_field\Plugin\Field\FieldType;

use Drupal\image\Plugin\Field\FieldType\ImageItem;

/**
 * Plugin implementation of the 'add_to_calendar' field type.
 *
 * @FieldType(
 *   id = "visual_date",
 *   label = @Translation("Visual date"),
 *   description = @Translation("This field displays informations about content types."),
 *   category = @Translation("Reference"),
 *   default_formatter = "tear_off_calendar_sheet_formatter",
 *   no_ui = TRUE
 * )
 */
class VisualDateItem extends ImageItem
{

}
