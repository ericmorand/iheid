<?php
/**
 * Created by PhpStorm.
 * User: ericmorand
 * Date: 27/02/18
 * Time: 14:25
 */

namespace Drupal\iheid_field\Plugin\Field\FieldType;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldType;
use Drupal\link\Plugin\Field\FieldType\LinkItem;

/**
 * Defines the 'share' entity field item.
 *
 * @FieldType(
 *   id = "social_share",
 *   label = @Translation("Social share"),
 *   description = @Translation("A social share field."),
 *   default_widget = "iheid_social_share",
 *   default_formatter = "iheid_social_share",
 *   no_ui = TRUE
 * )
 */
class SocialShareItem extends LinkItem
{

}