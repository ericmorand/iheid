<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 28.06.2018
 * Time: 17:20
 */

namespace Drupal\iheid_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItem;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'content_type' field type.
 *
 * @FieldType(
 *   id = "content_type",
 *   label = @Translation("Content Type"),
 *   description = @Translation("This field displays informations about content types."),
 *   category = @Translation("Reference"),
 *   default_formatter = "content_type"
 * )
 */
class ContentTypeItem extends StringItem
{

}
