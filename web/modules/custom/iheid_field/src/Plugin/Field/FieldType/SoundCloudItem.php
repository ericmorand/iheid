<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 4.07.2018
 * Time: 18:02
 */

namespace Drupal\iheid_field\Plugin\Field\FieldType;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldType;
use Drupal\Core\Field\Plugin\Field\FieldType\UriItem;

/**
 * Defines the 'soundcloud' entity field item.
 *
 * @FieldType(
 *   id = "soundcloud",
 *   label = @Translation("SoundCloud"),
 *   description = @Translation("A field containing a SoundCloud URL."),
 *   category = @Translation("General"),
 *   default_widget = "soundcloud_default",
 *   default_formatter = "soundcloud",
 * )
 */
class SoundcloudItem extends UriItem
{
}
