<?php
/**
 * Created by PhpStorm.
 * User: serhat
 * Date: 6.07.2018
 * Time: 11:16
 */

namespace Drupal\iheid_field\Plugin\Field\FieldType;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldType;
use Drupal\Core\Field\Plugin\Field\FieldType\UriItem;

/**
 * Plugin implementation of the 'YouTube' field type.
 *
 * @FieldType(
 *   id = "youtube",
 *   label = @Translation("YouTube"),
 *   description = @Translation("An entity field containing a YouTube URL."),
 *   category = @Translation("General"),
 *   default_formatter = "youtube",
 *   default_widget = "youtube_default",
 * )
 */
class YouTubeItem extends UriItem
{

}
