<?php
namespace Drupal\iheid_block\Plugin\Block;

use Drupal\Core\Annotation\ContextDefinition;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Entity\EntityDisplayRepository;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * This block renders the current node.
 *
 * @Block(
 *   id = "current_node",
 *   admin_label = @Translation("Current node"),
 *   context = {
 *     "node" = @ContextDefinition("entity:node", label = @Translation("Node"))
 *   }
 * )
 */
class CurrentNodeBlock extends BlockBase implements BlockPluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state)
    {
        $form = parent::blockForm($form, $form_state);
        $options = [];

        /** @var EntityDisplayRepository $entityDisplayRepository */
        $entityDisplayRepository = \Drupal::service('entity_display.repository');
        $viewModes = $entityDisplayRepository->getViewModes('node');

        foreach ($viewModes as $name => $viewMode) {
            $options[$name] = $viewMode['label'];
        }

        $form['view_mode'] = [
            '#title' => t('View mode'),
            '#type' => 'select',
            '#options' => $options,
            '#default_value' => $this->configuration['view_mode'],
            '#description' => t('Select the view mode the banner image should be rendered with.')
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state)
    {
        parent::blockSubmit($form, $form_state);

        $this->configuration['view_mode'] = $form_state->getValue('view_mode');
    }

    /**
     * Builds and returns the renderable array for this block plugin.
     *
     * If a block should not be rendered because it has no content, then this
     * method must also ensure to return no content: it must then only return an
     * empty array, or an empty array with #cache set (with cacheability metadata
     * indicating the circumstances for it being empty).
     *
     * @return array
     *   A renderable array representing the content of the block.
     *
     * @see \Drupal\block\BlockViewBuilder
     */
    public function build()
    {
        $renderArray = [];

        /** @var Node $node */
        $node = $this->getContextValue('node');

        if ($node) {
            /** @var EntityViewBuilder $viewBuilder */
            $viewBuilder = \Drupal::entityTypeManager()->getViewBuilder('node');
            $viewMode = $this->configuration['view_mode'];

            $renderArray['content'] = $viewBuilder->view($node, $viewMode);
        }

        return $renderArray;
    }
}
