const fs = require('fs-extra');
const Promise = require('promise');
const fsEmptyDir = Promise.denodeify(fs.emptyDir);

let cleanPromises = [
    fsEmptyDir('tmp'),
    fsEmptyDir('build')
];

let start = () => {
    return Promise.all(cleanPromises);
};

start().then();