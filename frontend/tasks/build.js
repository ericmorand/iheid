const AssetsBuilder = require('../lib/assets-builder');

let start = async () => {
    let deliverable;
    let config = require('../config/build');

    if (process.argv[2]) {
        deliverable = process.argv[2];

        config.componentManifest = `component-${deliverable}.json`;
    }
    else {
        deliverable = 'index';

        config.componentManifest = `component.json`;
    }

    let assetsBuilder = new AssetsBuilder(config, deliverable, 'build');

    await assetsBuilder.start();
};

start().then(
    () => {
        console.log('');
    }
);
