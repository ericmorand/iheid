const fs = require('fs-extra');
const path = require('path');
const merge = require('merge');

const Promise = require('promise');
const fsCopy = Promise.denodeify(fs.copy);
const fsRemove = Promise.denodeify(fs.remove);

let config = {
    deployPath: '../web/themes/custom/iheid_theme'
};

if (fs.existsSync(path.resolve('config.local.js'))) {
    config = merge.recursive(true, config, require(path.resolve('config.local.js')));
}

let deployPath = config.deployPath;

let assetsBuildPath = path.join('build', 'assets');
let assetsDeployPath = path.join(deployPath, 'assets');
let stylesheetsBuildPath = path.join('build', 'css');
let stylesheetsDeployPath = path.join(deployPath, 'css');
let scriptsBuildPath = path.join('build', 'js');
let scriptsDeployPath = path.join(deployPath, 'js');
let templatesBuildPath = path.join('build', 'templates');
let templatesDeployPath = path.join(deployPath, 'templates');

let cleanPromises = [
    fsRemove(assetsDeployPath),
    fsRemove(stylesheetsDeployPath),
    fsRemove(scriptsDeployPath),
    fsRemove(templatesDeployPath)
];

return Promise.all(cleanPromises).then(
    function() {
        let copyPromises = [
            fsCopy(assetsBuildPath, assetsDeployPath),
            fsCopy(stylesheetsBuildPath, stylesheetsDeployPath),
            fsCopy(scriptsBuildPath, scriptsDeployPath),
            fsCopy(templatesBuildPath, templatesDeployPath)
        ];

        return Promise.all(copyPromises);
    }
);