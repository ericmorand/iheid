let fs = require('fs-extra');

if (!fs.pathExistsSync('./.yo-rc.json')) {
    fs.copySync('./yo-rc.json', './.yo-rc.json');
}

