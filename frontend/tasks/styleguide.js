const fs = require('fs-extra');
const Stromboli = require('../lib/stromboli/stromboli');
const TwigProcessor = require('../lib/iheid-processor-twig');
const AssetsBuilder = require('../lib/assets-builder');
const {IHEIDLoaderFilesystem} = require('../lib/twing/loader/filesystem');

let writer = require('../lib/writer');

/**
 *
 * @param {Array<StromboliComponent>} components
 */
let checkComponents = (components) => {
    let errors = [];

    for (let component of components) {
        let renderResponses = component.getRenderResponses();

        for (let [pluginName, renderResponse] of renderResponses) {
            let renderError = renderResponse.getError();

            if (renderError) {
                errors.push(`${errors.length + 1} [${component.getName()}, ${pluginName}]: ${renderError.getMessage()}`);
            }
        }
    }

    if (errors.length > 0) {
        throw new Error(`${errors.length} ${errors.length > 1 ? 'errors' : 'error'} occurred while building the style guide:\n${errors.join('\n')}`);
    }
};

let start = async () => {
    fs.emptyDirSync('styleguide');

    let twigProcessorConfig = require('../config/processor/twig');

    // step 1: build final assets
    let config = require('../config/styleguide');

    let assetsBuilder = new AssetsBuilder(config, 'index', 'styleguide');

    let components = await assetsBuilder.start();

    checkComponents(components);

    // step 2: build demo indexes
    let builder = new Stromboli();

    config = {
        componentRoot: 'test',
        componentManifest: 'component.json',
        plugins: {
            html: {
                entry: 'index.html.twig',
                processors: [
                    new TwigProcessor(twigProcessorConfig)
                ]
            }
        }
    };

    let outputPath = 'styleguide/components';

    components = await builder.start(config);

    checkComponents(components);

    // todo: when Twing supports source maps, replace this by source map rebasing
    for (let component of components) {
        let binary = component.getRenderResponses().get('html').getBinaries()[0];

        let binaryString = binary.getData().toString();
        let dotDot = '../'.repeat(component.getName().split('/').length + 1);

        binaryString = binaryString.replace('href="index.css?', 'href="' + dotDot + 'css/index.css?');
        binaryString = binaryString.replace('src="index.js?', 'src="' + dotDot + 'js/index.js?');

        binary.setData(Buffer.from(binaryString));
    }

    await writer.writeComponents(components, outputPath, false);

    // step 3: build styleguide index
    class StyleGuideTwigProcessor extends TwigProcessor {
        getData(file) {
            return Promise.resolve(this.config.renderData);
        }
    }

    let styleGuideTwigProcessor = new StyleGuideTwigProcessor(twigProcessorConfig);

    config = {
        componentRoot: 'lib/styleguide',
        componentManifest: 'component.json',
        plugins: {
            html: {
                entry: 'index.html.twig',
                processors: [
                    styleGuideTwigProcessor
                ]
            },
            css: {
                entry: 'index.scss',
                processors: [
                    new (require('../lib/iheid-processor-sass'))(require('../config/processor/sass')),
                    new (require('../lib/stromboli/stromboli-processor-css-rebase'))()
                ]
            },
            js: {
                entry: 'index.js',
                processors: [
                    new (require('../lib/iheid-processor-javascript'))(require('../config/processor/javascript'))
                ]
            }
        }
    };

    let componentsData = components.map(function (component) {
        return {
            name: component.getName()
        }
    });

    styleGuideTwigProcessor.config.renderData = {
        components: componentsData
    };

    builder.start(config).then(
        (styleGuideComponents) => {
            return writer.writeComponents(styleGuideComponents, '.');
        }
    );
};

start()
    .then(() => {
        console.log('Style guide built with success!');
    })
    .catch((err) => {
        process.exitCode = 1;

        console.error(err);
    })
;