/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/paragraph/grid',
        testCleanName: 'entity--paragraph--grid',
        data: require('./test.data')()
    };
};
