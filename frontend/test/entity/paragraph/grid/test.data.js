const {Entity, Field} = require('../../../../lib/drupal/index');
const fileDownload = require('../file-download/test.data')();

class Grid extends Entity {
    constructor() {
        super('paragraph', 'grid', [
            new Field('entity-reference', 'grid', 'Cells')
        ]);
    }
}

module.exports = () => {
    let entity = new Grid()
        .set('grid', [
            fileDownload.entity,
            fileDownload.entity
        ])
    ;

    return {
        Grid: Grid,
        entity: entity
    };
};