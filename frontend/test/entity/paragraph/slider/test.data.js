const {Entity, Field} = require('../../../../lib/drupal/index');
const quoteParagraphTest = require('../quote/test.data')();

class SliderParagraph extends Entity {
    constructor() {
        super('paragraph', 'slider', [
            new Field('entity-reference', 'slider', 'Slides'),
            new Field('string', 'title', 'Title')
        ]);
    }
}

module.exports = () => {
    let entity = new SliderParagraph()
        .set('slider', [
            quoteParagraphTest.entity,
            quoteParagraphTest.entity,
            quoteParagraphTest.entity
        ])
        .set('title', 'Testimonials')
    ;

    return {
        SliderParagraph: SliderParagraph,
        entity: entity
    };
};