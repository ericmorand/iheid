/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/paragraph/slider',
        testCleanName: 'entity--paragraph--slider',
        data: require('./test.data')()
    };
};
