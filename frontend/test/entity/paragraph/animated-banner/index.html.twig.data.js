/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/paragraph/animated-banner',
        testCleanName: 'entity--paragraph--animated-banner',
        data: require('./test.data')()
    }
};
