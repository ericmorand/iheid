const {Entity, Field, FieldFormatter, FormattedField, FieldGroup, ViewMode} = require('../../../../lib/drupal/index');
const {VideoMedia} = require('../../../entity/media/video/test.data.js')();

class AnimatedBanner extends Entity {
    constructor() {
        super('paragraph', 'animated-banner', [
            new Field('string', 'title', 'Title'),
            new Field('text-with-summary', 'body', 'body'),
            new Field('link', 'buttons', 'Buttons'),
            new Field('entity-reference', 'video', 'Video')
        ]);
    }
}

module.exports = () => {
    let animatedBanner = new AnimatedBanner()
        .set('title', 'Welcome to Graduate Institute, we glad to see you on our new website')
        .set('body', '<p>Discover the latest news from the Institute<br>and select your profile to access the most relevant content for you</p>')
        .set('buttons', [
            {
                url: '/',
                title: 'Student Services'
            },
            {
                url: '/',
                title: 'Exchange'
            },
            {
                url: '/',
                title: 'Student life '
            }
        ])
        .set('video', new VideoMedia()
            .set('field_video', {
                title: 'IHEID.mp4',
                src: 'http://demo.emakina.ch/iheid/test-resources/Institute%20(General)%20720360%20COMP%203.1.mp4',
                type: 'video/mp4',
            })
        )
    ;

    return {
        AnimatedBanner: AnimatedBanner,
        entity: animatedBanner
    }
};