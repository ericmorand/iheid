const {Entity, Field} = require('../../../../lib/drupal/index');
const {ImageMedia} = require('../../media/image/mocks');

class ImageParagraph extends Entity {
    constructor() {
        super('paragraph', 'image', [
            new Field('string', 'caption', 'Caption'),
            new Field('entity-reference', 'image', 'Image'),
        ]);
    }
}

// lorem ipsum
let loremIpsum = new ImageParagraph()
    .set('caption', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit')
    .set('image', new ImageMedia()
        .set('field_image', {
            src: 'https://picsum.photos/1920/1080?image=550',
            width: '1920px',
            height: '1080px'
        })
    )
;

module.exports = {
    ImageParagraph: ImageParagraph,
    loremIpsum: loremIpsum
};