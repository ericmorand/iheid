/**
 *
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function () {
    return {
        testName: 'entity/paragraph/image',
        testCleanName: 'entity--paragraph--image',
        data: require('./test.data')()
    };
};
