const {loremIpsum} = require('./mocks');

module.exports = () => {
    return {
        entity: loremIpsum
    };
};