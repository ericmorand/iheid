/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/paragraph/teaser',
        testCleanName: 'entity--paragraph--teaser',
        data: require('./test.data')()
    };
};
