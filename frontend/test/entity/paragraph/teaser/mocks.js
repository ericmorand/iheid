const {Entity, Field} = require('../../../../lib/drupal/index');

class TeaserParagraph extends Entity {
    constructor() {
        super('paragraph', 'teaser', [
            new Field('entity_reference', 'field_content', 'Content')
        ]);
    }
}

module.exports = {
    TeaserParagraph: TeaserParagraph
};
