const {TeaserParagraph} = require('./mocks');
const mocks = require('../../node/event/mocks');

let entity = new TeaserParagraph();

entity.set('field_content', mocks["Killer robots in the battlefield and the alleged accountability gap for war crimes"]);

module.exports = () => {
    return {
        entity: entity
    };
};