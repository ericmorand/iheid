/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/paragraph/related-content',
        testCleanName: 'entity--paragraph--related-content',
        data: require('./test.data')()
    };
};
