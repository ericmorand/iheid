const {Entity, Field} = require('../../../../lib/drupal/index');
const {RelatedContentField} = require('../../../field/type/related-content/mocks');

class RelatedContentParagraph extends Entity {
    constructor() {
        super('paragraph', 'related_content', [
            new Field('string', 'title', 'Title'),
            new RelatedContentField('related_content', 'Content')
        ]);
    }
}

module.exports = {
    RelatedContentParagraph: RelatedContentParagraph
};
