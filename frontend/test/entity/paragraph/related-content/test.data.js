const {RelatedContentParagraph} = require('./mocks');
const eventMocks = require('../../node/event/mocks');
const newsMocks = require('../../node/news/mocks');
const collaboratorMocks = require('../../node/collaborator/mocks');

let entity = new RelatedContentParagraph();

entity
    .set('title', 'Related content')
    .set('related_content', [
        {
            entity: eventMocks["Killer robots in the battlefield and the alleged accountability gap for war crimes"]
        },
        {
            entity: collaboratorMocks["Galileo Galilei"]
        },
        {
            entity: collaboratorMocks["Marie Curie"],
            active: true
        },
        {
            entity: newsMocks["Student story: Fighting for youth empowerment"]
        }
    ])
;

module.exports = () => {
    return {
        entity: entity
    };
};