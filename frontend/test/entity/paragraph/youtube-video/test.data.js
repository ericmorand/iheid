const {Entity, Field} = require('../../../../lib/drupal/index');
const {YouTubeField} = require('../../../field/type/youtube/mocks');

class YouTubeVideoParagraph extends Entity {
    constructor() {
        super('paragraph', 'youtube_video', [
            new YouTubeField('field_youtube', 'Video URL'),
            new Field('string', 'field_caption', 'Caption')
        ]);
    }
}

/**
 *
 * @returns {{StaticBanner: StaticBanner, entity: {StaticBanner}, view_modes}}
 */
module.exports = () => {
    let entity = new YouTubeVideoParagraph()
        .set('field_youtube', 'https://www.youtube.com/watch/5sQO-jPg6bE')
        .set('field_caption', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit 21 february 2018')
    ;

    return {
        YouTubeVideoParagraph: YouTubeVideoParagraph,
        entity: entity
    }
};