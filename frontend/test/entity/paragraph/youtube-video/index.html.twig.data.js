/**
 *
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function () {
    return {
        testName: 'entity/paragraph/youtube-video',
        testCleanName: 'entity--paragraph--youtube-video',
        data: require('./test.data')()
    };
};
