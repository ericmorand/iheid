const {Entity, Field} = require('../../../../lib/drupal/index');
const {ThreeCardsField} = require('../../../field/type/three-cards/mocks');

class RecommendedContentParagraph extends Entity {
    constructor() {
        super('paragraph', 'recommended_content', [
            new Field('string', 'title', 'Title'),
            new Field('link', 'field_show_all_link', 'Show all link'),
            new ThreeCardsField('recommended_content', 'Content')
        ]);
    }
}

module.exports = {
    RecommendedContentParagraph: RecommendedContentParagraph
};
