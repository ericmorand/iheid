/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/paragraph/recommended-content',
        testCleanName: 'entity--paragraph--recommended-content',
        data: require('./test.data')()
    };
};
