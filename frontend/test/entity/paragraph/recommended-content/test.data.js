const {RecommendedContentParagraph} = require('./mocks');

let entity = new RecommendedContentParagraph();

entity
    .set('title', 'Recommended content')
    .set('field_show_all_link', {
        url: '#',
        title: 'Show all'
    })
    .set('recommended_content', {
        ws_url: '3'
    })
;

module.exports = () => {
    return {
        entity: entity
    };
};