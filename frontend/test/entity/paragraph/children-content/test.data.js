const {ChildrenContentParagraph} = require('./mocks');
const {field: childrenContentField} = require('../../../field/type/children-content/test.data')();

let entity = new ChildrenContentParagraph();

entity
    .set('title', 'Academic departments')
    .set('children_content', childrenContentField.values)
;

module.exports = () => {
    return {
        entity: entity
    };
};