/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/paragraph/children-content',
        testCleanName: 'entity--paragraph--children-content',
        data: require('./test.data')()
    };
};
