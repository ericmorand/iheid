const {Entity, Field} = require('../../../../lib/drupal/index');
const {ChildrenContentField} = require('../../../field/type/children-content/mocks');

class ChildrenContentParagraph extends Entity {
    constructor() {
        super('paragraph', 'children_content', [
            new Field('string', 'title', 'Title'),
            new ChildrenContentField('children_content', 'Content')
        ]);
    }
}

module.exports = {
    ChildrenContentParagraph: ChildrenContentParagraph
};
