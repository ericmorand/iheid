const {Entity, Field} = require('../../../../lib/drupal/index');
const {ThreeCardsField} = require('../../../field/type/three-cards/mocks');

class NewsletterParagraph extends Entity {
    constructor() {
        super('paragraph', 'newsletter', [
            new Field('string', 'field_title', 'Title'),
        ]);
    }
}

let newsOfTheWeek = new NewsletterParagraph()
    .set('field_title', 'Subscribe to the institute’s « News of the week » newsletter')
;

module.exports = {
    NewsletterParagraph: NewsletterParagraph,
    newsOfTheWeek: newsOfTheWeek
};
