/**
 *
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function () {
    return {
        testName: 'entity/paragraph/newsletter',
        testCleanName: 'entity--paragraph--newsletter',
        data: require('./test.data')()
    };
};
