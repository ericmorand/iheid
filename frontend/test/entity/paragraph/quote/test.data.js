const {Entity, Field} = require('../../../../lib/drupal/index');
const {ImageMedia} = require('../../media/image/mocks');

class QuoteParagraph extends Entity {
    constructor() {
        super('paragraph', 'quote', [
            new Field('entity-reference', 'image', 'Image'),
            new Field('string', 'quote', 'Quote'),
            new Field('string', 'caption', 'Caption'),
            new Field('string', 'author', 'Author')
        ]);
    }
}

module.exports = () => {
    let entity = new QuoteParagraph()
        .set('image', new ImageMedia()
            .set('field_image', {
                src: 'https://picsum.photos/240?image=69',
                width: '240px',
                height: '240px'
            })
        )
        .set('caption', 'Professor, international relations/political science Curt Gasteyger chair in international security and conflict studies')
        .set('quote', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.')
        .set('author', 'Thomas J. Biersteker')
    ;

    return {
        QuoteParagraph: QuoteParagraph,
        entity: entity
    };
};
