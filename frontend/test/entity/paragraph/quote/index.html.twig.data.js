/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = function () {
    return {
        testName: 'entity/paragraph/quote',
        testCleanName: 'entity--paragraph--quote',
        data: require('./test.data')()
    };
};
