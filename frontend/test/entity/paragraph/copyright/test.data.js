const {CopyrightParagraph} = require('./mocks');

let entity = new CopyrightParagraph();

entity.set('field_text', 'Lorem ipsum dolor sit amet.');

module.exports = () => {
    return {
        entity: entity
    };
};