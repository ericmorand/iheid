/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/paragraph/copyright',
        testCleanName: 'entity--paragraph--copyright',
        data: require('./test.data')()
    };
};
