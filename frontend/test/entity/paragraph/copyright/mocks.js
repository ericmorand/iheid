const {Entity, Field} = require('../../../../lib/drupal/index');
const {ImageMedia} = require('../../media/image/mocks');

class CopyrightParagraph extends Entity {
    constructor() {
        super('paragraph', 'copyright', [
            new Field('string', 'field_text', 'Text')
        ]);
    }
}

module.exports = {
    CopyrightParagraph: CopyrightParagraph
};