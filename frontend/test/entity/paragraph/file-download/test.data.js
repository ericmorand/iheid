const {Entity, Field} = require('../../../../lib/drupal/index');
const {ImageMedia} = require('../../../entity/media/image/mocks.js');

class FileDownload extends Entity {
    constructor() {
        super('paragraph', 'file-download', [
            new Field('string', 'title', 'Title'),
            new Field('string', 'department', 'Department'),
            new Field('link', 'link', 'Link'),
            new Field('entity-reference', 'image', 'Image')
        ]);
    }
}

module.exports = () => {
    let paragraph = new FileDownload()
        .set('title', 'Alternative Pathways to Sustainable Development: Lessons from Latin America')
        .set('department', 'Department')
        .set('link', {
            url: 'https://journals.openedition.org/poldev/2333',
            title: 'Download'
        })
        .set('image', new ImageMedia()
            .set('field_image', {
                src: 'http://journals.openedition.org/poldev/docannexe/file/2381/couv_poldev_sustain_dvp_latin_am_2_1_-small500.jpg'
            })
        )
    ;

    return {
        FileDownload: FileDownload,
        entity: paragraph
    }
};