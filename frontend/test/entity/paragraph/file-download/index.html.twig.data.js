/**
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/paragraph/file-download',
        testCleanName: 'entity--paragraph--file-download',
        data: require('./test.data')()
    };
};
