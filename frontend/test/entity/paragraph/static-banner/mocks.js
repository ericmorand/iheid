const {Entity, Field} = require('../../../../lib/drupal/index');
const {ImageMedia} = require('../../media/image/mocks');

class StaticBannerParagraph extends Entity {
    constructor() {
        super('paragraph', 'static-banner', [
            new Field('string', 'field_top_header', 'Top header'),
            new Field('string', 'field_header', 'Header'),
            new Field('string', 'field_description', 'Description'),
            new Field('link', 'field_link', 'Link'),
            new Field('entity-reference', 'field_image', 'Image')
        ]);
    }
}

// the geneva challenge mock
let genevaChallenge = new StaticBannerParagraph()
    .set('field_top_header', 'Study')
    .set('field_header', 'The Geneva Challenge 2018')
    .set('field_description', 'The Advancing Development Goals International Contest for Graduate Students')
    .set('field_link', {
        url: 'http://example.com',
        title: 'Apply now'
    })
    .set('field_image', new ImageMedia()
        .set('field_image', {
            src: 'https://picsum.photos/1440/720?image=1067',
            width: 1440,
            height: 720
        })
    )
;

module.exports = {
    StaticBannerParagraph: StaticBannerParagraph,
    genevaChallenge: genevaChallenge
};