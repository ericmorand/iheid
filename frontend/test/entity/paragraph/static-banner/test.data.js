const {genevaChallenge} = require('./mocks');

/**
 *
 * @returns {{entity: {StaticBannerParagraph}}}
 */
module.exports = () => {
    return {
        entity: genevaChallenge
    }
};