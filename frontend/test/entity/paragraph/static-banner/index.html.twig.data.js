/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/paragraph/static-banner',
        testCleanName: 'entity--paragraph--static-banner',
        data: require('./test.data')()
    };
};
