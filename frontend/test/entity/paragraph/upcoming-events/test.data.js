const {UpcomingEventsParagraph} = require('./mocks');

let entity = new UpcomingEventsParagraph();

entity
    .set('title', 'Upcoming events')
    .set('field_show_all_link', {
        url: '#',
        title: 'Show all'
    })
    .set('upcoming_events', {
        ws_url: '3'
    })
;

module.exports = () => {
    return {
        entity: entity
    };
};