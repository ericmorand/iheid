/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/paragraph/upcoming-events',
        testCleanName: 'entity--paragraph--upcoming-events',
        data: require('./test.data')()
    };
};
