const {Entity, Field} = require('../../../../lib/drupal/index');
const {SoundCloudField} = require('../../../field/type/soundcloud/mocks');

class SoundCloudParagraph extends Entity {
    constructor() {
        super('paragraph', 'soundcloud', [
            new Field('string', 'title', 'Title'),
            new SoundCloudField('field_soundcloud', 'Track URL')
        ]);
    }
}

module.exports = () => {
    let entity = new SoundCloudParagraph()
        .set('title', 'The Geneva Challenge 2018')
        .set('field_soundcloud', 'https://soundcloud.com/the-bugle/bugle-179-playas-gon-play')
    ;

    return {
        SoundCloudParagraph: SoundCloudParagraph,
        entity: entity
    };
};