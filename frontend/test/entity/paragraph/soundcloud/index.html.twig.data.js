/**
 * @returns {{testName: string, testCleanName: string, data: object}}
 */
module.exports = function () {
    return {
        testName: 'entity/paragraph/soundcloud',
        testCleanName: 'entity--paragraph--soundcloud',
        data: require('./test.data')()
    };
};
