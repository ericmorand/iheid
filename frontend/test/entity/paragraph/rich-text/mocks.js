const {Entity, Field} = require('../../../../lib/drupal/index');

class RichTextParagraph extends Entity {
    constructor() {
        super('paragraph', 'rich_text', [
            new Field('text-with-summary', 'body', 'Body')
        ]);
    }
}

// lorem ipsum mock
let loremIpsum = new RichTextParagraph()
    .set('body', '<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet</h3><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>')
;

module.exports = {
    RichTextParagraph: RichTextParagraph,
    loremIpsum: loremIpsum
};