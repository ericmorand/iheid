/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/paragraph/rich-text',
        testCleanName: 'entity--paragraph--rich-text',
        data: require('./test.data')()
    };
};
