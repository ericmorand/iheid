const {Entity, Field} = require('../../../../lib/drupal/index');
const {CategorySliderField} = require('../../../field/type/category-slider/mocks');
const {field: categorySliderField} = require('../../../field/type/category-slider/test.data')();

class CategorySliderParagraph extends Entity {
    constructor() {
        super('paragraph', 'category_slider', [
            new Field('string', 'title', 'Title'),
            new Field('link', 'field_show_all_link', 'Show all link'),
            new CategorySliderField('category_slider', 'Content')
        ]);
    }
}

let academicDepartments = new CategorySliderParagraph();

academicDepartments
    .set('title', 'Academic departments')
    .set('field_show_all_link', {
        url: '#',
        title: 'Show all'
    })
    .set('category_slider', categorySliderField.values)
;

module.exports = {
    CategorySliderParagraph: CategorySliderParagraph,
    academicDepartments: academicDepartments
};
