/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/paragraph/category-slider',
        testCleanName: 'entity--paragraph--category-slider',
        data: require('./test.data')()
    };
};
