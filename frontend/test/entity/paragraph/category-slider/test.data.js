const {academicDepartments} = require('./mocks');

module.exports = () => {
    return {
        entity: academicDepartments
    };
};