const {Entity, Field} = require('../../../../lib/drupal/index');
const {ThreeCardsField} = require('../../../field/type/three-cards/mocks');

class MostRecentContentParagraph extends Entity {
    constructor() {
        super('paragraph', 'most_recent_content', [
            new Field('string', 'title', 'Title'),
            new Field('link', 'field_show_all_link', 'Show all link'),
            new ThreeCardsField('most_recent_content', 'Content')
        ]);
    }
}

module.exports = {
    MostRecentContentParagraph: MostRecentContentParagraph
};
