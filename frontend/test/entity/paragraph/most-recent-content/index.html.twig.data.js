/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/paragraph/most-recent-content',
        testCleanName: 'entity--paragraph--most-recent-content',
        data: require('./test.data')()
    };
};
