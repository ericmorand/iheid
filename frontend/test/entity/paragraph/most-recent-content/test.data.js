const {MostRecentContentParagraph} = require('./mocks');

let entity = new MostRecentContentParagraph();

entity
    .set('title', 'Most recent content')
    .set('field_show_all_link', {
        url: '#',
        title: 'Show all'
    })
    .set('most_recent_content', {
        ws_url: '3'
    })
;

module.exports = () => {
    return {
        entity: entity
    };
};