/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/node/news',
        testCleanName: 'entity--node--news',
        data: require('./test.data')()
    };
};
