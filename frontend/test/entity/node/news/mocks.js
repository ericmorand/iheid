const {Field} = require('../../../../lib/drupal/index');
const {ImageMedia} = require('../../media/image/mocks');
const {Node} = require('../basic/mocks');
const {loremIpsum} = require('../../../entity/paragraph/rich-text/mocks');
const {genevaChallenge} = require('../../../entity/paragraph/static-banner/mocks');
const collaboratorMocks = require('../../../entity/node/collaborator/mocks');
const {ProjectCollaboratorField} =  require('../../../field/type/project-collaborator/mocks');
const {HighlightedProjectCollaboratorsField} =  require('../../../field/type/highlighted-project-collaborators/mocks');
const sinon = require('sinon');

class NewsNode extends Node {
    constructor() {
        // stub the computed fields
        let highlightedProjectCollaboratorField = new HighlightedProjectCollaboratorsField('highlighted_project_collaborators');

        sinon.stub(highlightedProjectCollaboratorField, 'values').get(function() {
            let values = [];
            let collaborators = this.entity.get('field_authors').values;

            for (let collaborator of collaborators) {
                values.push({
                    collaborator: collaborator
                });
            }

            return values;
        });

        super('news', [
            new Field('string', 'news_name', 'News Name'), // 0
            new Field('entity_reference', 'news_main_image', 'Main image'), // 1
            new Field('date', 'news_publication_date', 'Date'), //3
            new ProjectCollaboratorField('field_authors', 'Authors'),
            new Field('string', 'field_top_header', 'Category'), // 8
            new Field('entity_reference', 'field_editorial_content', 'Editorial content'),
            new ProjectCollaboratorField('field_project_collaborators', 'Contributors'),
            highlightedProjectCollaboratorField
        ]);

        // default banner image
        this.set('field_banner_image', new ImageMedia()
            .set('field_image', {
                src: 'https://picsum.photos/3504/2336?image=91',
                type: 'image/jpeg'
            })
        )
    }
}

module.exports = {
    NewsNode: NewsNode,
    'Student story: Fighting for youth empowerment': new NewsNode()
        .set('news_name', 'Student story: Fighting for youth empowerment')
        .set('title', 'Student story: Fighting for youth empowerment')
        .set('field_top_header', 'Governance')
        .set('news_publication_date', new Date(2018, 5, 24, 16, 7))
        .set('body', "The Foundation is a private foundation that receives subsidies from the Swiss Confederation and the Canton of Geneva to fulfill its public service mandate.")
        .set('field_authors', [
            collaboratorMocks["Thomas J. Biersteker"],
        ])
        // todo: should be in SocialShareField
        .set('social_share', require('../../../field/formatter/social-share/test.data')().values)
        .set('news_main_image', new ImageMedia()
            .set('field_image', {
                src: 'https://picsum.photos/4896/3264?image=882',
                type: 'image/jpeg'
            })
        )
        .set('field_editorial_content', [
            loremIpsum,
            genevaChallenge
        ])
};
