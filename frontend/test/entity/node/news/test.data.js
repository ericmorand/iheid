const mocks = require('./mocks');

module.exports = () => {
    return {
        entity: mocks["Student story: Fighting for youth empowerment"],
    };
};
