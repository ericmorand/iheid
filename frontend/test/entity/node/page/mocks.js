const {Field} = require('../../../../lib/drupal/index');
const {ImageMedia, createImageMedia} = require('../../media/image/mocks');
const {Node} = require('../basic/mocks');
const {entity: imageParagraph} = require('../../paragraph/image/test.data')();
const {entity: richTextParagraph} = require('../../paragraph/rich-text/test.data')();
const {entity: relatedContentParagraph} = require('../../paragraph/related-content/test.data')();
const {genevaChallenge} = require('../../paragraph/static-banner/mocks');

class PageNode extends Node {
    constructor() {
        super('page', [
            new Field('string', 'page_name', 'Page Name'),
            new Field('entity_reference', 'page_bg_image', 'Page Background image'),
            new Field('string', 'page_introduction', 'Page Introduction'),
            new Field('string', 'page_author', 'Author'),
            new Field('entity_reference', 'field_main_content', 'Main content'),
            new Field('entity_reference', 'field_secondary_content', 'Secondary content'),
            new Field('entity_reference', 'field_editorial_content', 'Editorial content'),
            new Field('string', 'page_top_header', 'Category')
        ]);

        // default banner image
        this.set('field_banner_image', new ImageMedia()
            .set('field_image', {
                src: 'https://picsum.photos/5398/3599?image=431',
                type: 'image/jpeg'
            })
        )
    }
}

let theEdgarDePicciottoInternationalPrize = new PageNode();

theEdgarDePicciottoInternationalPrize
    .set('title', 'The Edgar de Picciotto International Prize')
    .set('page_introduction', "The Edgar de Picciotto International Prize was created as a tribute and token of thanks to Mr Edgar de Picciotto. With their donation of CHF 20 million which has enabled us to finance a considerable part of the Student House, Mr de Picciotto and his family have greatly supported and participated in the realisation of our mission by facilitating the hosting of students coming from all over the world.")
    .set('page_author', "Andrew Liptak")
    .set('social_share', require('../../../field/formatter/social-share/test.data')().values)
    .set('field_main_content', [
        imageParagraph,
        richTextParagraph
    ])
    .set('field_secondary_content', [
        relatedContentParagraph
    ])
    .set('field_editorial_content', [
        genevaChallenge
    ])
    .set('page_top_header', 'Lorem ipsum')
;

let financialAid = new PageNode()
    .set('title', 'Financial aid')
    .set('page_introduction', "The Institute invests considerable resources in financial aid, with the goal of attracting talented scholars from around the world, regardless of origin and financial ability.")
    .set('field_banner_image', createImageMedia('https://picsum.photos/4000/2667?image=885'))
    .set('page_author', "Andrew Liptak")
    .set('social_share', require('../../../field/formatter/social-share/test.data')().values)
    .set('field_main_content', [
        imageParagraph,
        richTextParagraph
    ])
    .set('field_secondary_content', [
        relatedContentParagraph
    ])
    .set('field_editorial_content', [
        genevaChallenge
    ])
    .set('page_top_header', 'Lorem ipsum')
;

let meetTheDirector = new PageNode()
    .set('title', 'Meet the director')
    .set('page_introduction', "Welcome to the Graduate Institute ! We are delighted by your interest in our areas of specialisation, international relations and development studies.")
    .set('field_banner_image', createImageMedia('http://100anssteiner.ch/sites/default/files/img/03_partner_burrin_01_1280x640_150311_0_0_0.jpg'))
    .set('page_author', "Andrew Liptak")
    .set('social_share', require('../../../field/formatter/social-share/test.data')().values)
    .set('field_main_content', [
        imageParagraph,
        richTextParagraph
    ])
    .set('field_secondary_content', [
        relatedContentParagraph
    ])
    .set('field_editorial_content', [
        genevaChallenge
    ])
    .set('page_top_header', 'Lorem ipsum')
;

let campusDeLaPaix = new PageNode()
    .set('title', 'Campus de la Paix')
    .set('page_introduction', "The Campus de la paix is a network of buildings extending from Place des Nations to the shores of Lake Geneva, spanning two public parks - Parc Mon Repos and Parc Rigot.")
    .set('field_banner_image', createImageMedia('http://graduateinstitute.ch/files/live/sites/iheid/files/sites/about-us/campus-de-la-paix/campus_air_660_345.jpg'))
    .set('page_author', "Andrew Liptak")
    .set('social_share', require('../../../field/formatter/social-share/test.data')().values)
    .set('field_main_content', [
        imageParagraph,
        richTextParagraph
    ])
    .set('field_secondary_content', [
        relatedContentParagraph
    ])
    .set('field_editorial_content', [
        genevaChallenge
    ])
    .set('page_top_header', 'Lorem ipsum')
;

module.exports = {
    PageNode: PageNode,
    'The Edgar de Picciotto International Prize': theEdgarDePicciottoInternationalPrize,
    'Financial aid': financialAid,
    'Meet the director': meetTheDirector,
    'Campus de la Paix': campusDeLaPaix
};
