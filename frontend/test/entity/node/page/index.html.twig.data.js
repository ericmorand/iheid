/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/node/page',
        testCleanName: 'entity--node--page',
        data: require('./test.data')()
    };
};
