const {Field} = require('../../../../lib/drupal/index');
const {ImageMedia} = require('../../media/image/mocks');
const {Node} = require('../basic/mocks');
const collaboratorEntity = require('../../node/collaborator/test.data')();
const paragraphEntity = require('../../paragraph/grid/test.data')();

class CourseNode extends Node {
    constructor() {
        super('course', [
            new Field('string', 'course_name', 'Course Name'), // 0
            new Field('string', 'course_top_header','Course Top Hedaer'), // 3
            new Field('entity-reference', 'course_professors','Course Professors'), // 4
            new Field('string', 'course_introduction', 'Course Introduction'), //5
            new Field('link', 'syllabus_cta', 'Syllabus CTA'), //6
            new Field('link', 'support_material_cta', 'Support Material CTA'), //7
            new Field('entity-reference', 'editorial_content', 'Blocks'), //9
            new Field('string', 'semester', 'Semester'), // 11
            new Field('string', 'course_code', 'Course Code'), // 12
            new Field('string', 'credits', 'Credits'), // 12
            new Field('string', 'course_sem_code_cred', 'Credits'), // 13
        ]);

        // default banner image
        this.set('field_banner_image', new ImageMedia()
            .set('field_image', {
                src: 'https://picsum.photos/5184/3456?image=201',
                type: 'image/jpeg'
            })
        )
    }
}

module.exports = {
    CourseNode: CourseNode,
    'Alternative pathways to sustainable development: lessons from Latin America': new CourseNode()
        .set('title', 'Alternative pathways to sustainable development: lessons from Latin America')
        .set('course_top_header', 'Department')
        .set('course_professors', [
            collaboratorEntity.entity
        ])
        //collaborators: require('../../../field/type/project-collaborator/test.data')().positions[0].collaborators,
        .set('course_introduction', "Vestibulum rutrum quam vitae fringilla tincidunt. Suspendisse nec tortor urna. Ut laoreet sodales nisi, quis iaculis nulla iaculis vitae. Donec sagittis faucibus lacus eget blandit. Mauris vitae ultricies metus, at condimentum nulla. Donec quis ornare lacus. Sed sapien metus, scelerisque nec pharetra id, tempor a tortor. Pellentesque non dignissim neque. Ut porta viverra est, ut dignissim elit elementum.<br/><br/>Vestibulum rutrum quam vitae fringilla tincidunt. Suspendisse nec tortor urna. Ut laoreet sodales nisi, quis iaculis nulla iaculis vitae. Donec sagittis faucibus lacus eget blandit. Mauris vitae ultricies metus, at condimentum nulla. <br/><br/>© The Graduate Institute, Geneva 2018")
        .set('syllabus_cta', {
            url: 'http://graduateinstitute.ch/directory/_/people/biersteker',
            title: 'Syllabus'
        })
        .set('support_material_cta', {
            url: 'http://graduateinstitute.ch/directory/_/people/biersteker',
            title: 'Support Material'
        })
        .set('social_share', require('../../../field/formatter/social-share/test.data')().values)
        .set('editorial_content', [
            paragraphEntity.entity
        ])
        .set('course_image', new ImageMedia()
            .set('field_image', {
                src: 'https://scooget.com/assets/cover_photo_placeholder_2x.jpg',
                type: 'image/jpeg'
            })
        )
        .set('semester', "Autumn")
        .set('course_code', "IA095")
        .set('credits', "6ECTS")
        .set('course_sem_code_cred', "IA095 | Autumn | 6ECTS")
};
