/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/node/course',
        testCleanName: 'entity--node--course',
        data: require('./test.data')()
    };
};
