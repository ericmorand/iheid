const mocks = require('./mocks');

module.exports = () => {
    return {
        entity: mocks["Alternative pathways to sustainable development: lessons from Latin America"],
    };
};
