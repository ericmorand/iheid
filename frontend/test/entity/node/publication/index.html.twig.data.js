/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/node/publication',
        testCleanName: 'entity--node--publication',
        data: require('./test.data')()
    };
};
