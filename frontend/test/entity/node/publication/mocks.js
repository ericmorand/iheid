const {Field} = require('../../../../lib/drupal/index');
const {ImageMedia} = require('../../media/image/mocks');
const {Node} = require('../basic/mocks');const collaboratorMocks = require('../../../entity/node/collaborator/mocks');
const sinon = require('sinon');

class PublicationNode extends Node {
    constructor() {
        // stub computed field
        let computedDateRangeField = new Field('date_range', 'field_computed_start_end_date', 'Computed date range');

        sinon.stub(computedDateRangeField, 'values').get(function() {
            return this.entity.get('field_start_end_date').values;
        });

        super('publication', [
            new Field('string', 'title', 'Title'),
            new Field('entity_reference', 'field_main_image', 'Main image'),
            new Field('string', 'field_top_header', 'Top header'),
            new Field('entity_reference', 'field_author', 'Authors'),
            new Field('datetime', 'field_publication_date', 'Publication date'),
            new Field('text-with-summary', 'body', 'Introduction'),
            new Field('link', 'field_cta', 'CTA'),
        ]);

        // default banner image
        this.set('field_banner_image', new ImageMedia()
            .set('field_image', {
                src: 'https://picsum.photos/4000/3000?image=528',
                type: 'image/jpeg'
            })
        )
    }
}

module.exports = {
    PublicationNode: PublicationNode,
    'Alternative pathways to sustainable development': new PublicationNode()
        .set('title', 'Alternative pathways to sustainable development')
        .set('field_top_header', 'Department')
        .set('body', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque.In volutpat ante semper diam molestie, et aliquam erat.')
        .set('field_publication_date', new Date(2018, 1, 21, 17, 30))
        .set('field_cta', {
            url: 'http://graduateinstitute.ch/directory/_/people/biersteker',
            title: 'Order'
        })
        .set('field_main_image', new ImageMedia()
            .set('field_image', {
                src: 'http://journals.openedition.org/poldev/docannexe/file/2381/couv_poldev_sustain_dvp_latin_am_2_1_-small500.jpg',
                type: 'image/jpeg'
            })
        )
        .set('field_author', collaboratorMocks["Enrico Letta"])
        .set('social_share', require('../../../field/formatter/social-share/test.data')().values)
};
