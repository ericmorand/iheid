/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/node/contact',
        testCleanName: 'entity--node--contact',
        data: require('./test.data')()
    };
};
