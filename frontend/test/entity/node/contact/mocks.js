const {Field} = require('../../../../lib/drupal/index');
const {createImageMedia} = require('../../media/image/mocks');
const {Node} = require('../basic/mocks');
const collaboratorMocks = require('../../node/collaborator/mocks');

class ContactNode extends Node {
    constructor() {
        super('contact', [
            new Field('entity_reference', 'field_collaborators', 'collaborators'),
        ]);

        // default banner image
        this.set('field_banner_image', createImageMedia('https://picsum.photos/5398/3599?image=431'));
    }
}

let generalInformation = new ContactNode();

generalInformation
    .set('title', 'General information')
    .set('field_collaborators', [
        collaboratorMocks["Sophie Fleury"]
    ])
;

let mediaEnquiries = new ContactNode();

mediaEnquiries
    .set('title', 'Media enquiries')
    .set('field_collaborators', [
        collaboratorMocks["Sophie Fleury"],
        collaboratorMocks["Dougal Thompson"]
    ])
;

module.exports = {
    ContactNode: ContactNode,
    mediaEnquiries: mediaEnquiries,
    generalInformation: generalInformation
};
