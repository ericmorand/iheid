const {mediaEnquiries} = require('./mocks');

module.exports = () => {
    return {
        entity: mediaEnquiries,
    };
};