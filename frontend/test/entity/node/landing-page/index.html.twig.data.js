/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/node/landing-page',
        testCleanName: 'entity--node--landing-page',
        data: require('./test.data')()
    };
};
