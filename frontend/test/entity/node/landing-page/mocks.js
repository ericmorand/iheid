const {Field} = require('../../../../lib/drupal/index');
const {createImageMedia} = require('../../media/image/mocks');
const {Node} = require('../basic/mocks');
const {genevaChallenge} = require('../../paragraph/static-banner/mocks');
const {academicDepartments} = require('../../paragraph/category-slider/mocks');

class LandingPageNode extends Node {
    constructor() {
        super('landing_page', [
            new Field('entity_reference', 'field_editorial_content', 'Editorial content'),
            new Field('string', 'field_secondary_content_title', 'Secondary content title'),
            new Field('entity_reference', 'field_secondary_content', 'Secondary content'),
        ]);

        // default banner image
        this.set('field_banner_image', createImageMedia('https://picsum.photos/5398/3599?image=600'))
    }
}

let discoverTheInstitute = new LandingPageNode();

discoverTheInstitute
    .set('title', 'Discover the institute')
    .set('body', '<p>The Graduate Institute is an institution of research and higher education (Master and PhD). Selective and cosmopolitan, it is located in the heart of International Geneva and specialises in the study of the major global, international and transnational challenges facing the contemporary world. It also offers professional development programmes and expertise to international actors from the public, private and non-profit sectors.</p>')
    .set('field_editorial_content', [
        genevaChallenge
    ])
    .set('field_secondary_content_title', 'You may also be interested in…')
    .set('field_secondary_content', [
        academicDepartments,
        academicDepartments,
        academicDepartments
    ])
;

module.exports = {
    PageNode: LandingPageNode,
    discoverTheInstitute: discoverTheInstitute
};
