/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/node/collaborator',
        testCleanName: 'entity--node--collaborator',
        data: require('./test.data')()
    };
};
