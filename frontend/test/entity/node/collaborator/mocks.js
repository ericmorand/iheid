const {Field} = require('../../../../lib/drupal/index');
const {ImageMedia} = require('../../media/image/mocks');
const {Node} = require('../basic/mocks');
const {createTerm} = require('../../term/mocks');
const sinon = require('sinon');

class CollaboratorNode extends Node {
    constructor() {
        super('collaborator', [
            new Field('entity-reference', 'main_image', 'Main image'),
            new Field('string', 'department', 'Department'),
            new Field('string', 'position', 'Position'),
            new Field('link', 'profile_cta', 'CTA'),
            new Field('string', 'field_phone', 'Phone'),
            new Field('string', 'languages', 'Languages Spoken'),
            new Field('string', 'expertise', 'Expertise'),
            new Field('social_profile', 'field_social_profile', 'Social profiles'),
        ]);
    }
}

module.exports = {
    CollaboratorNode: CollaboratorNode,
    'Thomas J. Biersteker': new CollaboratorNode()
        .set('title', 'Thomas J. Biersteker')
        .set('department', 'Department')
        .set('position', 'Professor, international relations/political science Curt Gasteyger chair in international security and conflict studies')
        .set('profile_cta', {
            url: 'http://graduateinstitute.ch/directory/_/people/biersteker',
            title: 'Profile page'
        })
        .set('main_image', new ImageMedia()
            .set('field_image', {
                src: 'http://graduateinstitute.ch/files/live/sites/iheid/files/sites/admininst/shared/photos-professors/TB_Photo.jpg',
                type: 'image/jpeg'
            })
        )
    ,
    'Albert Einstein': new CollaboratorNode()
        .set('title', 'Albert Einstein')
        .set('department', 'Department')
        .set('position', 'Theoretical physicist')
        .set('profile_cta', {
            url: 'https://en.wikipedia.org/wiki/Albert_Einstein',
            title: 'Profile page'
        })
        .set('main_image', new ImageMedia()
            .set('field_image', {
                src: 'https://i1.wp.com/bilgikapsulu.com/wp-content/uploads/2017/03/Albert-Einstein.jpg?w=460',
                type: 'image/jpeg',
                height: '100px',
                width: '100px',
                sources: [
                    {
                        src: 'http://iheid-dev.emakina.ch/sites/default/files/styles/thumbnail_medium/public/2018-05/Albert-Einstein_0.jpg?itok=xXb0vM6c',
                        type: 'image/jpeg',
                        height: '150px',
                        width: '150px'
                    }
                ]
            })
        ),
    'Mike Jefferson': new CollaboratorNode()
        .set('title', 'Mike Jefferson')
        .set('department', 'Department')
        .set('position', 'Professor School of Life Sciences')
        .set('profile_cta', {
            url: 'https://en.wikipedia.org/wiki/Albert_Einstein',
            title: 'Profile page'
        })
        .set('main_image', new ImageMedia()
            .set('field_image', {
                src: 'http://iheid-dev.emakina.ch/sites/default/files/styles/thumbnail/public/2018-05/No-Avatar-High-Definition.jpg?itok=_qL7CvLd',
                type: 'image/jpeg',
                height: '100px',
                width: '100px',
                sources: [
                    {
                        src: 'http://iheid-dev.emakina.ch/sites/default/files/styles/thumbnail_medium/public/2018-05/No-Avatar-High-Definition.jpg?itok=lodwWM_j',
                        type: 'image/jpeg',
                        height: '150px',
                        width: '150px'
                    }
                ]
            })
        ),
    'Pascal Lamy': new CollaboratorNode()
        .set('title', 'Pascal Lamy')
        .set('department', 'Department')
        .set('position', 'Professor School of Life Sciences')
        .set('profile_cta', {
            url: 'https://en.wikipedia.org/wiki/Albert_Einstein',
            title: 'Profile page'
        })
        .set('main_image', new ImageMedia()
            .set('field_image', {
                src: 'http://www.chartwellspeakers.com/wp-content/uploads/2013/12/Pascal-Lamy-Wordpress.jpg',
                type: 'image/jpeg'
            })
        )
        .set('field_social_profile', [
            {
                network: 'email',
                url: 'plamy@graduateinstitute.ch'
            },
            {
                network: 'facebook',
                url: 'http://example.com'
            },
            {
                network: 'twitter',
                url: 'http://example.com'
            },
            {
                network: 'linkedin',
                url: 'http://example.com'
            }
        ])
        .set('field_phone', "+41 22 908 58 98")
        .set('languages', [
            createTerm('language', 'bulgarian'),
            createTerm('language', 'english'),
            createTerm('language', 'french'),
            createTerm('language', 'italian'),
            createTerm('language', 'russian')
        ])
        .set('expertise', [
            createTerm('expertise', "Climate, climate change, natural disasters (Climate  cooperation)"),
            createTerm('expertise', "Energy"),
            createTerm('expertise', "European Union (European integration)"),
            createTerm('expertise', "Environment, environmental policies and law"),
            createTerm('expertise', "Governance, local and international"),
            createTerm('expertise', "International organisations, UN"),
            createTerm('expertise', "Multilateral diplomacy and international negotiation"),
            createTerm('expertise', "Public-private partnerships, Institutional change"),
            createTerm('expertise', "Europe, Eastern and Russia"),
            createTerm('expertise', "Europe, Western and Central")
        ])
        .set('social_share', require('../../../field/formatter/social-share/test.data')().values),
    'Nikola Tesla': new CollaboratorNode()
        .set('title', 'Nikola Tesla')
        .set('department', 'Department')
        .set('position', 'Electrical engineer')
        .set('profile_cta', {
            url: 'https://en.wikipedia.org/wiki/Albert_Einstein',
            title: 'Profile page'
        })
        .set('main_image', new ImageMedia()
            .set('field_image', {
                src: 'https://www.famousbirthdays.com/faces/tesla-nikola-image.jpg',
                type: 'image/jpeg',
                height: '100px',
                width: '100px'
            })
        ),
    'Marie Curie': new CollaboratorNode()
        .set('title', 'Marie Curie')
        .set('department', 'Department')
        .set('position', 'Physicist')
        .set('profile_cta', {
            url: 'https://en.wikipedia.org/wiki/Albert_Einstein',
            title: 'Profile page'
        })
        .set('main_image', new ImageMedia()
            .set('field_image', {
                src: 'https://i.pinimg.com/originals/2d/4e/3b/2d4e3ba341132bb422494a916238d2c6.jpg',
                type: 'image/jpeg',
                height: '100px',
                width: '100px'
            })
        ),
    'Galileo Galilei': new CollaboratorNode()
        .set('title', 'Galileo Galilei')
        .set('department', 'Department')
        .set('position', 'Polymath')
        .set('profile_cta', {
            url: 'https://en.wikipedia.org/wiki/Albert_Einstein',
            title: 'Profile page'
        })
        .set('main_image', new ImageMedia()
            .set('field_image', {
                src: 'http://iheid-dev.emakina.ch/sites/default/files/styles/thumbnail/public/2018-05/Galileo-Galilei.jpeg?itok=IrpZVICs',
                type: 'image/jpeg',
                height: '100px',
                width: '100px',
                sources: [
                    {
                        src: 'http://iheid-dev.emakina.ch/sites/default/files/styles/thumbnail_medium/public/2018-05/Galileo-Galilei.jpeg?itok=wxrqiWog',
                        height: '150px',
                        width: '150px',
                        type: 'image/jpeg'
                    }
                ]
            })
        ),
    'Stephen Hawking': new CollaboratorNode()
        .set('title', 'Stephen Hawking')
        .set('department', 'Lorem ipsum')
        .set('position', 'Theoretical physicist')
        .set('profile_cta', {
            url: 'https://en.wikipedia.org/wiki/Albert_Einstein',
            title: 'Profile page'
        })
        .set('main_image', new ImageMedia()
            .set('field_image', {
                src: 'https://www.alternatememories.com/images/intro/people/stephen-hawking_300x300.jpg',
                type: 'image/jpeg',
                height: '100px',
                width: '100px'
            })
        ),
    'Enrico Letta': new CollaboratorNode()
        .set('title', 'Enrico Letta')
        .set('department', 'Lorem ipsum')
        .set('position', 'Professor School of Life Sciences')
        .set('profile_cta', {
            url: 'http://graduateinstitute.ch/directory/_/people/biersteker',
            title: 'Profile page'
        })
        .set('main_image', new ImageMedia()
            .set('field_image', {
                src: 'http://iheid-dev.emakina.ch/sites/default/files/styles/thumbnail/public/2018-05/TB_Photo.jpg?itok=zGeOyRjJ',
                type: 'image/jpeg',
                height: '100px',
                width: '100px',
                sources: [
                    {
                        src: 'http://iheid-dev.emakina.ch/sites/default/files/styles/thumbnail_medium/public/2018-05/biersteker.jpg?itok=CqGwKoAB',
                        type: 'image/jpeg',
                        height: '150px',
                        width: '150px'
                    }
                ]
            })
        ),
    'Sophie Fleury': new CollaboratorNode()
        .set('title', 'Sophie Fleury')
        .set('field_social_profile', [
            {
                network: 'email',
                url: 'sophie.fleury@graduateinstitute.ch'
            }
        ])
        .set('field_phone', "+41 22 908 57 54"),
    'Dougal Thompson': new CollaboratorNode()
        .set('title', 'Dougal Thompson')
        .set('field_social_profile', [
            {
                network: 'email',
                url: 'dougal.thompson@graduateinstitute.ch'
            }
        ])
        .set('field_phone', "+41 22 908 43 49")
};
