/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/node/research-project',
        testCleanName: 'entity--node--research-project',
        data: require('./test.data')()
    };
};
