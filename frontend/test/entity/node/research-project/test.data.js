const collaboratorMocks = require('../collaborator/mocks');
const gridParagraphTest = require('../../paragraph/grid/test.data')();
const mocks = require('./mocks');
const {ImageMedia} = require('../../media/image/mocks');

module.exports = () => {
    let entity = new mocks.ResearchProject()
        .set('title', 'Community policing and roma minorities in the context of police reform: a comparative case study of albania and hungary')
        .set('field_top_header', 'Department')
        .set('field_header', 'Community policing and roma minorities in the context of police reform: a comparative case study of albania and hungary')
        .set('body', '<p>Vestibulum rutrum quam vitae fringilla tincidunt. Suspendisse nec tortor urna. Ut laoreet sodales nisi, quis iaculis nulla iaculis vitae. Donec sagittis faucibus lacus eget blandit. Mauris vitae ultricies metus, at condimentum nulla. Donec quis ornare lacus. Etiam gravida mollis tortor quis porttitor. Donec facilisis tortor ut augue lacinia, at viverra est semper. Sed sapien metus, scelerisque nec pharetra id, tempor a tortor. Pellentesque non dignissim neque. Ut porta viverra est, ut dignissim elit elementum.</p><p>Vestibulum rutrum quam vitae fringilla tincidunt. Suspendisse nec tortor urna. Ut laoreet sodales nisi, quis iaculis nulla iaculis vitae. Donec sagittis faucibus lacus eget blandit. Mauris vitae ultricies metus, at condimentum nulla.</p>')
        .set('field_editorial_content', [
            gridParagraphTest.entity
        ])
        .set('social_share', require('../../../field/formatter/social-share/test.data')().values)
        .set('field_collaborators', [
            {
                collaborator: collaboratorMocks["Thomas J. Biersteker"],
                position: 'Project coordinators',
                highlighted: true
            },
            {
                collaborator: collaboratorMocks["Albert Einstein"],
                position: 'Project coordinators',
                highlighted: false
            },
            {
                collaborator: collaboratorMocks["Mike Jefferson"],
                position: 'Collaborators',
                highlighted: true
            },
            {
                collaborator: collaboratorMocks["Pascal Lamy"],
                position: 'Collaborators',
                highlighted: false
            },
            {
                collaborator: collaboratorMocks["Nikola Tesla"],
                position: 'Collaborators',
                highlighted: false
            },
            {
                collaborator: collaboratorMocks["Marie Curie"],
                position: 'Collaborators',
                highlighted: false
            },
            {
                collaborator: collaboratorMocks["Galileo Galilei"],
                position: 'Donators',
                highlighted: true
            },
            {
                collaborator: collaboratorMocks["Stephen Hawking"],
                position: 'Donators',
                highlighted: false
            }
        ])
    ;

    return {
        entity: entity
    };
};