const {Field} = require('../../../../lib/drupal/index');
const {ProjectCollaboratorField} =  require('../../../field/type/project-collaborator/mocks');
const {HighlightedProjectCollaboratorsField} =  require('../../../field/type/highlighted-project-collaborators/mocks');
const {ImageMedia} = require('../../media/image/mocks');
const {Node} = require('../basic/mocks');
const sinon = require('sinon');

class ResearchProject extends Node {
    constructor() {
        // stub the computed fields
        let highlightedProjectCollaboratorField = new HighlightedProjectCollaboratorsField('highlighted_project_collaborators');

        sinon.stub(highlightedProjectCollaboratorField, 'values').get(function() {
            let values = [];
            let collaborators = this.entity.get('field_collaborators').values;

            for (let collaborator of collaborators) {
                if (collaborator.highlighted) {
                    values.push(collaborator);
                }
            }

            return values;
        });

        super('research-project', [
            new Field('string', 'title', 'Title'),
            new Field('string', 'field_top_header', 'Top header'),
            new Field('text-with-summary', 'body', 'Introduction'),
            new Field('entity-reference', 'field_editorial_content', 'Blocks'),
            new Field('entity-reference', 'field_banner_image', 'Banner image'),
            new ProjectCollaboratorField('field_collaborators', 'Contributors'),
            highlightedProjectCollaboratorField
        ]);

        // default banner image
        this.set('field_banner_image', new ImageMedia()
            .set('field_image', {
                src: 'https://picsum.photos/4133/2745?image=668',
                type: 'image/jpeg'
            })
        )
    }
}

module.exports = {
    ResearchProject: ResearchProject
};