let mocks = require('./mocks');

/**
 *
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function () {
    return {
        testName: 'entity/node',
        testCleanName: 'entity--node',
        data: {
            entity: mocks.entity
        }
    };
};
