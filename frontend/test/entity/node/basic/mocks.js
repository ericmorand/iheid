const {Entity, Field} = require('../../../../lib/drupal/index');
const {ImageMedia} = require('../../media/image/mocks');
const {ContentTypeField} = require('../../../field/type/content-type/mocks');
const {LogosField} = require('../../../field/type/logos/mocks');
const {RelatedContentField} = require('../../../field/type/related-content/mocks');

class Node extends Entity {
    constructor(bundle, fields = []) {
        fields.push(new Field('string', 'title', 'Title'));
        fields.push(new Field('text_with_summary', 'body', 'Body'));
        fields.push(new Field('entity-reference', 'field_banner_image', 'Banner image'));

        fields.push(new ContentTypeField('content_type'));
        fields.push(new Field('social-share', 'social_share', 'Share'));
        fields.push(new LogosField('logos', 'Logos'));
        fields.push(new RelatedContentField('related_content', 'Related content'));

        super('node', bundle, fields);

        // default banner image
        this.set('field_banner_image', new ImageMedia()
            .set('field_image', {
                src: 'https://picsum.photos/2048/1365?image=1033',
                type: 'image/jpeg'
            })
        )
    }
}

module.exports = {
    Node: Node,
    entity: new Node('basic')
};
