const mocks = require('./mocks');

module.exports = () => {
    return {
        entity: mocks["Killer robots in the battlefield and the alleged accountability gap for war crimes"]
    };
};