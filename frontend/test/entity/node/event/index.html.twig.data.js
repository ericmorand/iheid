/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/node/event',
        testCleanName: 'entity--node--event',
        data: require('./test.data')()
    };
};
