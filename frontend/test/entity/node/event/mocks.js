const {Field} = require('../../../../lib/drupal/index');
const {ImageMedia} = require('../../media/image/mocks');
const {Node} = require('../basic/mocks');
const collaboratorMocks = require('../../../entity/node/collaborator/mocks');
const {VisualDateField} = require('../../../field/type/visual-date/mocks');
const {loremIpsum: loremIpsumRichTextParagraph} = require('../../../entity/paragraph/rich-text/mocks');
const {loremIpsum: loremIpsumImageParagraph} = require('../../../entity/paragraph/image/mocks');
const sinon = require('sinon');

class EventNode extends Node {
    constructor() {
        // stub computed field
        let visualDateField = new VisualDateField('visual_date');

        // sinon.stub(computedDateRangeField, 'values').get(function() {
        //     return this.entity.get('field_start_end_date').values;
        // });

        super('event', [
            new Field('string', 'title', 'Title'),
            new Field('text-with-summary', 'body', 'Body'),
            new Field('date_range', 'field_start_end_date', 'Date range'),
            new Field('entity_reference', 'field_editorial_content', 'Editorial content'),
            new Field('entity_reference', 'field_main_category', 'Main category'),
            new Field('entity_reference', 'field_main_image', 'Main image'),
            new Field('entity_reference', 'field_other_categories', 'Other categories'),
            new Field('string', 'field_location', 'Location'),
            new Field('entity_reference', 'field_author', 'Speakers'),
            new Field('entity_reference', 'field_tags', 'Tags'),
            new Field('string', 'field_top_header', 'Top header'),
            new Field('add_to_calendar', 'add_to_calendar', 'Add to calendar'),
            visualDateField,
            new Field('entity_reference', 'field_editorial_content', 'Editorial content'),
            new Field('string', 'field_raw_content', 'Raw content'),
        ]);

        // default banner image
        this.set('field_banner_image', new ImageMedia()
            .set('field_image', {
                src: 'https://picsum.photos/4288/2848?image=444',
                type: 'image/jpeg'
            })
        )
    }
}

module.exports = {
    EventNode: EventNode,
    'Killer robots in the battlefield and the alleged accountability gap for war crimes': new EventNode()
        .set('title', 'Killer robots in the battlefield and the alleged accountability gap for war crimes')
        .set('field_top_header', 'Department')
        .set('field_start_end_date', {
            value: new Date(2018, 1, 21, 17, 30),
            end_value: new Date(2018, 1, 21, 19, 15)
        })
        .set('field_cta', {
            url: 'http://graduateinstitute.ch/directory/_/people/biersteker',
            title: 'Profile page'
        })
        .set('field_main_image', new ImageMedia()
            .set('field_image', {
                src: 'http://www.chartwellspeakers.com/wp-content/uploads/2013/12/Pascal-Lamy-Wordpress.jpg',
                type: 'image/jpeg'
            })
        )
        .set('field_author', collaboratorMocks["Enrico Letta"])
        .set('field_location', 'Geneva, Switzerland')
        .set('body', 'Auditorium Ivan Pictet A, Maison de la Paix The Centre for Finance and Development is delighted to partner...')
        .set('social_share', require('../../../field/formatter/social-share/test.data')().values)
        .set('add_to_calendar', {
            url: 'http://example.com',
            title: 'iCalendar'
        })
        .set('field_editorial_content', [
            loremIpsumRichTextParagraph,
            loremIpsumImageParagraph
        ])
        .set('field_raw_content', `<iframe style="border: solid 1px; width: 100%; box-sizing: border-box; height: 640px" src="https://nodejs.org"></iframe>`)
};
