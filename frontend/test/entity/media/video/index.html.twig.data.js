/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    let data = require('./test.data')();

    data.label = 'Lorem ipsum';

    return {
        testName: 'entity/media/video',
        testCleanName: 'entity--media--video',
        data: data
    };
};
