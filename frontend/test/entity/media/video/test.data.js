const {Entity, Field, FieldFormatter, FormattedField, FieldGroup, ViewMode} = require('../../../../lib/drupal/index');

class VideoMedia extends Entity {
    constructor() {
        super('media', 'video', [
            new Field('file', 'field_video', 'Video')
        ]);
    }
}

module.exports = () => {
    let entity = new VideoMedia()
        .set('field_video', {
            title: 'IHEID.mp4',
            src: 'http://demo.emakina.ch/iheid/test-resources/Institute%20(General)%20720360%20COMP%203.1.mp4',
            type: 'video/mp4',
        })
    ;

    return {
        VideoMedia: VideoMedia,
        entity: entity
    }
};