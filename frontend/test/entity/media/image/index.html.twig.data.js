/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    let data = require('./test.data')();

    data.label = 'Lorem ipsum';

    return {
        testName: 'entity/media/image',
        testCleanName: 'entity--media--image',
        data: data
    };
};
