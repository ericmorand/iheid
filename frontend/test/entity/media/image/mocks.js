const {Entity, Field} = require('../../../../lib/drupal/index');

class ImageMedia extends Entity {
    constructor() {
        super('media', 'image', [
            new Field('image', 'field_image', 'Image')
        ]);
    }
}

let createImageMedia = (src) => {
    return new ImageMedia().set('field_image', {
        src: src,
        type: 'image/jpeg',
    })
};

module.exports = {
    ImageMedia: ImageMedia,
    createImageMedia: createImageMedia,
    large: new ImageMedia().set('field_image', {
        src: 'https://placehold.it/1920x1080',
        type: 'image/jpeg',
        width: '1920px',
        height: '1080px'
    }),
    thumbnail: new ImageMedia().set('field_image', {
        src: 'https://placehold.it/100x100',
        type: 'image/jpeg',
        width: '100px',
        height: '100px'
    }),
    thumbnail_medium_responsive: new ImageMedia().set('field_image', {
        src: 'https://placehold.it/100x100',
        type: 'image/jpeg',
        width: '100px',
        height: '100px',
        sources: [
            {
                src: 'https://placehold.it/150x150',
                type: 'image/jpeg',
                width: '150px',
                height: '150px'
            }
        ]
    }),
    thumbnail_medium: new ImageMedia().set('field_image', {
        src: 'https://placehold.it/150x150',
        type: 'image/jpeg',
        width: '150px',
        height: '150px'
    }),
    thumbnail_large: new ImageMedia().set('field_image', {
        src: 'https://placehold.it/400x400',
        type: 'image/jpeg',
        width: '400px',
        height: '400px'
    })
};