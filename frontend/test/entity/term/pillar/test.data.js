const {foo} = require('./mocks');

module.exports = () => {
    return {
        entity: foo
    };
};