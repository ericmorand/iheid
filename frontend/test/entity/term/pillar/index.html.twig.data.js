/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/term/pillar',
        testCleanName: 'entity--term--pillar',
        data: require('./test.data')()
    };
};
