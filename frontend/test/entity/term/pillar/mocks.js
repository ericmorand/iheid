const {Field} = require('../../../../lib/drupal/index');
const {Term} = require('../mocks');
const contactMocks = require('../../node/contact/mocks');

class PillarTerm extends Term {
    constructor() {
        super('pillar', [
            new Field('string', 'title', 'Title'),
            new Field('entity_reference', 'field_collaborators', 'Contacts'),
        ]);
    }
}

let foo = new PillarTerm('foo')
    .set('title', 'Foo pillar')
    .set('body', '<h3>Your contact at IHEID</h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.')
    .set('field_collaborators', [
        contactMocks.generalInformation,
        contactMocks.mediaEnquiries
    ])
;

module.exports = {
    PillarTerm: PillarTerm,
    'foo': foo
};
