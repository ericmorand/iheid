/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'entity/term',
        testCleanName: 'entity--term',
        data: require('./test.data')()
    };
};
