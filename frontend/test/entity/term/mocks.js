const {Entity, Field} = require('../../../lib/drupal/index');

class Term extends Entity {
    constructor(bundle, fields = []) {
        fields.push(new Field('string', 'title', 'Title'));
        fields.push(new Field('text_with_summary', 'body', 'Body'));

        super('term', bundle, fields);
    }
}

let createTerm = (bundle, title) => {
    return new Term(bundle)
        .set('title', title);
};

module.exports = {
    createTerm: createTerm,
    Term: Term,
    'Foo': new Term('foo')
        .set('title', 'Foo term')
        .set('body', 'Lorem ipsum dolor sit amet')
};
