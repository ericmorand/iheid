let Attributes = require('drupal-attribute');

/**
 *
 * @param twing {TwingEnvironment}
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function (twing) {
    return {
        testName: 'field/group/view-group',
        testCleanName: 'field--group--view-group',
        data: {
            attributes: new Attributes()
        }
    };
};
