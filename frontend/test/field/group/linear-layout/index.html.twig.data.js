let Attributes = require('drupal-attribute');

/**
 *
 * @param twing {TwingEnvironment}
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function (twing) {
    return {
        testName: 'field/group/linear-layout',
        testCleanName: 'field--group--linear-layout',
        data: {
            attributes: new Attributes()
        }
    };
};
