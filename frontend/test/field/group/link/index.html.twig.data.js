/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/group/link',
        testCleanName: 'field--group--link',
        data: require('./test.data')()
    };
};
