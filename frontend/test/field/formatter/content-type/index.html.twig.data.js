/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/formatter/content-type',
        testCleanName: 'field--formatter--content-type',
        data: require('./test.data')()
    };
};
