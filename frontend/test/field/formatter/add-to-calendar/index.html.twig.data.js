/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/formatter/add-to-calendar',
        testCleanName: 'field--formatter--add-to-calendar',
        data: require('./test.data')()
    };
};
