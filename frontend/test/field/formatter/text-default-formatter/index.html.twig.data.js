const {TwingMarkup} = require('twing');

/**
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function () {
    return {
        testName: 'field/formatter/text-default-formatter',
        testCleanName: 'field--formatter--text-default-formatter',
        data: {
            value: require('./value.html')
        }
    };
};
