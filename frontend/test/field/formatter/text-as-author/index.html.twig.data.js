let Attributes = require('drupal-attribute');

/**
 *
 * @param twing {TwingEnvironment}
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function (twing) {
    return {
        testName: 'field/formatter/text-as-author',
        testCleanName: 'field--formatter--text-as-author',
        data: {
            attributes: new Attributes(),
            text: 'Sandra Hendrikson'
        }
    };
};
