/**
 *
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function () {
    return {
        testName: 'field/formatter/image-as-decorated-image',
        testCleanName: 'field--formatter--image-as-decorated-image',
        data: {
            image: {
                src: 'https://picsum.photos/1920/1080?image=778',
                width: '1920px',
                height: '1080px'
            }
        }
    };
};
