/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/formatter/fitted-image',
        testCleanName: 'field--formatter--fitted-image',
        data: require('./test.data')()
    };
};
