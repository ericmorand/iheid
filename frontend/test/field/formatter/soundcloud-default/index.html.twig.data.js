/**
 *
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function () {
    return {
        testName: 'field/formatter/soundcloud-default',
        testCleanName: 'field--formatter--soundcloud-default'
    };
};
