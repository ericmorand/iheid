/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/formatter/project-collaborator',
        testCleanName: 'field--formatter--project-collaborator',
        data: require('./test.data')()
    };
};
