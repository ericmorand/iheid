const collaboratorMocks = require('../../../entity/node/collaborator/mocks');

module.exports = () => {
    return {
        project_collaborators: [
            {
                collaborator: collaboratorMocks["Albert Einstein"],
                highlighted: false
            },
            {
                collaborator: collaboratorMocks["Thomas J. Biersteker"],
                highlighted: true
            }
        ]
    };
};