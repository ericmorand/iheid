/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/formatter/datetime-default',
        testCleanName: 'field--formatter--datetime-default',
        data: require('./test.data')()
    };
};
