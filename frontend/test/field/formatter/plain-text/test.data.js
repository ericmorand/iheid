module.exports = () => {
    return {
        test_cases: [
            {
                title: 'Plain text',
                value: 'Lorem ipsum dolor sit amet'
            },
            {
                title: 'HTML Markup',
                value: '<div style="color: brown;">Lorem ipsum dolor sit amet</div>'
            },
            {
                title: 'HTML Markup + Script',
                value: `<div>There is an easter egg in your console...</div>
<script>
    console.log('Easter egg!')
</script>`
            }
        ]
    };
};