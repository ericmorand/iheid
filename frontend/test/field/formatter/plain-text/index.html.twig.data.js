/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/formatter/plain-text',
        testCleanName: 'field--formatter--plain-text',
        data: require('./test.data')()
    };
};
