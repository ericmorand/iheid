/**
 *
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function () {
    return {
        testName: 'field/formatter/link-as-button',
        testCleanName: 'field--formatter--link-as-button'
    };
};
