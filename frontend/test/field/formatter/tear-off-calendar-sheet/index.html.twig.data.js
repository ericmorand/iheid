/**
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function () {
    return {
        testName: 'field/formatter/tear-off-calendar-sheet',
        testCleanName: 'field--formatter--tear-off-calendar-sheet',
        data: require('./test.data')
    };
};
