/**
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function () {
    return {
        testName: 'field/formatter/responsive-tear-off-calendar-sheet',
        testCleanName: 'field--formatter--responsive-tear-off-calendar-sheet',
        data: require('./test.data')
    };
};
