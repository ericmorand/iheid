/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/formatter/link-as-fake-button',
        testCleanName: 'field--formatter--link-as-fake-button',
        data: require('./test.data')()
    };
};
