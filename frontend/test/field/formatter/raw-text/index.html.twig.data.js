/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/formatter/raw-text',
        testCleanName: 'field--formatter--raw-text',
        data: require('./test.data')()
    };
};
