let Attributes = require('drupal-attribute');

/**
 *
 * @param twing {TwingEnvironment}
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function (twing) {
    return {
        testName: 'field/formatter/text-as-caption',
        testCleanName: 'field--formatter--text-as-caption',
        data: {
            attributes: new Attributes(),
            text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a tellus leo."
        }
    };
};
