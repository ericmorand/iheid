/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/formatter/social-profile',
        testCleanName: 'field--formatter--social-profile',
        data: require('./test.data')()
    };
};
