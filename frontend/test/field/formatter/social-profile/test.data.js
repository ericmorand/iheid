module.exports = () => {
    return {
        profiles: [
            {
                network: 'email',
                url: 'cyanbakan@emakina.ch'
            },
            {
                network: 'facebook',
                url: '#'
            },
            {
                network: 'twitter',
                url: '#'
            },
            {
                network: 'instagram',
                url: '#'
            },
            {
                network: 'linkedin',
                url: '#'
            }
        ]
    };
};