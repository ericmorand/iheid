/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/formatter/telephone-link',
        testCleanName: 'field--formatter--telephone-link',
        data: require('./test.data')()
    };
};
