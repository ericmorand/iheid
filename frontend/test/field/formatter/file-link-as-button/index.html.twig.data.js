/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/formatter/file-link-as-button',
        testCleanName: 'field--formatter--file-link-as-button',
        data: require('./test.data')()
    };
};
