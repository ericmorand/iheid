/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/formatter/discrete-parts-datetime',
        testCleanName: 'field--formatter--discrete-parts-datetime',
        data: require('./test.data')()
    };
};
