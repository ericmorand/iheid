/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/formatter/entity-reference-label',
        testCleanName: 'field--formatter--entity-reference-label',
        data: require('./test.data')()
    };
};
