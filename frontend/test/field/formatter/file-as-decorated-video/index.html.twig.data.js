/**
 *
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function () {
    return {
        testName: 'field/formatter/file-as-decorated-video',
        testCleanName: 'field--formatter--file-as-decorated-video',
        data: {
            file: {
                src: 'http://www.htmgarcia.com/themes/cacoon/images/spaceboundd.mp4',
                type: 'video/mp4'
            }
        }
    };
};
