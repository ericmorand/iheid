/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/formatter/discrete-parts-daterange',
        testCleanName: 'field--formatter--discrete-parts-daterange',
        data: require('./test.data')()
    };
};
