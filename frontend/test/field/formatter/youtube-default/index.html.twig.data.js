/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/formatter/youtube-default',
        testCleanName: 'field--formatter--youtube-default',
        data: require('./test.data')()
    };
};
