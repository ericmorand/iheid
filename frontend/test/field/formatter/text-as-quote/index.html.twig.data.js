let Attributes = require('drupal-attribute');

/**
 *
 * @param twing {TwingEnvironment}
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function (twing) {
    return {
        testName: 'field/formatter/text-as-quote',
        testCleanName: 'field--formatter--text-as-quote',
        data: {
            attributes: new Attributes(),
            text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo."
        }
    };
};
