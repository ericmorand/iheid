module.exports = () => {
    let url = 'https://curvy-catfish-23.localtunnel.me';
    let socialSettings = {
        facebook: {
            app_id: 1852373988394950
        }
    };

    return {
        values: [
            {
                network: 'facebook',
                title: 'Facebook',
                url: url,
                social_settings: socialSettings
            },
            {
                network: 'twitter',
                title: 'Twitter',
                url: url,
                social_settings: socialSettings
            },
            {
                network: 'linkedin',
                title: 'LinkedIn',
                url: url,
                social_settings: socialSettings
            }
        ]
    }
};