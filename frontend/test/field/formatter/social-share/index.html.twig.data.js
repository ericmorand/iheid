/**
 *
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function () {
    return {
        testName: 'field/formatter/social-share',
        testCleanName: 'field--formatter--social-share',
        data: require('./test.data')()
    };
};
