const {Field} = require('../../../lib/drupal/index');

module.exports = () => {
    let field = new Field('string', 'foo', 'Foo');

    field.values = [
        'Lorem ipsum',
        'Dolor sit amet',
        'Consectetur adipiscing elit'
    ];

    return {
        field: field
    };
};