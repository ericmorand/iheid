/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field',
        testCleanName: 'field',
        data: require('./test.data')()
    };
};
