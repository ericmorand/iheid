const {Field} = require('../../../../lib/drupal');

class ContentTypeField extends Field {
    constructor(name, label) {
        super('content_type', name, label);
    }

    get values() {
        return [
            this.entity.bundle
        ];
    }

    /**
     * @param {*} value
     * @param {DrupalFieldFormatter} formatter
     * @returns {*}
     */
    processValue(value, formatter) {
        if (formatter.settings.get('overridden_title')) {
            value = formatter.settings.get('overridden_title');
        }

        return value;
    }
}

module.exports = {
    ContentTypeField: ContentTypeField
};