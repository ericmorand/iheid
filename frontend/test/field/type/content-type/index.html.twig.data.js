/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/type/content-type',
        testCleanName: 'field--type--content-type',
        data: require('./test.data')()
    };
};
