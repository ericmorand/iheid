const {Field} = require('../../../../lib/drupal');

class VisualDateField extends Field {
    constructor(name, label) {
        super('visual_date', name, label);
    }

    get values() {
        // let date =this.entity.get('field_start_end_date').values[0].value;
        // let dateFormatter = new DateFormatter();

        return [
            {
                date_parts: {
                    day_of_week: 'Wednesday',
                    day_of_month: '21',
                    month: 'February',
                },
                image: {
                    src: 'https://frankl.in/wp-content/uploads/2016/02/alpha_1px.png',
                    type: 'image/jpeg'
                }
            }
        ];
    }
}

module.exports = {
    VisualDateField: VisualDateField
};