let mocks = require('./mocks');
let sinon = require('sinon');

let field = new mocks.VisualDateField();

// stub values since we don't have an entity
sinon.stub(field, 'values').get(() => {
    return [
        {
            date_parts: {
                day_of_week: 'Friday',
                day_of_month: 21,
                month: 'February',
            },
            image: {
                src: 'https://placehold.it/1x1/A52A2A',
                type: 'image/jpeg'
            }
        }
    ];
});

module.exports = () => {
    return {
        field: field
    };
};