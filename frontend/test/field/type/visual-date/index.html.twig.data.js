/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/type/visual-date',
        testCleanName: 'field--type--visual-date',
        data: require('./test.data')()
    };
};
