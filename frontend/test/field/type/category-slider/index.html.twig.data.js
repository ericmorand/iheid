/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/type/category-slider',
        testCleanName: 'field--type--category-slider',
        data: require('./test.data')()
    };
};
