const {Field} = require('../../../../lib/drupal');

class CategorySliderField extends Field {
    constructor(name, label) {
        super('category_slider', name, label);
    }

    /**
     * Process the value that will be passed to the formatter.
     *
     * @param {*} value
     * @param {DrupalFieldFormatter} formatter
     * @returns {*}
     */
    processValue(value, formatter) {
        let index = this.values.indexOf(value);

        value.is_most_relevant = (index === 0);

        return value;
    }

    /**
     * Process the variables that will be passed to the template.
     *
     * This emulates a potential field pre-process function from the Drupal side.
     *
     * @param {*} variables
     * @param {Map<string, *>} formatterSettings
     * @returns {*}
     */
    processVariables(variables, formatterSettings) {
        variables.show_all_link = new Field('link', 'foo', 'Foo', [
            {
                url: '#',
                title: 'Show all'
            }
        ]);

        return variables;
    }
}

module.exports = {
    CategorySliderField: CategorySliderField
};