let {CategorySliderField} = require('./mocks');
let mocks = require('../../../entity/node/page/mocks');

let field = new CategorySliderField('foo', 'Foo');

field.values = [
    {
        entity: mocks["The Edgar de Picciotto International Prize"]
    },
    {
        entity: mocks["Financial aid"]
    },
    {
        entity: mocks["Meet the director"]
    },
    {
        entity: mocks["Campus de la Paix"]
    },
    {
        entity: mocks["The Edgar de Picciotto International Prize"]
    },
    {
        entity: mocks["Financial aid"]
    },
    {
        entity: mocks["Campus de la Paix"]
    },
];

module.exports = () => {
    return {
        field: field
    };
};