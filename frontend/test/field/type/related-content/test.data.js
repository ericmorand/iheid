const {RelatedContentField} = require('./mocks');
const pageMocks = require('../../../entity/node/page/mocks');
const newMocks = require('../../../entity/node/news/mocks');
const eventMocks = require('../../../entity/node/event/mocks');

let field = new RelatedContentField('foo', 'Foo');

field.values = [
    {
        entity: pageMocks["The Edgar de Picciotto International Prize"],
        type: 'category',
        active: false
    },
    {
        entity: newMocks["Student story: Fighting for youth empowerment"],
        type: 'content',
        active: false
    },
    {
        entity: pageMocks["Campus de la Paix"],
        type: 'content',
        active: true
    },
    {
        entity: pageMocks["Financial aid"],
        type: 'category',
        active: true
    },
    {
        entity: pageMocks["Meet the director"],
        type: 'content',
        active: false
    }
];

module.exports = () => {
    return {
        field: field
    };
};