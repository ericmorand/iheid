const {Field} = require('../../../../lib/drupal');

class RelatedContentField extends Field {
    constructor(name, label) {
        super('related_content', name, label);
    }

    /**
     * Process the variables that will be passed to the template.
     *
     * This emulates a potential field pre-process function from the Drupal side.
     *
     * @param {*} variables
     * @param {Map<string, *>} formatterSettings
     * @returns {*}
     */
    processVariables(variables, formatterSettings) {
        for (let i = 0; i < variables.items.length; i++) {
            let item = variables.items[i];

            item.active = this.values[i].active;
            item.type = this.values[i].type;
        }

        return variables;
    }
}

module.exports = {
    RelatedContentField: RelatedContentField
};