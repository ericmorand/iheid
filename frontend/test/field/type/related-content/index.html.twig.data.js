/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/type/related-content',
        testCleanName: 'field--type--related-content',
        data: require('./test.data')()
    };
};
