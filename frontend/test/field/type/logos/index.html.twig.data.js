/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/type/logos',
        testCleanName: 'field--type--logos',
        data: require('./test.data')()
    };
};
