const {LogosField} = require('./mocks');
const {createImageMedia} = require('../../../entity/media/image/mocks');

module.exports = () => {
    let field = new LogosField('foo', 'Foo');

    field.values = [
        createImageMedia('http://placehold.it/640x640/000000/FFFFFF&text=main%20small'),
        createImageMedia('http://placehold.it/640x640/880000/FFFFFF&text=main%20large'),
        createImageMedia('http://placehold.it/640x640/FF0000/FFFFFF&text=secondary'),
    ];

    return {
        field: field
    };
};