const {Field} = require('../../../../lib/drupal');
const {createImageMedia} = require('../../../entity/media/image/mocks');

class LogosField extends Field {
    constructor(name, label) {
        super('logos', name, label);
    }

    get values() {
        return [
            createImageMedia('https://placehold.it/240x240/?text=Small'),
            createImageMedia('http://iheid-dev.emakina.ch/sites/default/files/styles/logo_large/public/2018-07/main-logo.png?itok=qnlzyTLf'),
            createImageMedia('http://iheid-dev.emakina.ch/sites/default/files/styles/logo_large/public/2018-07/logo-default-text-en.png?itok=qfNVBpgg')
        ];
    }
}

module.exports = {
    LogosField: LogosField
};