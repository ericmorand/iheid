/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/type/three-cards',
        testCleanName: 'field--type--three-cards',
        data: require('./test.data')()
    };
};
