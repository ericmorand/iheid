const sinon = require('sinon');

sinon.stub(window, 'fetch').callsFake((url) => {
    let results = [];

    for (let i = 0; i < url; i++) {
        results.push({
            markup: `<div class="fake-content">Content #${i}</div>`
        });
    }

    let response = new window.Response(JSON.stringify(results), {
        status: 200,
        headers: {'Content-type': 'application/json'}
    });

    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve(response);
        }, 1500);
    });
});

require('./test');

module.exports = {};
