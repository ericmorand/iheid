const {Field} = require('../../../../lib/drupal');

class ThreeCardsField extends Field {
    constructor(name, label) {
        super('three_cards', name, label);
    }

    /**
     * Process the variables that will be passed to the template.
     *
     * This emulates a potential field pre-process function from the Drupal side.
     *
     * @param {*} variables
     * @param {Map<string, *>} formatterSettings
     * @returns {*}
     */
    processVariables(variables, formatterSettings) {
        Object.assign(variables, this.values[0]);

        return variables;
    }
}

module.exports = {
    ThreeCardsField: ThreeCardsField
};