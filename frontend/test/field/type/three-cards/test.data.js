let {ThreeCardsField} = require('./mocks');

let createField = function(url) {
    let field = new ThreeCardsField('foo', 'Foo');

    field.values = [
        {
            ws_url: url
        }
    ];

    return field;
};

let testCases = [
    {
        title: 'No content',
        field: createField(0)
    },
    {
        title: '1 content',
        field: createField(1)
    },
    {
        title: '2 contents',
        field: createField(2)
    },
    {
        title: '3 contents',
        field: createField(3)
    },
    {
        title: 'More than 3 contents',
        field: createField(10)
    }
];

module.exports = () => {
    return {
        test_cases: testCases
    };
};