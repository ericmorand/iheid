/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/type/youtube',
        testCleanName: 'field--type--youtube',
        data: require('./test.data')()
    };
};
