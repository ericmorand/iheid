const {Field} = require('../../../../lib/drupal');
const uniqid = require('uniqid');

class YouTubeField extends Field {
    constructor(name, label) {
        super('youtube', name, label);
    }

    /**
     * Process the value that will be passed to the formatter.
     *
     * @param {*} value
     * @param {DrupalFieldFormatter} formatter
     * @returns {*}
     */
    processValue(value, formatter) {
        return {
            player_id: uniqid(),
            video_id: 'jI-kpVh6e1U'
        };
    }
}

module.exports = {
    YouTubeField: YouTubeField
};