let mocks = require('./mocks');

let field = new mocks.YouTubeField('foo');

field.values = [
    'https://youtu.be/jI-kpVh6e1U',
    'https://youtu.be/jI-kpVh6e1U'
];

module.exports = () => {
    return {
        field: field
    };
};