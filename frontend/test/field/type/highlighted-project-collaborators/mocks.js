const {Field} = require('../../../../lib/drupal');

class HighlightedProjectCollaboratorsField extends Field {
    constructor(name) {
        super('highlighted_project_collaborators', name);
    }

    // stub the variables to add the color scheme
    /**
     *
     * @param {*} variables
     * @param {Map<string, *>} formatterSettings
     * @returns {*}
     */
    processVariables(variables, formatterSettings) {
        return Object.assign({}, variables, {
            color_scheme: formatterSettings.get('color_scheme')
        });
    }
}

module.exports = {
    HighlightedProjectCollaboratorsField: HighlightedProjectCollaboratorsField,
    field: new HighlightedProjectCollaboratorsField('foo', 'Foo')
};