/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/type/highlighted-project-collaborators',
        testCleanName: 'field--type--highlighted-project-collaborators',
        data: require('./test.data')()
    };
};
