const mocks = require('./mocks');
const collaboratorMocks = require('../../../entity/node/collaborator/mocks');
const {FieldFormatter} = require('../../../../lib/drupal');
const sinon = require('sinon');

module.exports = () => {
    let field = mocks.field;

    // stub the values since this is a computed field
    sinon.stub(field, 'values').get(function () {
        return [
            {collaborator: collaboratorMocks["Thomas J. Biersteker"]},
            {collaborator: collaboratorMocks["Marie Curie"]},
            {collaborator: collaboratorMocks["Nikola Tesla"]}
        ];
    });

    return {
        field: field,
        formatters: [
            new FieldFormatter('project-collaborator', 'hidden', new Map([
                ['view_mode', 'secondary_signature'],
                ['highlighted_view_mode', 'main_signature']
            ]))
        ]
    };
};