const {Field} = require('../../../../lib/drupal');

module.exports = () => {
    let field = new Field('image', 'foo', 'Foo');

    field.values = [
        {
            src: 'https://placehold.it/640x320',
            type: 'image/jpeg'
        }
    ];

    return {
        field: field
    };
};