/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/type/image',
        testCleanName: 'field--type--image',
        data: require('./test.data')()
    };
};
