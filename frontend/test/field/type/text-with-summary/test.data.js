const {Field, FieldFormatter} = require('../../../../lib/drupal');

module.exports = () => {
    let field = new Field('text-with-summary', 'foo', 'Foo');

    field.values = [
        'Lorem ipsum',
        'Dolor sit amet'
    ];

    return {
        field: field,
        formatters: [
            new FieldFormatter('default')
        ]
    };
};