/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/type/text-with-summary',
        testCleanName: 'field--type--text-with-summary',
        data: require('./test.data')()
    };
};
