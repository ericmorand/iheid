/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/type/add-to-calendar',
        testCleanName: 'field--type--add-to-calendar',
        data: require('./test.data')()
    };
};
