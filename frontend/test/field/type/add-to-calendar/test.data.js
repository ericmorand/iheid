const {Field} = require('../../../../lib/drupal/index');

module.exports = () => {
    let field = new Field('add_to_calendar', 'foo', 'Add to calendar');

    field.values = [
        {
            url: 'http://example.com',
            title: 'Google'
        },
        {
            url: 'http://example.com',
            title: 'Apple'
        },
        {
            url: 'http://example.com',
            title: 'Outlook'
        }
    ];

    return {
        field: field
    };
};