let Attributes = require('drupal-attribute');

/**
 *
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function () {
    return {
        testName: 'field/type/social-share',
        testCleanName: 'field--type--social-share',
        data: {
            getAttributes: (it) => {
                return new Attributes(it)
            },
            networks: [
                {
                    name: 'facebook',
                    title: 'Facebook'
                },
                {
                    name: 'twitter',
                    title: 'Twitter'
                },
                {
                    name: 'linkedin',
                    title: 'LinkedIn'
                }
            ],
            url: 'https://curvy-catfish-23.localtunnel.me',
            social_settings: {
                facebook: {
                    app_id: 1852373988394950
                }
            }
        }
    };
};
