module.exports = () => {
    return {
        networks: [
            {
                name: 'facebook',
                title: 'Facebook'
            },
            {
                name: 'twitter',
                title: 'Twitter'
            },
            {
                name: 'linkedin',
                title: 'LinkedIn'
            }
        ],
        url: 'https://curvy-catfish-23.localtunnel.me',
        social_settings: {
            facebook: {
                app_id: 1852373988394950
            }
        }
    }
};