/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/type/social-profile',
        testCleanName: 'field--type--social-profile',
        data: require('./test.data')()
    };
};
