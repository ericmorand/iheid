const {Field} = require('../../../../lib/drupal');

module.exports = () => {
    let field = new Field('social_profile', 'foo', 'Foo');

    field.values = [
        {
            network: 'foo',
            url: 'http://example.com'
        },
        {
            network: 'bar',
            url: 'http://example.com'
        }
    ];

    return {
        field: field
    };
};