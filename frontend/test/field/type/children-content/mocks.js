const {Field} = require('../../../../lib/drupal');

class ChildrenContentField extends Field {
    constructor(name, label) {
        super('children_content', name, label);
    }
}

module.exports = {
    ChildrenContentField: ChildrenContentField
};