/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/type/children-content',
        testCleanName: 'field--type--children-content',
        data: require('./test.data')()
    };
};
