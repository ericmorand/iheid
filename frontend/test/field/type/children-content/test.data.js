let {ChildrenContentField} = require('./mocks');
let mocks = require('../../../entity/node/page/mocks');

let field = new ChildrenContentField('foo', 'Foo');

field.values = [
    {
        entity: mocks["The Edgar de Picciotto International Prize"]
    },
    {
        entity: mocks["Financial aid"]
    },
    {
        entity: mocks["Meet the director"]
    },
    {
        entity: mocks["Campus de la Paix"]
    },
    {
        entity: mocks["The Edgar de Picciotto International Prize"]
    },
    {
        entity: mocks["Financial aid"]
    },
];

module.exports = () => {
    return {
        field: field
    };
};