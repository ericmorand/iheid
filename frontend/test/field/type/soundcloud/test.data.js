let {SoundCloudField} = require('./mocks');

let field = new SoundCloudField('foo', 'Foo');

field.values = [
    'https://soundcloud.com/the-bugle/bugle-179-playas-gon-play'
];

module.exports = () => {
    return {
        field: field
    };
};