/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/type/soundcloud',
        testCleanName: 'field--type--soundcloud',
        data: require('./test.data')()
    };
};
