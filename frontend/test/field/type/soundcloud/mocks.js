const {Field} = require('../../../../lib/drupal');

class SoundCloudField extends Field {
    constructor(name, label) {
        super('soundcloud', name, label);
    }

    /**
     * Process the value that will be passed to the formatter.
     *
     * @param {*} value
     * @param {DrupalFieldFormatter} formatter
     * @returns {*}
     */
    processValue(value, formatter) {
        return {
            url: value
        };
    }
}

module.exports = {
    SoundCloudField: SoundCloudField
};