/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/type/media/image',
        testCleanName: 'field--type--media--image',
        data: require('./test.data')()
    };
};
