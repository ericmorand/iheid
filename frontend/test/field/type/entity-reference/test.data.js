const {Field, FieldFormatter} = require('../../../../lib/drupal/index');

module.exports = () => {
    let media = require('../../../entity/media/image/mocks').thumbnail_large;
    let field = new Field('entity_reference', 'image_media', 'Image media');

    field.values = [media];

    return {
        field: field,
        formatters: [
            new FieldFormatter('rendered-entity', 'visible', new Map([
                ['view_mode', 'full']
            ]))
        ]
    };
};