const {Field, FieldFormatter} = require('../../../../lib/drupal');

module.exports = () => {
    let field = new Field('link', 'foo', 'Foo');

    field.values = [
        {
            title: 'Lorem ipsum',
            url: 'https://lipsum.com/feed/html'
        },
        {
            title: 'Dolor sit amet',
            url: 'https://lipsum.com/feed/html'
        },
        {
            title: 'Dolor sit amet',
            url: 'https://lipsum.com/feed/html'
        }
    ];

    return {
        field: field,
        formatters: [
            new FieldFormatter('link-as-button', 'hidden', new Map([
                ['variant', 'secondary']
            ])),
            new FieldFormatter('link-as-fake-button', 'hidden', new Map([
                ['variant', 'secondary']
            ]))
        ]
    };
};
