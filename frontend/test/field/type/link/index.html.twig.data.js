/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/type/link',
        testCleanName: 'field--type--link',
        data: require('./test.data')()
    };
};
