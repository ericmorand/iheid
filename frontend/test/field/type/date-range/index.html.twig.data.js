/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/type/date-range',
        testCleanName: 'field--type--date-range',
        data: require('./test.data')()
    };
};
