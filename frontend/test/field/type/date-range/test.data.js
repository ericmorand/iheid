const {Field} = require('../../../../lib/drupal');

module.exports = () => {
    let field = new Field('date_range', 'foo', 'Foo');

    field.values = [
        {
            value: new Date(2018, 1, 21, 17, 30),
            end_value: new Date(2018, 1, 21, 19, 15)
        },
        {
            value: new Date(2018, 1, 21, 10, 0),
            end_value: new Date(2018, 1, 25, 17, 30)
        }
    ];

    return {
        field: field
    };
};