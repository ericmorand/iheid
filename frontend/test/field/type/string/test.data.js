const {Field, FieldFormatter} = require('../../../../lib/drupal');

module.exports = () => {
    let field = new Field('text-with-summary', 'foo', 'Foo');

    field.values = [
        'Lorem ipsum',
        'Dolor sit amet'
    ];

    return {
        field: field,
        formatters: [
            new FieldFormatter('plain-text', 'hidden'),
            new FieldFormatter('text-as-author', 'hidden'),
            new FieldFormatter('text-as-caption', 'hidden'),
            new FieldFormatter('text-as-quote', 'hidden'),
            new FieldFormatter('text-as-heading', 'hidden', new Map([
                ['level', 2]
            ]))
        ]
    };
};