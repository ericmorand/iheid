/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/type/string',
        testCleanName: 'field--type--string',
        data: require('./test.data')()
    };
};
