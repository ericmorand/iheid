const {Field, FieldFormatter} = require('../../../../lib/drupal');

module.exports = () => {
    let field = new Field('file', 'foo', 'Foo');

    field.values = [
        {
            files: [
                {
                    src: 'http://techslides.com/demos/sample-videos/small.mp4',
                    type: 'video/mp4'
                },
                {
                    src: 'http://techslides.com/demos/sample-videos/small.webm',
                    type: 'video/webm'
                }
            ]
        }
    ];

    return {
        field: field,
        formatters: [
            new FieldFormatter('file-as-decorated-video', 'hidden', new Map([
                ['decorations', ['textures']]
            ]))
        ]
    };
};