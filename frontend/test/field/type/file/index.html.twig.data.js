/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/type/file',
        testCleanName: 'field--type--file',
        data: require('./test.data')()
    };
};
