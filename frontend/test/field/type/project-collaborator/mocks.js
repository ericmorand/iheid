const DrupalAttribute = require('drupal-attribute');
const {Field} = require('../../../../lib/drupal');

class ProjectCollaboratorField extends Field {
    constructor(name, label) {
        super('project_collaborator', name, label);
    }

    // stub the returned variables since we need to group by positions
    /**
     *
     * @param {*} variables
     * @param {Map<string, *>} formatterSettings
     * @returns {*}
     */
    processVariables(variables, formatterSettings) {
        let positions = new Map();
        let values = this.values;
        let index = 0;

        for (let item of variables.items) {
            let value = values[index];
            let positionTitle = value.position;

            if (!positions.has(positionTitle)) {
                positions.set(positionTitle, {
                    attributes: new DrupalAttribute(),
                    title: positionTitle,
                    collaborators: []
                });
            }

            let position = positions.get(positionTitle);

            position.collaborators.push(item);

            index++;
        }

        return Object.assign({}, variables, {
            positions: positions,
            color_scheme: formatterSettings.get('color_scheme')
        });
    }
}

module.exports = {
    ProjectCollaboratorField: ProjectCollaboratorField
};