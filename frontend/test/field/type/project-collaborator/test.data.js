const {ProjectCollaboratorField} = require('./mocks');
const collaboratorMocks = require('../../../entity/node/collaborator/mocks');
const {FieldFormatter} = require('../../../../lib/drupal');

module.exports = () => {
    let field = new ProjectCollaboratorField('foo', 'Foo');

    field.values = [
        {
            collaborator: collaboratorMocks["Thomas J. Biersteker"],
            position: 'Project coordinator',
            highlighted: true
        },
        {
            collaborator: collaboratorMocks["Marie Curie"],
            position: 'Researchers',
            highlighted: false
        },
        {
            collaborator: collaboratorMocks["Nikola Tesla"],
            position: 'Researchers',
            highlighted: false
        }
    ];

    return {
        field: field,
        formatters: [
            new FieldFormatter('project-collaborator', 'hidden', new Map([
                ['view_mode', 'secondary-signature'],
                ['highlighted_view_mode', 'main-signature']
            ]))
        ]
    };
};