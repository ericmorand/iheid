/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'field/type/project-collaborator',
        testCleanName: 'field--type--project-collaborator',
        data: require('./test.data')()
    };
};
