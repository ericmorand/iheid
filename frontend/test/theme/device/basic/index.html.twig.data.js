/**
 *
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function () {
    let devices = require('../../../../src/theme/device/devices').devices;

    return {
        testName: 'theme/device',
        testCleanName: 'theme--device',
        data: {
            devices: devices
        }
    };

};
