let Attributes = require('drupal-attribute');

/**
 *
 * @param twing {TwingEnvironment}
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function (twing) {
    return {
        testName: 'theme/color',
        testCleanName: 'theme--color',
        data: {
            attributes: new Attributes()
        }
    };
};
