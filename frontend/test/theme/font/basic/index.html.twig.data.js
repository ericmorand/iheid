/**
 * @param twing {TwingEnvironment}
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function (twing) {
    return {
        testName: 'theme/font',
        testCleanName: 'theme--font'
    };
};
