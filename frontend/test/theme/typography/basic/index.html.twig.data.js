let Attributes = require('drupal-attribute');

/**
 *
 * @param twing {TwingEnvironment}
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function (twing) {
    return {
        testName: 'typography',
        testCleanName: 'typography',
        data: {
            attributes: new Attributes()
        }
    };
};
