let glyphs = require('../../../../src/theme/iconography/glyphs');

/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = function () {
    return {
        testName: 'iconography',
        testCleanName: 'iconography',
        data: {
            icons: glyphs['iheid--iconography--glyphs']
        }
    };
};
