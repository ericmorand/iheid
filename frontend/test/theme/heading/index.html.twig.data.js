/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'theme/heading',
        testCleanName: 'theme--heading',
        data: require('./test.data')()
    };
};
