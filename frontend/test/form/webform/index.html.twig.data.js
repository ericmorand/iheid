/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'form/webform',
        testCleanName: 'form--webform',
        data: require('./test.data')()
    };
};
