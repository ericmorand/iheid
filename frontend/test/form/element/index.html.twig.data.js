/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'form/element',
        testCleanName: 'form--element',
        data: require('./test.data')()
    };
};
