const Attribute = require('drupal-attribute');

module.exports = () => {
    return {
        attributes: new Attribute([['type', 'email'], ['placeholder', 'Your E-Mail Address']]),
    };
};