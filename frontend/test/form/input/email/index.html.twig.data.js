/**
 *
 * @returns {{testName: string, testCleanName: string, attributes: {}, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'form/input/email',
        testCleanName: 'form--input--email',
        data: require('./test.data')()
    };
};
