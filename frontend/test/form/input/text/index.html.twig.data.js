const Attribute = require('drupal-attribute');

/**
 *
 * @returns {{testName: string, testCleanName: string, attributes: {}, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'form/input/text',
        testCleanName: 'form--input--text',
        attributes: new Attribute([['type', 'text'], ['placeholder', 'Write your name here, please.']]),
        data: require('./test.data')()
    };
};
