/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'util',
        testCleanName: 'util',
        data: require('./test.data')()
    };
};
