/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'body',
        testCleanName: 'body',
        data: require('./test.data')()
    };
};
