/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = function () {
    return {
        testName: 'ui/slider',
        testCleanName: 'ui--slider'
    };
};
