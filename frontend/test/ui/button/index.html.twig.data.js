let Attributes = require('drupal-attribute');

/**
 *
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function () {
    return {
        testName: 'ui/button',
        testCleanName: 'ui--button'
    };
};
