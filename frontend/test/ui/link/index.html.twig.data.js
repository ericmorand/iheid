/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'ui/link',
        testCleanName: 'ui--link',
        data: require('./test.data')()
    };
};
