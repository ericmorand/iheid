let Attributes = require('drupal-attribute');

/**
 *
 * @param twing {TwingEnvironment}
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function (twing) {
    return {
        testName: 'layout/grid',
        testCleanName: 'layout--grid',
        data: {
            attributes: new Attributes()
        }
    };
};
