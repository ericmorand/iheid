/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'layout/region/header-main',
        testCleanName: 'layout--region--header-main',
        data: require('./test.data')()
    };
};
