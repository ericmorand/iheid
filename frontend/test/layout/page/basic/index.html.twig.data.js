
/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'layout/page',
        testCleanName: 'layout--page',
        data: require('./test.data')()
    };
};
