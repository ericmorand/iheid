const {entity} = require('../../../entity/node/basic/mocks');
const {entity: animatedBanner} = require('../../../entity/paragraph/animated-banner/test.data')();

module.exports = () => {
    return {
        entity: entity,
        animated_banner: animatedBanner
    };
};