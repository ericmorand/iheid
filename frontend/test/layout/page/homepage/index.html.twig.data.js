
/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'layout/page/homepage',
        testCleanName: 'layout--page--homepage',
        data: require('./test.data')()
    };
};
