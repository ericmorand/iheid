/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'layout/page/research-project',
        testCleanName: 'layout--page--research-project',
        data: require('./test.data')()
    };
};
