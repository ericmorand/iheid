module.exports = () => {
    return {
        logos: require('../basic/test.data')().logos,
        research_project: require('../../../entity/node/research-project/test.data')().entity,
        breadcrumb: [
            {
                text: 'Node',
                url: '#'
            },
            {
                text: 'Research page',
                url: '#'
            },
            {
                text: 'Community policing and roma minorities in the context of police reform: a comparative case study of Albania and Hungary'
            }
        ]
    };
};