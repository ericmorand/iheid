const {menu} = require('./mocks');

module.exports = () => {
    return {
        content: 'Hello navigation/menu/header!',
        menu: menu
    };
};