/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'navigation/menu/header',
        testCleanName: 'navigation--menu--header',
        data: require('./test.data')()
    };
};
