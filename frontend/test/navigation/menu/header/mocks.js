const {createMenu, createItem} = require('../mocks');

let menu = createMenu('Quick links', [
    createItem('Research clusters', null, ['en', 'fr']),
    createItem('Research centers', null, ['en', 'fr']),
    createItem('Library', null, ['en']),
    createItem('Find an expert', null, ['en']),
    createItem('The faculty', null, ['en']),
], 'en');

module.exports = {
    menu: menu
};