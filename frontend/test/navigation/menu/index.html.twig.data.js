/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'navigation/menu',
        testCleanName: 'navigation--menu',
        data: require('./test.data')()
    };
};
