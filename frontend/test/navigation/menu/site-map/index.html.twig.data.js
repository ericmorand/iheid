/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'navigation/menu/site-map',
        testCleanName: 'navigation--menu--site-map',
        data: require('./test.data')()
    };
};
