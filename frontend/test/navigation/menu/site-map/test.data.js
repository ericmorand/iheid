const {menu} = require('./mocks');

module.exports = () => {
    return {
        menu: menu
    };
};