const {createMenu, createItem} = require('../mocks');

let menu = createMenu('Full sitemap', [
    createItem('Discover the institute', [
        createItem('Who we are', [], ['en', 'fr']),
        createItem('Meet the director', [], ['en']),
        createItem('International Geneva', [], ['en']),
        createItem('Campus de la paix', [], ['en']),
        createItem('Governance', [], ['en']),
        createItem('Distinguished Fellows', [], ['en']),
        createItem('Funded Chairs', [], ['en']),
        createItem('History of the institute', [], ['en']),
        createItem('Partners & Associations', [], ['en', 'fr']),
        createItem('Support the Institute', [], ['en']),
        createItem('The Edgar de Picciotto international Prize', [], ['en']),
        createItem('The Geneva Challenge', [], ['en']),
        createItem('Academic positions', [], ['en', 'fr'])
    ], ['en', 'fr']),
    createItem('Student life', [
        createItem('Arrival guide', [], ['en']),
        createItem('Student\'s Essentials', [], ['en']),
        createItem('Student Services', [], ['en']),
        createItem('Career services', [], ['en', 'fr']),
        createItem('Our community', [], ['en', 'fr']),
        createItem('Help & Support', [], ['en']),
        createItem('Cafeteria', [], ['en'])
    ], ['en', 'fr']),
    createItem('Executive education', [
        createItem('Why Executive Education', [], ['en']),
        createItem('A boost to your career', [], ['en']),
        createItem('Executive Programmes', [], ['en', 'fr']),
    ], ['en']),
    createItem('Venues', [
        createItem('Overview', [], ['en']),
        createItem('Event Facilities', [], ['en']),
        createItem('Catering', [], ['en']),
        createItem('Accommodation', [], ['en']),
    ], ['en']),
    createItem('Admissions', [
        createItem('How to apply', [], ['en']),
        createItem('Tuition & Cost', [], ['en']),
        createItem('Financial aids', [], ['en', 'fr']),
        createItem('Language requirement', [], ['en']),
        createItem('Application for accommodation', [], ['en']),
        createItem('Information for parents', [], ['en', 'fr']),
        createItem('Meet us', [], ['en', 'fr']),
        createItem('Contact us', [], ['en']),
    ], ['en']),
    createItem('Research', [
        createItem('Our research clusters', [], ['en']),
        createItem('Centre on Conflict, Development & Peacebuilding', [], ['en']),
        createItem('Centre for Finance and Development', [], ['en']),
        createItem('Centre for International Environmental Studies', [], ['en']),
        createItem('Global Migration Centre', [], ['en']),
        createItem('Centre for Trade and Economic Integration', [], ['en']),
        createItem('Gender Centre', [], ['en', 'fr']),
        createItem('Global Health Centre', [], ['en']),
        createItem('Albert Hirschman Centre on Democracy', [], ['en']),
        createItem('Global Governance Centre', [], ['en']),
        createItem('Research projects', [], ['en']),
        createItem('Library', [], ['en']),
        createItem('Funding opportunities', [], ['en']),
        createItem('Visiting programs', [], ['en', 'fr']),
        createItem('Bulletin de la recherche', [], ['en', 'fr']),
        createItem('Publications', [], ['en', 'fr']),
    ], ['en']),
    createItem('News', null, ['en']),
    createItem('Events', null, ['en']),
    createItem('Programmes', [
        createItem('Our programmes in a nutshell', [], ['en']),
        createItem('Courses Catalogue', [], ['en', 'fr']),
        createItem('Summer & Winter Programs', [], ['en']),
        createItem('Joint programmes', [], ['en', 'fr']),
        createItem('MOOCS', [], ['en', 'fr']),
    ], ['en']),
    createItem('Academic departments', [
        createItem('Interdisciplinary Master in International Affairs', [], ['en']),
        createItem('Interdisciplinary Master in Development Studies', [], ['en']),
        createItem('Anthropology and Sociology', [], ['en']),
        createItem('International Economics', [], ['en', 'fr']),
        createItem('International History', [], ['en', 'fr']),
        createItem('International Law', [], ['en', 'fr']),
        createItem('International Relations & Political Science', [], ['en']),
        createItem('The Faculty', [], ['en']),
    ], ['en']),
    createItem('Alumni', [
        createItem('About the Alumni', [], ['en']),
        createItem('Chapters', [], ['en']),
        createItem('Ambassadors', [], ['en']),
        createItem('Class leaders', [], ['en']),
        createItem('Services', [], ['en']),
        createItem('Get involved', [], ['en', 'fr']),
    ], ['en', 'fr'])
], 'en');

module.exports = {
    menu: menu
};