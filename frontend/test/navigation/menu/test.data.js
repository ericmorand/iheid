const DrupalAttribute = require('drupal-attribute');

let createData = (expanded = false) => {
    let createItem = (title, below = [], expanded = false) => {
        return {
            attributes: new DrupalAttribute(),
            title: title,
            url: '#',
            below: expanded ? below : null
        }
    };

    return {
        is_expanded: expanded,
        items: [
            createItem('Lorem ipsum', [
                createItem('Discover the institute'),
                createItem('Admissions'),
                createItem('Programmes'),
                createItem('Student life')
            ], expanded),
            createItem('Dolor sit amet', [
                createItem('Research'),
                createItem('Academic departments'),
                createItem('Executive education'),
                createItem('Alumni')
            ], expanded),
            createItem('Consectetur adipiscing elit', [
                createItem('Venues'),
                createItem('New'),
                createItem('Events')
            ], expanded)
        ]
    }
};

module.exports = () => {
    return {
        test_cases: [
            {
                title: 'One level deep',
                data: createData()
            },
            {
                title: 'Two levels deep',
                data: createData(true)
            }
        ]
    };
};
