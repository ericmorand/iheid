const {createMenu, createItem} = require('../mocks');

let menu = createMenu('Footer menu', [
    createItem('Item 1', [
        createItem('Discover the institute'),
        createItem('Admissions'),
        createItem('Programmes'),
        createItem('Student life')
    ], ['en', 'fr']),
    createItem('Item 2', [
        createItem('Research'),
        createItem('Academic departments'),
        createItem('Executive education'),
        createItem('Alumni'),
    ], ['en', 'fr']),
    createItem('Item 3', [
        createItem('Venues'),
        createItem('News'),
        createItem('Events'),
    ], ['en'])
], 'en');

module.exports = {
    menu: menu
};