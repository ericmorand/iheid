/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'navigation/menu/footer',
        testCleanName: 'navigation--menu--footer',
        data: require('./test.data')()
    };
};
