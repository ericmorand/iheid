/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'navigation/breadcrumb',
        testCleanName: 'navigation--breadcrumb',
        data: require('./test.data')()
    };
};
