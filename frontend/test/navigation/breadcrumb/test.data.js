module.exports = () => {
    return {
        breadcrumb: [
            {
                url: 'http://example.com',
                text: 'Lorem'
            },
            {
                url: 'http://example.com',
                text: 'Ipsum'
            },
            {
                url: 'http://example.com',
                text: 'Dolor'
            },
            {
                text: 'Sit amet'
            },
        ]
    };
};