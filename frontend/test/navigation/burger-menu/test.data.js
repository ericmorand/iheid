const menuMocks = require('../../navigation/menu/mocks');

let languages = [
    {
        code: 'en',
        label: 'English'
    },
    {
        code: 'fr',
        label: 'French'
    }
];

module.exports = () => {
    return {
        primary_menu: menuMocks.header,
        secondary_menu: menuMocks.site_map,
        languages: languages
    };
};