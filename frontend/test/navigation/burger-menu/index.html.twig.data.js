/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'navigation/burger-menu',
        testCleanName: 'navigation--burger-menu',
        data: require('./test.data')()
    };
};
