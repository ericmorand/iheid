/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'navigation/language-switcher',
        testCleanName: 'navigation--language-switcher',
        data: require('./test.data')()
    };
};
