/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'decorator/pillar',
        testCleanName: 'decorator--pillar',
        data: require('./test.data')()
    };
};
