let Attributes = require('drupal-attribute');

/**
 *
 * @param twing {TwingEnvironment}
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function (twing) {
    return {
        testName: 'decorator/overlay',
        testCleanName: 'decorator--overlay',
        data: {
            attributes: new Attributes()
        }
    };
};
