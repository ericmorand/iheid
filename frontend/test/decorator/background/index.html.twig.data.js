/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'decorator/background',
        testCleanName: 'decorator--background',
        data: require('./test.data')()
    };
};
