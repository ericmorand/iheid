/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'decorator/margin',
        testCleanName: 'decorator--margin',
        data: require('./test.data')()
    };
};
