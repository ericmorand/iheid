/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'decorator/padding',
        testCleanName: 'decorator--padding',
        data: require('./test.data')()
    };
};
