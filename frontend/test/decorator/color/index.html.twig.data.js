/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'decorator/color',
        testCleanName: 'decorator--color',
        data: require('./test.data')()
    };
};
