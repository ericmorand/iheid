let Attributes = require('drupal-attribute');

/**
 *
 * @param twing {TwingEnvironment}
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function (twing) {
    return {
        testName: 'decorator/brand-box',
        testCleanName: 'decorator--brand-box',
        data: {
            attributes: new Attributes()
        }
    };
};
