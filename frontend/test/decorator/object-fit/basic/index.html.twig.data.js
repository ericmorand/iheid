/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    let width = 1920;
    let height = 1080;

    return {
        testName: 'decorator/object-fit',
        testCleanName: 'decorator--object-fit',
        data: {
            image: function(format, size) {
                let imageWidth;
                let imageHeight;

                if (size === 'bigger') {
                    imageWidth = 1920;
                    imageHeight = 1080;
                }
                else {
                    imageWidth = 192;
                    imageHeight = 108;
                }

                switch (format) {
                    case 'square': {
                        imageHeight = imageWidth;
                        break;
                    }
                    case 'portrait': {
                        let tmpHeight = imageHeight;

                        imageHeight = imageWidth;
                        imageWidth = tmpHeight;

                        break;
                    }
                }

                return `https://picsum.photos/${imageWidth}/${imageHeight}?image=64`;
            }
        }
    };
};
