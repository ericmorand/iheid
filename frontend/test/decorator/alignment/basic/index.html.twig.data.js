let Attributes = require('drupal-attribute');

/**
 *
 * @param twing {TwingEnvironment}
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function (twing) {
    return {
        testName: 'decorator/alignment',
        testCleanName: 'decorator--alignment',
        data: {
            attributes: new Attributes()
        }
    };
};
