/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'decorator/dimension',
        testCleanName: 'decorator--dimension',
        data: require('./test.data')()
    };
};
