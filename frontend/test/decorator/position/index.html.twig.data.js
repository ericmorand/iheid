/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'decorator/position',
        testCleanName: 'decorator--position',
        data: require('./test.data')()
    };
};
