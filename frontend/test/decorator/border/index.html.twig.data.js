/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'decorator/border',
        testCleanName: 'decorator--border',
        data: require('./test.data')()
    };
};
