let Attributes = require('drupal-attribute');

/**
 *
 * @returns {{testName: string, testCleanName: string}}
 */
module.exports = function () {
    return {
        testName: 'decorator/visibility',
        testCleanName: 'decorator--visibility',
        data: {
            getAttributes: (it) => {
                return new Attributes(it)
            }
        }
    };
};
