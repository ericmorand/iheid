/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'decorator/flex',
        testCleanName: 'decorator--flex',
        data: require('./test.data')()
    };
};
