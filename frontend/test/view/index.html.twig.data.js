/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'view',
        testCleanName: 'view',
        data: require('./test.data')()
    };
};
