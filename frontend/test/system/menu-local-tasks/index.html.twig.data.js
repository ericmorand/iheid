/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'system/menu-local-tasks',
        testCleanName: 'system--menu-local-tasks',
        data: require('./test.data')()
    };
};
