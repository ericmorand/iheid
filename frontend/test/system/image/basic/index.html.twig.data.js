/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'system/image',
        testCleanName: 'system--image',
        data: require('./test.data')()
    };
};
