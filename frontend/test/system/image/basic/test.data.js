module.exports = () => {
    return {
        content: 'Hello system/image/basic!',
        image: {
            src: 'https://picsum.photos/1920/1080?image=111',
            width: '1920px',
            height: '1080px'
        }
    };
};