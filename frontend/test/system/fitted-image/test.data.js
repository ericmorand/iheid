module.exports = () => {
    return {
        content: 'Hello system/fitted-image!',
        image: {
            src: 'https://picsum.photos/1920/1080?image=550',
            width: '1920px',
            height: '1080px'
        }
    };
};