/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'system/fitted-image',
        testCleanName: 'system--fitted-image',
        data: require('./test.data')()
    };
};
