module.exports = () => {
    return {
        content: 'Hello system/status-messages!',
        test_cases: [
            {
                title: 'Single message',
                status_headings: {
                    'status': 'Status message',
                    'error': 'Error message'
                },
                message_list: {
                    'status': [
                        'The changes have been saved.'
                    ],
                    'error': [
                        'Notice: Undefined index: options in Drupal\\Core\\Menu\\MenuLinkBase->getOptions() (line 95 of core/lib/Drupal/Core/Menu/MenuLinkBase.php).'
                    ]
                }
            },
            {
                title: 'Multiple messages',
                status_headings: {
                    'status': 'Status message',
                    'error': 'Error message'
                },
                message_list: {
                    'status': [
                        'The changes have been saved.',
                        'The changes have been saved.'
                    ],
                    'error': [
                        'Notice: Undefined index: options in Drupal\\Core\\Menu\\MenuLinkBase->getOptions() (line 95 of core/lib/Drupal/Core/Menu/MenuLinkBase.php).',
                        'Notice: Undefined index: options in Drupal\\Core\\Menu\\MenuLinkBase->getOptions() (line 95 of core/lib/Drupal/Core/Menu/MenuLinkBase.php).'
                    ]
                }
            }
        ]
    };
};