/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'system/status-messages',
        testCleanName: 'system--status-messages',
        data: require('./test.data')()
    };
};
