/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'system/responsive-image',
        testCleanName: 'system--responsive-image',
        data: require('./test.data')()
    };
};
