module.exports = () => {
    return {
        content: 'Hello system/responsive-image!',
        image: {
            src: 'https://picsum.photos/1920/1080?image=111',
            type: 'image/jpeg'
        }
    };
};