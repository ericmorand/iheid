/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'block/block',
        testCleanName: 'block--block',
        data: require('./test.data')()
    };
};
