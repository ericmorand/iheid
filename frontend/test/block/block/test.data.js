module.exports = () => {
    let content = 'Hello Block!';

    return {
        test_cases: [
            {
                title: 'Without label',
                data: {
                    content: content
                }
            },
            {
                title: 'With label',
                data: {
                    label: 'Lorem ipsum',
                    content: content
                }
            },
            {
                title: 'With label, title prefix and title suffix',
                data: {
                    title_prefix: 'Title prefix',
                    label: 'Lorem ipsum',
                    content: content,
                    title_suffix: 'Title suffix'
                }
            }
        ]
    };
};