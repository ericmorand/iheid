const sinon = require('sinon');

sinon.stub(window, 'fetch').callsFake((url) => {
    let result = {
        markup: `<div class="fake-content">Content</div>`
    };

    let response = new window.Response(JSON.stringify(result), {
        status: 200,
        headers: {'Content-type': 'application/json'}
    });

    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve(response);
        }, 1500);
    });
});

require('./test');

module.exports = {};
