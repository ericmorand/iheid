/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'block/contact-information-modal',
        testCleanName: 'block--contact-information-modal',
        data: require('./test.data')()
    };
};
