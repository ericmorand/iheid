/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'block/pillar-selector',
        testCleanName: 'block--pillar-selector',
        data: require('./test.data')()
    };
};
