const DrupalAttribute = require('drupal-attribute');

module.exports = () => {
    return {
        profiles: [
            {
                id: 0,
                title: 'Study',
                url: '#'
            },
            {
                id: 1,
                title: 'Research',
                url: '#'
            },
            {
                id: 2,
                title: 'Engagement',
                url: '#'
            },
            {
                id: 3,
                title: 'About',
                url: '#'
            },
            {
                id: 4,
                title: 'Another one',
                url: '#'
            },
            {
                id: 5,
                title: 'Yet Another One!',
                url: '#'
            }
        ]
    };
};
