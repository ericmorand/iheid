/**
 *
 * @returns {{testName: string, testCleanName: string, data: {}}}
 */
module.exports = () => {
    return {
        testName: 'misc/contact-list',
        testCleanName: 'misc--contact-list',
        data: require('./test.data')()
    };
};
