const fs = require('fs-extra');
const path = require('path');
const webfontsGenerator = require('webfonts-generator');

const Promise = require('promise');
const Finder = require('fs-finder');

let options = require('../config');

let files = Finder.from('icon-font/assets').findFiles('*.svg');

let promise = new Promise(function (fulfill, reject) {
    options.files = Array.from(new Set(files.sort()));

    webfontsGenerator(options, function (error, result) {
        if (error) {
            reject(error);
        }
        else {
            let codePoints = options.codepoints;
            let variables = [];
            let map = [];
            let glyphs = [];

            Object.keys(codePoints).forEach(function (key) {
                let codePoint = `\\${codePoints[key].toString(16)}`;

                variables.push(`$iheid--theme--iconography--glyph--${key}: ${key};`);

                map.push(`${key}:${codePoint}`);

                glyphs.push({
                    name: key,
                    code: codePoints[key]
                })
            });

            variables.push(`$iheid--theme--iconography--code-points: (${map.join(',')});`);

            fs.writeFileSync(path.join(options.dest, 'variables.scss'), variables.join('\n'));
            fs.writeFileSync(path.join(options.dest, 'glyphs.json'), JSON.stringify(glyphs));
        }
    });

    fulfill();
});

let start = async function() {
    await promise;
};

return start();