const path = require('path');

module.exports = {
    css: false,
    dest: path.join('icon-font/dist'),
    fontName: 'IheidIcon',
    normalize: true,
    startCodepoint: 0xF101,
    codepoints: {}
};