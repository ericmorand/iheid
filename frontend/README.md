IHEID Drupal Theme Frontend Implementation
==========================================

## Prerequisites

[Node.js >= 6.0.0](https://nodejs.org)

## Installation instructions

* Install dependencies by running Node.js package manager.
       
       npm install
       
* Launch *default* npm task.

       npm start
     
* Choose the component you want to work on.

* Code!

* See the results in realtime by opening in your favorite browser the URL returned to you by the `npm start` command.

## Usage

### Running the test suite

````
npm t
````

The test suite consists of building the styleguide. This is currently the only way to ensure that every component build without error.

### Creating a component

To create a component, execute the `yo:component` script.

````
npm run yo:component
````
      
### Creating a test

To create a component, execute the `yo:test` script.

````
npm run yo:test
````

### Building the styleguide

````
npm run build:styleguide
````

### Building the custom icon font

````
npm run build:icon-font
````

Note that this script is executed automatically before the `start` and `build` scripts.