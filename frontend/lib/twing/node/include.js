const TwingNodeInclude = require('twing/lib/twing/node/include').TwingNodeInclude;

/**
 * Include node that is able to include template relatively to the current template.
 *
 * @type {module.exports}
 */
module.exports = class extends TwingNodeInclude {
    // addGetTemplate(compiler) {
    //     compiler
    //         .raw('(() => {\n')
    //         .indent()
    //         .write('let path = require(\'path\');\n')
    //         .write('let currentPath = this.getSourceContext().path;\n')
    //         .write('let templatePath = path.resolve(path.dirname(currentPath), ')
    //         .subcompile(this.getNode('expr'))
    //         .raw(');\n')
    //         .write('console.warn("THERE", templatePath);')
    //         .write('return this.loadTemplate(')
    //         .raw('templatePath')
    //         .raw(', ')
    //         .repr(this.getTemplateName())
    //         .raw(', ')
    //         .repr(this.getTemplateLine())
    //         .raw(');\n')
    //         .outdent()
    //         .write('})()')
    //     ;
    // }
};