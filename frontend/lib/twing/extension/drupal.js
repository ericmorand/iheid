const {TwingFilter, TwingExtension, TwingMarkup} = require('twing');
const {twingEscapeFilterIsSafe, twingEscapeFilter} = require('twing/lib/extension/core');
const DrupalNodeVisitor = require('../node-visitor/drupal');
const DrupalAttribute = require('drupal-attribute');
const kebabCase = require('change-case').paramCase;

/**
 * Prepares a string for use as a valid class name.
 *
 * Do not pass one string containing multiple classes as they will be
 * incorrectly concatenated with dashes, i.e. "one two" will become "one-two".
 *
 * @param {*} className The class name to clean. It can be a string or anything that can be cast to string.
 *
 * @returns {string} The cleaned class name.
 */
function cleanClass(className) {
    className = String(kebabCase(className));

    return className;
}

module.exports = class DrupalExtension extends TwingExtension {
    getNodeVisitors() {
        return [
            new DrupalNodeVisitor()
        ];
    }

    getFilters() {
        return [
            new TwingFilter('drupal_escape', this.escapeFilter, {
                needs_environment: true,
                is_safe_callback: twingEscapeFilterIsSafe
            }),
            new TwingFilter('clean_class', cleanClass),
            new TwingFilter('t', (value) => {
                return value;
            }),
        ]
    }

    escapeFilter(env, arg, strategy = 'html', charset = null, autoescape = false) {
        if (arg && arg instanceof TwingMarkup) {
            return arg;
        }

        if (arg && arg.constructor.name === DrupalAttribute.name) {
            return arg;
        }

        return twingEscapeFilter(env, arg, strategy, charset, autoescape);
    }
};