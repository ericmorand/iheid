const {TwingFunction, TwingExtension, TwingMarkup, TwingFilter} = require('twing');
const Attribute = require('drupal-attribute');
const {ViewMode, FormattedField, FieldGroup, FieldItem, FieldFormatter} = require('../../drupal');
const kebabCase = require('change-case').paramCase;

module.exports = class IHEIDExtension extends TwingExtension {
    getFilters() {
        let index = 0;

        return new Map([
            [index++, new TwingFilter('kebab_case', (string) => {
                return kebabCase(string);
            })]
        ]);
    }

    getFunctions() {
        let index = 0;

        /**
         * @param env {TwingEnvironment}
         * @param value {*}
         * @param formatter {DrupalFieldFormatter}
         */
        let renderFieldItem = (env, value, formatter) => {
            let template = env.resolveTemplate([
                `@Test/field/formatter/${kebabCase(formatter.name)}/test.html.twig`,
                `@Test/field/formatter/raw-text/test.html.twig`
            ]);

            return new TwingMarkup(template.render({
                value: value,
                settings: formatter.settings
            }));
        };

        /**
         * @param env {TwingEnvironment}
         * @param field {DrupalField}
         * @param formatter {DrupalFieldFormatter}
         */
        let renderField = (env, field, formatter) => {
            let items = [];

            for (let value of field.values) {
                value = field.processValue(value, formatter);

                let itemContent = renderFieldItem(env, value, formatter);

                items.push(new FieldItem(itemContent));
            }

            let variables = field.processVariables({
                attributes: new Attribute().addClass(formatter.decorators),
                title_attributes: new Attribute(),
                field_type: field.type,
                field_name: field.name,
                label: field.label,
                label_display: formatter.labelDisplay,
                label_hidden: formatter.labelHidden,
                items_layout: formatter.itemsLayout,
                alternate_styling: formatter.alternateStyling,
                items: items
            }, formatter.settings);

            let template = env.resolveTemplate([
                `@Test/field/name/${kebabCase(field.name)}/test.html.twig`,
                `@Test/field/type/${kebabCase(field.type)}/test.html.twig`,
                `@Test/field/field/test.html.twig`
            ]);

            return new TwingMarkup(template.render(variables));
        };

        /**
         * @param env {TwingEnvironment}
         * @param entity {DrupalEntity}
         * @param viewMode {DrupalViewMode|string}
         * @returns TwingMarkup
         */
        let renderEntity = (env, entity, viewMode) => {
            if (typeof viewMode === 'string') {
                let template = env.resolveTemplate([
                    `@Test/entity/${kebabCase(entity.type)}/${kebabCase(entity.bundle)}/view-modes/${kebabCase(viewMode)}.html.twig`,
                    `@Test/entity/${kebabCase(entity.type)}/view-modes/${kebabCase(viewMode)}.html.twig`,
                    `@Test/entity/node/basic/view-modes/${kebabCase(viewMode)}.html.twig`,
                    `@Test/entity/${kebabCase(entity.type)}/${kebabCase(entity.bundle)}/view-modes/full.html.twig`,
                ]);

                return new TwingMarkup(template.render({
                    entity: entity
                }));
            }

            /**
             * @param elements {Array<DrupalViewModeElement>}
             */
            let renderElements = (elements) => {
                let markup = '';

                for (let element of elements) {
                    if (element.kind === 'field_group') {
                        /**
                         * @name {DrupalFieldGroup} element
                         */
                        let children = renderElements(element.children);
                        let template = env.resolveTemplate(`@Test/field/group/${kebabCase(element.type)}/test.html.twig`);

                        markup += template.render({
                            settings: element.settings,
                            children: children,
                            entity: entity
                        });
                    }
                    else if (element.kind === 'formatted_field') {
                        let field = entity.get(element.fieldName);

                        if (!field) {
                            throw new Error(`Field '${element.fieldName}' not found in entity '${entity.type}/${entity.bundle}'`);
                        }

                        markup += renderField(env, field, new FieldFormatter(
                            element.formatterName,
                            element.labelDisplay,
                            element.settings)
                        );
                    }
                }

                return new TwingMarkup(markup);
            };

            return renderElements(viewMode.elements);
        };

        /**
         * @param {string} src
         * @param {string} operation
         * @param {Map<string, number>} dimensions
         * @returns {{src: string, width: number, height: number}}
         */
        let resizedImage = (src, operation, dimensions) => {
            let size;

            let width = dimensions.has('width') ? dimensions.get('width') : null;
            let height = dimensions.has('height') ? dimensions.get('height') : null;

            if (width && height) {
                size = `${width}x${height}`;
            }
            else {
                if (width) {
                    size = `${width}`;
                }
                else if (height) {
                    size = `${height}`;
                }
            }

            return {
                src: size ? `https://ce801bafc.cloudimg.io/${operation}/${size}/x/${src}` : src,
                width: width,
                height: height
            }
        };

        /**
         * @param env {TwingEnvironment}
         * @param {{src: string}} image
         * @param {string} imageStyle
         */
        let renderImage = (env, image, imageStyle = null) => {
            let template = env.resolveTemplate([
                `@Test/system/image-style/${kebabCase(imageStyle)}/test.html.twig`,
                `@Test/system/image/basic/test.html.twig`
            ]);

            return new TwingMarkup(template.render({
                image: image,
                image_style: imageStyle
            }));
        };

        /**
         * @param env {TwingEnvironment}
         * @param {{src: string}} image
         * @param {string} imageStyle
         */
        let renderResponsiveImage = (env, image, imageStyle = null) => {
            let template = env.resolveTemplate([
                `@Test/system/responsive-image-style/${kebabCase(imageStyle)}/test.html.twig`,
                `@Test/system/responsive-image/test.html.twig`
            ]);

            return new TwingMarkup(template.render({
                image: image,
                responsive_image_style: imageStyle
            }));
        };

        /**
         * @param env {TwingEnvironment}
         * @param content {*}
         * @param settings {Map<string, *>}
         */
        let renderBlock = (env, content, settings = new Map()) => {
            let template = env.resolveTemplate([
                `@Test/block/block/test.html.twig`
            ]);

            let decorators = settings.has('decorators') ? settings.get('decorators') : null

            return new TwingMarkup(template.render({
                attributes: new Attribute().addClass(decorators),
                content: content
            }));
        };

        return new Map([
            [index++, new TwingFunction('drupal_attribute', (it) => {
                return new Attribute(it);
            })],
            [index++, new TwingFunction('view_mode', (name, elements) => {
                return new ViewMode(name, elements);
            })],
            [index++, new TwingFunction('formatted_field', (fieldName, labelDisplay, formatterName, settings) => {
                return new FormattedField(fieldName, labelDisplay, formatterName, settings);
            })],
            [index++, new TwingFunction('field_group', (type, settings, children) => {
                return new FieldGroup(type, settings, children);
            })],
            [index++, new TwingFunction('field_formatter', (name, labelDisplay, settings) => {
                return new FieldFormatter(name, labelDisplay, settings);
            })],
            [index++, new TwingFunction('render_entity', renderEntity, {needs_environment: true})], // todo: should be named render_entity_content
            [index++, new TwingFunction('render_field', renderField, {needs_environment: true})],
            [index++, new TwingFunction('render_field_item', renderFieldItem, {needs_environment: true})],
            [index++, new TwingFunction('resized_image', resizedImage)],
            [index++, new TwingFunction('render_image', renderImage, {needs_environment: true})],
            [index++, new TwingFunction('render_responsive_image', renderResponsiveImage, {needs_environment: true})],
            [index++, new TwingFunction('render_block', renderBlock, {needs_environment: true})]
        ]);
    }
};