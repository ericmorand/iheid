const {TwingLoaderFilesystem} = require('twing');
const path = require('path');

class IHEIDLoaderFilesystem extends TwingLoaderFilesystem {
    findTemplate(name, throw_ = true) {
        name = this.normalizeName(name);

        if (path.isAbsolute(name)) {
            this.cache.set(name, name);
        }

        return super.findTemplate(name, throw_);
    }
}

module.exports = IHEIDLoaderFilesystem;