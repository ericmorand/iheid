const {TwingBaseNodeVisitor, TwingNodeType} = require('twing');

let drupal_escape = function() {

};

module.exports = class extends TwingBaseNodeVisitor {
    constructor() {
        super();

        this.skipRenderVarFunction = false;
    }

    doEnterNode(node, env) {
        return node;
    }

    doLeaveNode(node, env) {
        // Change the 'escape' filter to our own 'drupal_escape' filter.
        if (node.getType() === TwingNodeType.EXPRESSION_FILTER) {
            let name = node.getNode('filter').getAttribute('value');

            if (name === 'escape' || name === 'e') {
                // Use our own escape filter that is SafeMarkup aware.
                node.getNode('filter').setAttribute('value', 'drupal_escape');

                // Store that we have a filter active already that knows how to deal with render arrays.
                this.skipRenderVarFunction = true;
            }
        }

        return node;
    }

    getPriority() {
        // Just above the Optimizer, which is the normal last one.
        return 256;
    }
};