'use strict';

const Generator = require('yeoman-generator');

const chalk = require('chalk');
const yosay = require('yosay');
const path = require('path');
const slash = require('slash');
const getSlug = require('speakingurl');
const fs = require('fs-extra');

module.exports = class extends Generator {
    prompting() {
        // Have Yeoman greet the user.
        this.log(yosay(
            'Welcome to the ' + chalk.red('IHEID component') + ' generator!'
        ));

        let prompts = [
            {
                type: 'input',
                name: 'componentName',
                message: 'Name of the component',
                validate: function (input) {
                    return input.length > 0;
                },
                store: true
            },
            {
                type: 'input',
                name: 'componentDescription',
                message: 'Description of the component',
                store: true
            },
            {
                type: 'input',
                name: 'componentAuthor',
                message: 'Author of the component',
                store: true
            }
        ];

        return this.prompt(prompts).then(function (props) {
            // To access props later use this.props.someAnswer;
            this.props = props;
        }.bind(this));
    }

    writing() {
        let componentRoot = this.config.get('src').root;
        let componentManifest = this.config.get('src').componentManifest;
        let componentCleanName = getSlug(this.props.componentName, '--');

        // paths
        let componentPath = slash(path.join(componentRoot, this.props.componentName));

        // data
        let data = {
            componentName: this.props.componentName,
            componentDescription: this.props.componentDescription,
            componentVersion: '0.1.0',
            componentAuthors: this.props.componentAuthor,
            componentCleanName: componentCleanName,
            componentPath: componentPath
        };

        let that = this;
        let extensions = ['html.twig', 'js', 'scss'];

        // src
        this.fs.copyTpl(
            that.templatePath('manifest.json.ejs'),
            that.destinationPath(componentPath, componentManifest),
            data
        );

        this.fs.copyTpl(
            that.templatePath('README.md.ejs'),
            that.destinationPath(componentPath, 'README.md'),
            data
        );

        extensions.forEach(function (ext) {
            that.fs.copyTpl(
                that.templatePath(`index.${ext}.ejs`),
                that.destinationPath(componentPath, `index.${ext}`),
                data
            );
        });

        this.fs.copyTpl(
            that.templatePath('component.html.twig.ejs'),
            that.destinationPath(componentPath, `${componentCleanName}.html.twig`),
            data
        );

        this.fs.copyTpl(
            that.templatePath('mixins.scss.ejs'),
            that.destinationPath(componentPath, 'mixins.scss'),
            data
        );

        this.fs.copyTpl(
            that.templatePath('variables.scss.ejs'),
            that.destinationPath(componentPath, 'variables.scss'),
            data
        );

        this.composeWith(path.resolve('lib/generator/test'), {
            componentName: this.props.componentName,
            testName: null,
            testDescription: this.props.componentDescription,
            testAuthor: this.props.componentAuthor
        });
    }
};
