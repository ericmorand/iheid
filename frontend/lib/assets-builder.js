const fs = require('fs-extra');
const path = require('path');
const slash = require('slash');
const postcss = require('postcss');

const Promise = require('promise');
const fsCopy = Promise.denodeify(fs.copy);
const fsRemove = Promise.denodeify(fs.remove);
const fsEmptyDir = Promise.denodeify(fs.emptyDir);
const fsOutputFile = Promise.denodeify(fs.outputFile, 3);
const fsMkdirs = Promise.denodeify(fs.mkdirs, 1);
const Stromboli = require('../lib/stromboli/stromboli');

let writer = require('../lib/writer');
let builder = new Stromboli();

module.exports = class {
    constructor(config, deliverable, destination) {
        this.config = config;
        this.deliverable = deliverable;
        this.destination = destination;
    }

    /**
     * @function
     * @returns {Promise<Array<StromboliComponent>>}
     */
    async start() {
        let outputPath = this.destination;
        let deliverable = this.deliverable;
        let componentRoot = this.config.componentRoot;

        let splashComponents = [
            `Build started`,
            `Output path: ${path.resolve(outputPath)}`
        ];

        let length = 0;

        splashComponents.forEach(function (splashComponent) {
            length = Math.max(splashComponent.length, length);
        });

        splashComponents.splice(1, 0, '='.repeat(length));
        splashComponents.push('='.repeat(length));

        splashComponents.forEach(function (splashComponent) {
            builder.warn(splashComponent);
        });

        let tmpPath = path.join('tmp', 'build', deliverable);

        let assetsOutputPath = path.join(outputPath, 'assets');
        let stylesheetssOutputPath = path.join(outputPath, 'css');
        let scriptsOutputPath = path.join(outputPath, 'js');
        let templatesOutputPath = path.join(outputPath, 'templates');

        /** @type StromboliPlugin[] **/
        let plugins = await builder.getPlugins(this.config);
        /** @type StromboliComponent[] **/
        let components = await builder.getComponents(this.config.componentRoot, this.config.componentManifest);

        let pluginsEntries = new Map();

        for (let component of components) {
            for (let plugin of plugins) {
                let entry = plugin.getEntry();
                let entryPath = path.join(component.getPath(), entry);

                if (!pluginsEntries.has(plugin)) {
                    pluginsEntries.set(plugin, []);
                }

                let entries = pluginsEntries.get(plugin);

                try {
                    fs.statSync(entryPath);

                    entries.push(entryPath);
                }
                catch (err) {
                }
            }
        }

        // create the final component
        let promises = [];

        for (let [plugin, entries] of pluginsEntries) {
            let data = '';

            for (let entry of entries) {
                switch (plugin.getName()) {
                    case 'js':
                        data += 'require("' + slash(path.relative(tmpPath, entry)) + '");';
                        break;
                    case 'css':
                        data += '@import "' + slash(path.relative(tmpPath, entry)) + '";';
                        break;
                    case 'twig':
                        data += '{% include "' + slash(entry) + '" %}';
                }
            }

            promises.push(fsOutputFile(path.join(tmpPath, plugin.getEntry()), data));
        }

        // manifest
        let nameParts = [
            'build'
        ];

        if (deliverable.length) {
            nameParts.push(deliverable);
        }

        let manifest = {
            name: nameParts.join('--')
        };

        promises.push(fsOutputFile(path.join(tmpPath, 'component.json'), JSON.stringify(manifest)));

        await Promise.all(promises);

        // build the final component
        this.config.componentRoot = tmpPath;
        this.config.componentManifest = 'component.json';

        /**
         *
         * @param components {StromboliComponent[]}
         */
        components = await builder.start(this.config);

        // write
        await writer.writeComponents(components, outputPath);

        // post-process and copy assets
        promises = [];

        let postProcessStylesheet = async (stylesheet) => {
            let css = fs.readFileSync(stylesheet);

            let postCssPlugins = [
                require('cssnano')({
                    discardDuplicates: true,
                    discardComments: true,
                    zindex: false // https://github.com/ben-eb/gulp-cssnano/issues/8
                }),
                require('postcss-copy')({
                    src: path.resolve('.'),
                    dest: assetsOutputPath,
                    inputPath: function () {
                        return path.resolve('.');
                    },
                    template: function (fileMeta) {
                        return `${fileMeta.filename}`;
                    },
                    relativePath: function (dirname, fileMeta) {
                        return path.dirname(fileMeta.sourceInputFile);
                    }
                })
            ];

            let result = await postcss(postCssPlugins).process(css.toString(), {
                from: stylesheet,
                map: null
            });

            return fs.outputFileSync(path.join(path.dirname(stylesheet), path.basename(stylesheet)), result.css);
        };

        let finalizeStylesheet = async (stylesheet) => {
            let to = path.join(stylesheetssOutputPath, `${deliverable}.css`);

            fs.mkdirsSync(path.dirname(to));
            fs.copySync(stylesheet, to);

            return postProcessStylesheet(to);
        };

        let finalizeScript = async (script) => {
            let to = path.join(scriptsOutputPath, `${deliverable}.js`);

            return fs.copySync(script, to);
        };

        // css
        for (let component of components) {
            let stylesheet = path.join(outputPath, component.getName(), 'index.css');

            promises.push(finalizeStylesheet(stylesheet));
        }

        // js
        for (let component of components) {
            let script = path.join(outputPath, component.getName(), 'index.js');

            promises.push(finalizeScript(script));
        }

        // twig
        for (let component of components) {
            let renderResponses = component.getRenderResponses();

            if (renderResponses.has('twig')) {
                let renderResponse = renderResponses.get('twig');

                for (let dependency of renderResponse.getDependencies()) {
                    if (path.basename(dependency) !== 'index.html.twig') {
                        let relativeDependencyPath = path.relative(componentRoot, dependency);

                        let to = path.join(templatesOutputPath, relativeDependencyPath);

                        promises.push(fsMkdirs(path.dirname(to)).then(
                            function () {
                                return fsCopy(dependency, to);
                            }
                        ));
                    }
                }
            }
        }

        await Promise.all(promises);

        // clean
        promises = [];

        for (let component of components) {
            promises.push(fsRemove(path.join(outputPath, component.getName())));
        }

        promises.push(fsRemove(path.join(outputPath, tmpPath)));

        await Promise.all(promises);

        return components;
    }
};