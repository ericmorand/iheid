const Attribute = require('drupal-attribute');

class DrupalFieldItem {
    /**
     *
     * @param content {string}
     */
    constructor(content) {
        /**
         *
         * @type {string}
         * @private
         */
        this._content = content;

        /**
         * @var DrupalAttribute
         */
        this._attributes = new Attribute();
    }

    /**
     *
     * @returns {string}
     */
    get content() {
        return this._content;
    }

    /**
     * @return {DrupalAttribute}
     */
    get attributes() {
        return this._attributes;
    }
}

module.exports = DrupalFieldItem;