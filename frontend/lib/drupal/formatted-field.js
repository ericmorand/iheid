const DrupalViewModeElement = require('./view-mode-element');

class DrupalFormattedField extends DrupalViewModeElement {
    /**
     *
     * @param fieldName {string}
     * @param labelDisplay {string}
     * @param formatterName {string}
     * @param settings {Map<string, *>}
     */
    constructor(fieldName, labelDisplay, formatterName, settings = new Map()) {
        super('formatted_field');

        /**
         * @var string
         * @private
         */
        this._fieldName = fieldName;
        /**
         * @var string
         * @private
         */
        this._labelDisplay = labelDisplay;
        /**
         * @var string
         * @private
         */
        this._formatterName = formatterName;
        /**
         * @var {Map<string, *>}
         */
        this._settings = settings;
    }

    /**
     * @return {string}
     */
    get fieldName() {
        return this._fieldName;
    }

    /**
     * @returns {boolean}
     */
    get labelHidden() {
        return this._labelDisplay === 'hidden';
    }

    /**
     * @return {string}
     */
    get labelDisplay() {
        return this._labelDisplay;
    }

    /**
     * @return {string}
     */
    get formatterName() {
        return this._formatterName;
    }

    /**
     * @return {Map<string, *>}
     */
    get settings() {
        return this._settings;
    }
}

module.exports = DrupalFormattedField;