class DrupalViewMode {
    /**
     *
     * @param name {string}
     * @param elements {Array<DrupalFieldGroup|DrupalField>}
     */
    constructor(name, elements) {
        /**
         *
         * @type {string}
         * @private
         */
        this._name = name;
        /**
         *
         * @type {Array<DrupalFieldGroup|DrupalField>}
         * @private
         */
        this._elements = elements;
    }

    /**
     * @return {string}
     */
    get name() {
        return this._name;
    }

    /**
     *
     * @returns {Array<DrupalFieldGroup|DrupalField>}
     */
    get elements() {
        return this._elements;
    }
}

module.exports = DrupalViewMode;