const DrupalViewModeElement = require('./view-mode-element');

class DrupalFieldGroup extends DrupalViewModeElement {
    /**
     *
     * @param type {string}
     * @param settings {*}
     * @param children {Array<DrupalFieldGroup|DrupalFormattedField>}
     */
    constructor(type, settings, children) {
        super('field_group');

        /**
         *
         * @type {string}
         * @private
         */
        this._type = type;
        /**
         *
         * @type {Array<*>}
         * @private
         */
        this._settings = settings;
        /**
         *
         * @type {Array<DrupalFieldGroup|DrupalFormattedField>}
         * @private
         */
        this._children = children;
    }

    /**
     *
     * @returns {string}
     */
    get type() {
        return this._type;
    }

    /**
     *
     * @returns {Array<*>}
     */
    get settings() {
        return this._settings;
    }

    /**
     *
     * @returns {Array<DrupalFieldGroup|DrupalFormattedField>}
     */
    get children() {
        return this._children;
    }
}

module.exports = DrupalFieldGroup;