class DrupalViewModeElement {
    /**
     *
     * @param kind {string}
     */
    constructor(kind) {
        /**
         * @var string
         */
        this._kind = kind;
    }

    /**
     * @return {string}
     */
    get kind() {
        return this._kind;
    }
}

module.exports = DrupalViewModeElement;