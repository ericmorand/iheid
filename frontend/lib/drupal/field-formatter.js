class DrupalFieldFormatter {
    /**
     *
     * @param name {string}
     * @param labelDisplay {string}
     * @param settings {Map<string, *>}
     */
    constructor(name, labelDisplay = 'visible', settings = new Map()) {
        /**
         * @var string
         * @private
         */
        this._name = name;
        /**
         * @var string
         */
        this._labelDisplay = labelDisplay;

        /**
         * @var {Map<string, *>}
         * @private
         */
        this._settings = settings;
    }

    /**
     * @return {string}
     */
    get name() {
        return this._name;
    }

    /**
     * @return {Map<string, *>}
     */
    get settings() {
        return this._settings;
    }

    /**
     * @return {string}
     */
    get labelDisplay() {
        return this._labelDisplay;
    }

    /**
     * @returns {boolean}
     */
    get labelHidden() {
        return this._labelDisplay === 'hidden';
    }

    /**
     * @returns {string}
     */
    get itemsLayout() {
        let itemsLayout = null;

        if (this.settings.has('items_layout')) {
            itemsLayout = this.settings.get('items_layout');
        }

        return itemsLayout;
    }

    /**
     * @returns {string}
     */
    get decorators() {
        let decorators = null;

        if (this.settings.has('decorators')) {
            decorators = this.settings.get('decorators');
        }

        return decorators;
    }

    /**
     * @returns {string}
     */
    get alternateStyling() {
        let alternateStyling = null;

        if (this.settings.has('alternate_styling')) {
            alternateStyling = this.settings.get('alternate_styling');
        }

        return alternateStyling;
    }
}

module.exports = DrupalFieldFormatter;