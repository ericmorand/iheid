class DrupalEntity {
    /**
     *
     * @param type {string}
     * @param bundle {string}
     * @param fields {Array<DrupalField>}
     */
    constructor(type, bundle, fields) {
        /**
         * @type {string}
         * @private
         */
        this._type = type;
        /**
         * @type {string}
         * @private
         */
        this._bundle = bundle;
        this._fields = new Map();

        for (let field of fields) {
            field.entity = this;

            this._fields.set(field.name, field);
        }
    }

    /**
     * @returns {string}
     */
    get type() {
        return this._type;
    }

    /**
     * @returns {string}
     */
    get bundle() {
        return this._bundle;
    }

    /**
     * Alias of get bundle().
     *
     * @returns {string}
     */
    getEntityTypeId() {
        return this.bundle;
    }

    /**
     *
     * @param name {string}
     */
    get(name) {
        if (this._fields.has(name)) {
            return this._fields.get(name);
        }

        return null;
    }

    /**
     *
     * @param name {string}
     * @param value {*}
     *
     * @returns {DrupalEntity}
     */
    set(name, values) {
        if (this._fields.has(name)) {
            let field = this._fields.get(name);

            if (!Array.isArray(values)) {
                values = [values];
            }

            field.values = values;
        }

        return this;
    }
}

module.exports = DrupalEntity;