/**
 * @callback PreprocessCallback
 * @param {Array<DrupalFieldItem>} items
 * @param {Map<string, *>} formatterSettings
 */

/**
 * Drupal fieldName
 */
class DrupalField {
    /**
     *
     * @param type {string}
     * @param name {string}
     * @param label {string | NULL}
     * @param values {Array<*>}
     */
    constructor(type, name, label = null, values = []) {
        /**
         * @type {string}
         * @private
         */
        this._type = type;
        /**
         * @type {string}
         * @private
         */
        this._name = name;
        /**
         * @type {string}
         * @private
         */
        this._label = label;
        /**
         * @type {DrupalEntity}
         * @private
         */
        this._entity = null;
        /**
         * @type {Array}
         * @private
         */
        this._values = values
    }

    /**
     *
     * @returns {string}
     */
    get type() {
        return this._type;
    }

    /**
     *
     * @returns {string}
     */
    get name() {
        return this._name;
    }

    /**
     *
     * @returns {string}
     */
    get label() {
        return this._label;
    }

    /**
     *
     * @returns {Array<*>}
     */
    get values() {
        return this._values;
    }

    set values(data) {
        this._values = data;
    }

    /**
     * @param {DrupalEntity} data
     */
    set entity(data) {
        this._entity = data;
    }

    /**
     * @return {DrupalEntity}
     */
    get entity() {
        return this._entity;
    }

    /**
     * Process the value that will be passed to the formatter.
     *
     * @param {*} value
     * @param {DrupalFieldFormatter} formatter
     * @returns {*}
     */
    processValue(value, formatter) {
        return value;
    }

    /**
     * Process the variables that will be passed to the template.
     *
     * This emulates a potential field pre-process function from the Drupal side.
     *
     * @param {*} variables
     * @param {Map<string, *>} formatterSettings
     * @returns {*}
     */
    processVariables(variables, formatterSettings) {
        return variables;
    }

    /**
     * @returns {*}
     */
    get value() {
        return this.values[0];
    }
}

/**
 *
 * @type {DrupalField}
 */
module.exports = DrupalField;