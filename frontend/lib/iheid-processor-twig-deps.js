const BaseProcessor = require('./iheid-processor-twig');
const {TwingEnvironment} = require('twing');

module.exports = class extends BaseProcessor {
    /**
     *
     * @param renderRequest {StromboliRenderRequest}
     */
    process(renderRequest) {
        let twing = new TwingEnvironment(this.config.loader, this.config.options);

        let extensions = this.config.extensions;

        if (extensions) {
            for (let extension of extensions) {
                twing.addExtension(extension);
            }
        }

        return new Promise((fulfill, reject) => {
            let onTemplate = (name, from) => {
                renderRequest.getResponse().addDependency(twing.getLoader().resolve(name, from));
            };

            twing.on('template', onTemplate);
            twing.render(renderRequest.getSource());
            twing.removeListener('template', onTemplate);

            fulfill();
        });
    }
};