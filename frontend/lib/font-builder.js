const fs = require('fs-extra');
const path = require('path');
const webfontsGenerator = require('webfonts-generator');

const Promise = require('promise');
const Finder = require('fs-finder');

let fontName = 'IheidIcon';

let options = {
    codepoints: {},
    css: false,
    dest: path.join('src/theme/font/assets', fontName),
    variablesDest: 'src/theme/iconography',
    fontName: fontName,
    normalize: true,
    startCodepoint: 0xF101
};


module.exports = class {
    start(glyphsPath, config) {
        let result = [];
        let files = Finder.from(glyphsPath).findFiles('*.svg');

        let promise = new Promise(function (fulfill, reject) {
            config.files = Array.from(new Set(files.sort()));
            config.dest = path.join(glyphsPath, '../font');

            webfontsGenerator(config, function (error, result) {
                fulfill(result);
                //
                // if (error) {
                //     reject(error);
                // }
                // else {
                //     let codePoints = config.codepoints;
                //     let variables = [];
                //     let map = [];
                //     let glyphs = [];
                //
                //
                //     Object.keys(codePoints).forEach(function (key) {
                //         let codePoint = `\\${codePoints[key].toString(16)}`;
                //
                //         variables.push(`$iheid--theme--iconography--glyph--${key}: ${codePoint};`);
                //
                //         map.push(`${key}: ${codePoint}`);
                //
                //         glyphs.push({
                //             name: key,
                //             code: codePoints[key]
                //         });
                //
                //         result.push(glyphs);
                //     });
                //
                //     variables.push(`$iheid--theme--iconography--glyphs: (${map.join(',\n')});`);
                //
                //     // fs.writeFileSync(path.join(options.variablesDest, 'variables.scss'), variables.join('\n'));
                //     // fs.writeFileSync(path.join(options.variablesDest, 'glyphs.json'), JSON.stringify(glyphs));
                // }
            });

            // fulfill(result);
        });

        return promise;
    }
};