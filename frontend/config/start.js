module.exports = {
    componentRoot: 'test',
    componentManifest: 'component.json',
    plugins: {
        html: {
            entry: 'index.html.twig',
            processors: [
                new (require('../lib/iheid-processor-twig'))(Object.assign({}, require('../config/processor/twig'), {
                    options: {
                        cache: 'tmp/twig',
                        auto_reload: true,
                        debug: true
                    }
                }))
            ]
        },
        css: {
            entry: 'index.scss',
            processors: [
                new (require('../lib/iheid-processor-sass'))(Object.assign({}, require('../config/processor/sass'), {
                    sourceMap: true,
                    sourceComments: true
                })),
                new (require('../lib/stromboli/stromboli-processor-css-rebase'))()
            ]
        },
        js: {
            entry: 'index.js',
            processors: [
                new (require('../lib/iheid-processor-javascript'))(Object.assign({}, require('./processor/javascript'), {
                    debug: true
                }))
            ]
        }
    }
};