module.exports = {
    precision: 8,
    importer: [
        require('node-sass-js-importer'),
        require('node-sass-json-importer'),
        require('node-sass-tilde-importer')
    ]
};