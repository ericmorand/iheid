let getFilterForFile = function(file) {
    return new RegExp(file)
};

let processJQueryPlugin = function(module, content, done) {
    let prefix = 'var jQuery = require("jquery");';

    content = prefix + content;

    done(content);
};

module.exports = {
    transform: [
        ['broaderify', {
            global: true,
            loaders: [
                {
                    filter: getFilterForFile('node_modules\/jquery-ui\/ui\/(.*).js'),
                    worker: processJQueryPlugin
                }
            ]
        }],
        ['stringify']
    ]
};