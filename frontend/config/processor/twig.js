const IHEIDExtension = require('../../lib/twing/extension/iheid');
const DrupalExtension = require('../../lib/twing/extension/drupal');
const {TwingExtensionDebug, TwingLoaderRelativeFilesystem, TwingLoaderChain, TwingLoaderFilesystem} = require('twing');

let fsLoader = new TwingLoaderFilesystem('/');
let namespaces = require('../../config/namespaces').namespaces;

for (let namespace of namespaces) {
    fsLoader.addPath(namespace.path, namespace.namespace);
}

module.exports = {
    loader: new TwingLoaderChain([
        fsLoader,
        new TwingLoaderRelativeFilesystem()
    ]),
    extensions: [
        new IHEIDExtension,
        new DrupalExtension,
        new TwingExtensionDebug
    ]
};