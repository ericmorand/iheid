let config = require('./build');

config.componentRoot = 'test';

// we don't want twig sources
delete config.plugins.twig;

module.exports = config;
