let merge = require('merge');

module.exports = {
    componentRoot: 'src',
    componentManifest: 'component.json',
    plugins: {
        css: {
            entry: 'index.scss',
            processors: [
                new (require('../lib/iheid-processor-sass'))(merge(true, require('./processor/sass'), {
                    sourceMap: false,
                    sourceComments: false
                })),
                new (require('../lib/stromboli/stromboli-processor-css-rebase'))()
            ]
        },
        js: {
            entry: 'index.js',
            processors: [
                new (require('../lib/iheid-processor-javascript'))(merge(true, require('./processor/javascript'), {
                    debug: false
                })),
                new (require('../lib/stromboli/stromboli-processor-babel'))
            ]
        },
        twig: {
            entry: 'index.html.twig',
            processors: [
                new (require('../lib/iheid-processor-twig-deps'))(Object.assign({}, require('../config/processor/twig')))
            ]
        }
    }
};