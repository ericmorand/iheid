let jQuery = require('jquery');
let Masonry = require('masonry-layout');

let scope = jQuery('.iheid--navigation--menu--site-map');
let masonries = [];

scope.each(function (index, element) {
    masonries.push(new Masonry(element, {
        itemSelector: '.level-0 > li',
        transitionDuration: 0
    }));
});

let reload = function () {
    for (let masonry of masonries) {
        masonry.layout();
    }
};

module.exports = {
    reload: reload
};