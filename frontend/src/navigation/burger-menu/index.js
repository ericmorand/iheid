const jQuery = require('jquery');
const {createModal, openModal, closeModal} = require('../../ui/modal');
const {reload} = require('../menu/site-map');

let scope = jQuery('.iheid--navigation--burger-menu');

scope.each(function (index, element) {
    let $element = jQuery(element);
    let $content = $element.find('.iheid--navigation--burger-menu--content');
    let modal = createModal($content);

    // trigger button
    $element
        .find('.trigger')
        .on('click', function (event) {
            openModal(modal, function () {
                reload();
            });
        })
    ;

    // close button
    $content
        .find('.close')
        .on('click', function (event) {
            closeModal(modal);
            $content.removeClass('next');
        })
    ;

    // show-next button
    $content
        .find('.show-next')
        .on('click', function (event) {
            $content.addClass('next');
            reload();
        })
    ;

    // languages selector support
    let language = $content.data('language');
    let $languageSelector = $content.find('.language-selector');
    let $languageSelectorButtons = $languageSelector.find('button');
    let $menuItems = $content.find('.menu__item');

    let refreshUI = function (language) {
        $languageSelectorButtons.each(function (index, button) {
            let $button = jQuery(button);
            let buttonLanguage = $button.data('language');

            if (buttonLanguage === language) {
                $button.removeClass('disabled');
            }
            else {
                $button.addClass('disabled');
            }
        });

        $menuItems.each(function (menuItemIndex, menuItem) {
            let $menuItem = jQuery(menuItem);
            let languages = $menuItem.data('languages');

            if (languages.includes(language)) {
                $menuItem.removeClass('disabled');
            }
            else {
                $menuItem.addClass('disabled');
            }
        });
    };

    $languageSelectorButtons.each(function (index, button) {
        let $button = jQuery(button);
        let language = $button.data('language');

        $button.on('click', function (event) {
            refreshUI(language);
        });
    });

    refreshUI(language);
});

module.exports = {};