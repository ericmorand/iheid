# Theme/Font

[![Styleguide][styleguide-image]][styleguide-url]

> One component to font them all

## Description

This component declares and manages the fonts of the theme.

The primary font is based on *DejaVu Sans* with a fallback to *sans-serif*.

The secondary font is based on *DejaVu Serif* with a fallback to *serif*.

## API

### SASS

#### Mixins

* iheid--theme--font--primary()

  Add the primary font declarations to the element.

* iheid--theme--font--secondary()

  Add the secondary font declarations to the element.

## Run time usage

### CSS classes

* iheid--theme--font--primary

  Render the element with the primary font.
  
* iheid--theme--font--secondary

  Render the element with the secondary font.

[styleguide-url]: http://picsum.photos/150/24
[styleguide-image]: http://picsum.photos/150/24