require = require('require-uncached');

let glyphs = require('../../../icon-font/dist/glyphs.json');

/**
 * @var {{name: string, code: number, hex: string}} glyph
 */
for (let glyph of glyphs) {
    glyph.hex = glyph.code.toString()
}

module.exports = {
    'iheid--iconography--glyphs': glyphs
};