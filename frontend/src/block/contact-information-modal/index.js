const jQuery = require('jquery');

let scope = jQuery('.iheid--block--contact-information-modal');

scope.each(function (index, element) {
    let $element = jQuery(element);
    let expanded = false;

    $element
        .find('.label')
        .on('click', function (event) {
            expanded = !expanded;

            refreshUI();
        })
    ;

    let refreshUI = function() {
        if (expanded) {
            $element.addClass('expanded');
        }
        else {
            $element.removeClass('expanded');
        }
    }
});

module.exports = {};