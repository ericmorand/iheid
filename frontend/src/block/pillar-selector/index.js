const jQuery = require('jquery');

let scope = jQuery('.iheid--block--pillar-selector');

scope.each(function (index, element) {
    let $element = jQuery(element);

    // find current profile and redirect to its landing page
    let currentProfile = JSON.parse(localStorage.getItem('profile'));

    let $profileItems = $element.find('li');

    $profileItems.each(function (index, profileItem) {
        let $profileItem = jQuery(profileItem);
        let profileId = $profileItem.data('profile-id');

        if (currentProfile) {
            if (profileId === currentProfile.id) {
                $profileItem.addClass('active');
            }
        }

        $profileItem.on('click', function (event) {
            localStorage.setItem('profile', JSON.stringify({
                id: profileId
            }));
        });
    });
});

module.exports = {};