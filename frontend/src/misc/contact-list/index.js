const jQuery = require('jquery');

let scope = jQuery('.iheid--misc--contact-list');

scope.each(function (index, element) {
    let $element = jQuery(element);
    let wsUrl = $element.data('ws-url');

    fetch(wsUrl).then(function (response) {
        return response.json().then(
            function (data) {
                let $placeholder = $element.find('.content');

                $placeholder.html(data.markup);
            }
        );
    })
});

module.exports = {};