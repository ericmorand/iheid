const {pushOverlay, popOverlay} = require('../../body');
const jQuery = require('jquery');

let createClass = function (suffix = '') {
    return `iheid--ui--modal${suffix ? '--' : ''}${suffix}`;
};
//
// class ModalManager {
//     /**
//      * @param $content {JQuery<JQuery.node>}
//      * @param $anchor {JQuery<JQuery.node>}
//      * @return {number}
//      */
//     static createModal($content, $anchor = null) {
//         let id = ModalManager.index++;
//
//         if (!$anchor) {
//             $anchor = jQuery('body');
//         }
//
//         let $modal = jQuery('<div>')
//             .addClass(createClass())
//         ;
//
//         let $modalContent = $content
//             .addClass(createClass('content'))
//             .appendTo($modal)
//         ;
//
//         ModalManager.registry.set(id, {
//             $modal: $modal,
//             $anchor: $anchor,
//             $modalContent: $modalContent
//         });
//
//         return id;
//     }
//
//     /**
//      * @param {number} id
//      */
//     static openModal(id) {
//         let modal = null;
//
//         if (ModalManager.registry.has(id)) {
//             modal = ModalManager.registry.get(id);
//
//             let $overlay = pushOverlay();
//             console.warn('CLICK',$overlay)
//
//             $overlay.on('click', function() {
//
//                 ModalManager.closeModal(id);
//             });
//
//             setTimeout(function() {
//                 modal.$modal.addClass('opened');
//             }, 0);
//
//             let anchorPosition = modal.$anchor.position();
//
//             modal.$modalContent.css({
//                 top: anchorPosition.top
//             });
//
//             jQuery('body').append(modal.$modal);
//         }
//     }
//
//     /**
//      * @param {number} id
//      */
//     static closeModal(id) {
//         let modal = null;
//
//         if (ModalManager.registry.has(id)) {
//             popOverlay();
//
//             modal = ModalManager.registry.get(id);
//             modal.$modal.removeClass('opened');
//
//             modal.$modal.detach();
//         }
//     }
// }
//
// ModalManager.registry = new Map();
// ModalManager.index = 0;


let registry = [];

const createModal = function ($content) {
    let $modal = $content
        .addClass(createClass())
        .detach()
    ;

    return $modal;
};

const openModal = function ($modal, callback = null) {
    let overlay = pushOverlay();

    $modal
        .appendTo('body')
        .css({
            zIndex: overlay.zIndex,
            position: 'absolute',
            display: 'block'
        })
    ;

    setTimeout(function () {
        $modal.addClass('opened');

        if (callback) {
            callback();
        }
    }, 0);

    return overlay;
};

const closeModal = function ($modal, callback = null) {
    popOverlay();

    $modal.removeClass('opened');

    $modal.detach();

    if (callback) {
        callback();
    }
};

module.exports = {
    createModal: createModal,
    openModal: openModal,
    closeModal: closeModal
};