const jQuery = require('jquery');
require('slick-carousel');

module.exports = class {
    constructor(selector) {
        /**
         * @type {JQuery<HTMLElement>}
         */
        this.$views = null;
    }

    init(selector, options = {}) {
        let self = this;

        this.$views = jQuery(selector);

        this.$views.each(function (index, view) {
            let $view = jQuery(view);
            let $slides = $view.find('> .slides');
            let slickOptions = self.getOptionsForView(view, options);

            // responsive support
            if (!slickOptions.responsive) {
                // support known breakpoints
                slickOptions.responsive = [];

                let devices = require('../../theme/device/devices.json').devices;
                let index = 0;

                for (let key in devices) {
                    // mobile first
                    if (index > 0) {
                        let device = devices[key];

                        slickOptions.responsive.push({
                            breakpoint: device.min,
                            settings: {
                                slidesToShow: 1
                            }
                        });
                    }

                    index++;
                }
            }

            // declarative slideToShow
            let slidesToShow = $view.data('slides-to-show');

            if (slidesToShow) {
                if (!Array.isArray(slidesToShow)) {
                    slidesToShow = [slidesToShow];
                }

                slickOptions.slidesToShow = slidesToShow.shift();

                let lastSlidesToShow = slickOptions.slidesToShow;

                for (let i = 0; i < slickOptions.responsive.length; i++) {
                    let responsive = slickOptions.responsive[i];

                    if (!responsive.settings) {
                        responsive.settings = {};
                    }

                    if (slidesToShow.length > 0) {
                        lastSlidesToShow = slidesToShow.shift();
                    }

                    responsive.settings.slidesToShow = lastSlidesToShow;
                }
            }

            $slides.slick(slickOptions);
        })
    }

    getOptionsForView(view, options = {}) {
        let $view = jQuery(view);

        options = jQuery.extend({}, {
            dots: true,
            arrows: true,
            rows: 0,
            infinite: true,
            mobileFirst: true,
            appendArrows: $view.find('.navigation-arrows'),
            appendDots: $view.find('.navigation-dots'),
            prevArrow: require('./templates/previous-button.html'),
            nextArrow: require('./templates/next-button.html')
        }, options);

        return options;
    }
};