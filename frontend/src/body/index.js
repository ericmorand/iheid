const jQuery = require('jquery');

class Overlay {
    constructor(node, zIndex) {
        /**
         * @var JQuery
         */
        this._node = node;

        /**
         * @var number
         */
        this._zIndex = zIndex;
    }

    /**
     * @return {JQuery}
     */
    get node() {
        return this._node;
    }

    /**
     * @return {number}
     */
    get zIndex() {
        return this._zIndex;
    }
}

/**
 * @type {Overlay[]}
 */
let overlays = [];

/**
 * @return {Overlay}
 */
const pushOverlay = function () {
    let zIndex = overlays.length;

    let $node = jQuery('<div>')
        .addClass('overlay')
        .css('zIndex', zIndex)
    ;

    jQuery('body')
        .addClass('has-overlay')
        .append($node)
    ;

    let overlay = new Overlay($node, zIndex);

    overlays.push(overlay);

    return overlay;
};

const popOverlay = function () {
    let overlay = overlays.pop();

    overlay.node.remove();

    if (!hasOverlay()) {
        jQuery('body')
            .removeClass('has-overlay')
        ;
    }

    return overlay;
};

const hasOverlay = function () {
    return overlays.length > 0;
};

module.exports = {
    pushOverlay: pushOverlay,
    popOverlay: popOverlay,
    hasOverlay: hasOverlay
};