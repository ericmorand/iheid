let Slider = require('../../ui/slider/index');

let slider = new Slider();

document.addEventListener('DOMContentLoaded', function (event) {
    slider.init('.iheid--field--items-layout-slider');
});

module.exports = {};