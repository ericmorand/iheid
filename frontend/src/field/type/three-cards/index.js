const jQuery = require('jquery');

let scope = jQuery('.iheid--field--type-three-cards');

scope.each(function (index, element) {
    let $element = jQuery(element);
    let wsUrl = $element.data('ws-url');

    fetch(wsUrl).then(function (response) {
        return response.json().then(
            function (data) {
                let $placeholders = $element.find('.field__item.placeholder');

                $placeholders.each(function (index, placeholder) {
                    let $placeholder = jQuery(placeholder);

                    if (data.length > index) {
                        let placeholderData = data[index].markup;

                        $placeholder.html(placeholderData);
                    }
                    else {
                        $placeholder.remove();
                    }

                    $placeholder.removeClass('placeholder');
                });
            }
        );
    })
});

module.exports = {};