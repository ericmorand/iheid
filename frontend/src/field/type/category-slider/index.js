let Slider = require('../../../ui/slider');
let jQuery = require('jquery');

let slider = new Slider();

document.addEventListener('DOMContentLoaded', function (event) {
    slider.init('.iheid--field--type-category-slider > .slider', {
        infinite: false,
    });

    slider.$views.each(function (index, view) {
        let $view = jQuery(view);

        $view.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
            console.log(slick, nextSlide + 1, slick.slideCount);

            if ((nextSlide + 1) === slick.slideCount) {
                $view.addClass('last');
            }
            else {
                $view.removeClass('last');
            }
        });
    });
});

module.exports = {};