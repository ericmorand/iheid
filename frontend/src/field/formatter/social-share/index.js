// todo: we should have a component that abstract this
window.IHEID = window.IHEID || {};
window.IHEID.social_share = window.IHEID.social_share || {};

if (!window.IHEID.social_share.initialized) {
    let addScript = function (id, source) {
        let script;
        let firstScript = document.getElementsByTagName('script')[0];

        if (document.getElementById(id)) {
            return;
        }

        script = document.createElement('script');
        script.id = id;
        script.src = source;

        firstScript.parentNode.insertBefore(script, firstScript);
    };

    let initFacebook = function () {
        let facebookTriggers = document.querySelectorAll('.iheid--field--formatter--social-share.facebook > .network');
        let appId;

        for (let i = 0; i < facebookTriggers.length; i++) {
            let facebookTrigger = facebookTriggers[i];

            if (!appId) {
                appId = facebookTrigger.getAttribute('data-app-id')
            }

            facebookTrigger.addEventListener('click', function (event) {
                window.FB.ui({
                    method: 'share',
                    display: 'popup',
                    href: facebookTrigger.getAttribute('data-url')
                }, function (response) {
                });
            });
        }

        window.FB.init({
            appId: appId,
            autoLogAppEvents: true,
            xfbml: true,
            version: 'v2.12'
        });
    };

    window.fbAsyncInit = function () {
        initFacebook();

        delete window.fbAsyncInit;
    };

    addScript('facebook-sdk', 'https://connect.facebook.net/en_US/sdk.js');
    addScript('twitter-widget', 'https://platform.twitter.com/widgets.js');

    window.IHEID.social_share.initialized = true;
}

module.exports = {};