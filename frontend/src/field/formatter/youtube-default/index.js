const jQuery = require('jquery');
const uniqid = require('uniqid');

let tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
let firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

window.onYouTubeIframeAPIReady = function () {
    let $scope = jQuery('.iheid--field--formatter--youtube-default');

    $scope.each(function (index, element) {
        let $placeholders = $scope.find('> div');

        $placeholders.each(function (index, placeholder) {
            let $placeholder = jQuery(placeholder);
            let id = uniqid();

            $placeholder.attr('id', id);

            new YT.Player(id, {
                videoId: $placeholder.data('video-id'),
                playerVars: {rel: 0}
            });
        });
    });
};

module.exports = {};