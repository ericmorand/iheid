Decorators
==========

No HTML markup, only styles and scripts. Expected to be used both at compile and run time to decorate and/or add behaviors to an element.
